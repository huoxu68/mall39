package com.cskaoyan.config.shiro;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.shiro.authc.UsernamePasswordToken;

 @Data
 @NoArgsConstructor
 public class MallToken extends UsernamePasswordToken {
     String type;

     public MallToken(String username, String password, String remoteHost, String type) {
         super(username, password, remoteHost);
         this.type = type;
     }
 }
