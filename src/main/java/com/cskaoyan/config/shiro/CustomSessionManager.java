package com.cskaoyan.config.shiro;

import org.apache.shiro.web.servlet.Cookie;
import org.apache.shiro.web.session.mgt.DefaultWebSessionManager;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.Serializable;

/**
 * @ClassName: CustomSessionManager
 * @Description:
 * @Since: 2022/06/06 11:36
 * @Author: ZhangHuixiang
 */

@Component
public class CustomSessionManager extends DefaultWebSessionManager {
    private static final String ADMIN_HEADER = "X-CskaoyanMarket-Admin-Token";
    private static final String USER_HEADER = "X-CskaoyanMarket-Token";
    @Override
    protected Serializable getSessionId(ServletRequest request, ServletResponse response) {
        HttpServletRequest req = (HttpServletRequest) request;
        String adminSessionId = req.getHeader(ADMIN_HEADER);
        if (!ObjectUtils.isEmpty(adminSessionId)) {
            return adminSessionId;
        }
        String userSessionId = req.getHeader(USER_HEADER);
        if (!ObjectUtils.isEmpty(userSessionId)) {
            return userSessionId;
        }
        return super.getSessionId(request, response);
    }
}
