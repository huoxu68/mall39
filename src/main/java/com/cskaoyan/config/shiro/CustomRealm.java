package com.cskaoyan.config.shiro;

import com.cskaoyan.bean.*;
import com.cskaoyan.mapper.MarketAdminMapper;
import com.cskaoyan.mapper.MarketPermissionMapper;
import com.cskaoyan.mapper.MarketRoleMapper;
import com.cskaoyan.mapper.MarketUserMapper;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * @ClassName: CustomRealm
 * @Description:
 * @Since: 2022/06/06 11:45
 * @Author: ZhangHuixiang
 */

@Component
public class CustomRealm extends AuthorizingRealm {

    @Autowired
    MarketAdminMapper marketAdminMapper;

    @Autowired
    MarketUserMapper marketUserMapper;

    @Autowired
    MarketRoleMapper marketRoleMapper;

    @Autowired
    MarketPermissionMapper marketPermissionMapper;

    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {
        MallToken mallToken = (MallToken) authenticationToken;
        String username = mallToken.getUsername();
        String host = mallToken.getHost();
        String type = mallToken.getType();

        if ("admin".equals(type)) {
            MarketAdminExample marketAdminExample = new MarketAdminExample();
            MarketAdminExample.Criteria criteria = marketAdminExample.createCriteria();
            criteria.andDeletedEqualTo(false);
            criteria.andUsernameEqualTo(username);
            List<MarketAdmin> marketAdmins = marketAdminMapper.selectByExample(marketAdminExample);
            if (marketAdmins != null && marketAdmins.size() == 1) {
                MarketAdmin marketAdmin = marketAdmins.get(0);
                String credentials = marketAdmin.getPassword();
                marketAdmin.setLastLoginIp(host);
                marketAdmin.setLastLoginTime(new Date());
                marketAdminMapper.updateByPrimaryKeySelective(marketAdmin);
                return new SimpleAuthenticationInfo(marketAdmin, credentials, this.getName());
            }
            return null;
        } else {
            MarketUserExample marketUserExample = new MarketUserExample();
            MarketUserExample.Criteria criteria = marketUserExample.createCriteria();
            criteria.andDeletedEqualTo(false);
            criteria.andUsernameEqualTo(username);
            criteria.andStatusEqualTo((byte) 0);
            List<MarketUser> marketUsers = marketUserMapper.selectByExample(marketUserExample);
            if (marketUsers != null && marketUsers.size() == 1) {
                MarketUser marketUser = marketUsers.get(0);
                String credentials = marketUser.getPassword();
                marketUser.setLastLoginIp(host);
                marketUser.setLastLoginTime(new Date());
                marketUserMapper.updateByPrimaryKeySelective(marketUser);
                return new SimpleAuthenticationInfo(marketUser, credentials, this.getName());
            }
            return null;
        }

    }

    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
        MarketAdmin primaryPrincipal = (MarketAdmin) principalCollection.getPrimaryPrincipal();
        Integer[] roleIds = primaryPrincipal.getRoleIds();
        MarketRoleExample example = new MarketRoleExample();
        MarketRoleExample.Criteria criteria = example.createCriteria();
        criteria.andDeletedEqualTo(false);
        criteria.andIdIn(Arrays.asList(roleIds));
        List<com.cskaoyan.bean.MarketRole> marketRoles = marketRoleMapper.selectByExample(example);

        ArrayList<String> roles = new ArrayList<>();
        for (com.cskaoyan.bean.MarketRole marketRole : marketRoles) {
            roles.add(marketRole.getName());
        }
        MarketPermissionExample example1 = new MarketPermissionExample();
        MarketPermissionExample.Criteria criteria1 = example1.createCriteria();
        criteria1.andDeletedEqualTo(false);
        criteria1.andRoleIdIn(Arrays.asList(roleIds));
        List<MarketPermission> marketPermissions = marketPermissionMapper.selectByExample(example1);
        ArrayList<String> perms = new ArrayList<>();
        for (MarketPermission marketPermission : marketPermissions) {
            perms.add(marketPermission.getPermission());
        }

        SimpleAuthorizationInfo simpleAuthorizationInfo = new SimpleAuthorizationInfo();
        simpleAuthorizationInfo.addRoles(roles);
        simpleAuthorizationInfo.addStringPermissions(perms);
        return simpleAuthorizationInfo;
    }
}
