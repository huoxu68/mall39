package com.cskaoyan.mapper;

import com.cskaoyan.bean.SearchHistory;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface HistoryMapper {
    List<SearchHistory> query(@Param("sort") String sort, @Param("order") String order, @Param("userId") Integer userId, @Param("keyword") String keyword);
}
