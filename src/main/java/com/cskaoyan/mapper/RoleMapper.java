package com.cskaoyan.mapper;

import com.cskaoyan.bean.MarketRole;
import com.cskaoyan.bean.role.vo.CreateVo;
import com.cskaoyan.bean.vo.RoleInAdmin;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author zxy
 * @version 1.0.0
 * @ClassName RoleMapper.java
 * @Description TODO
 * @createTime 2022年06月04日 16:39:00
 */
public interface RoleMapper {
    Integer insertRole(@Param("createVo") CreateVo createVo);

    Integer deleteRole(@Param("createVo") CreateVo createVo);

    int updateRole(@Param("marketRole") MarketRole marketRole);

    List<RoleInAdmin> selectRole();

}
