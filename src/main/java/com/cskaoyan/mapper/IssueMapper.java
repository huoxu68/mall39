package com.cskaoyan.mapper;

import com.cskaoyan.bean.bo.IssueBO;
import com.cskaoyan.bean.param.BaseParam;
import com.cskaoyan.bean.po.Issue;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface IssueMapper {
    List<Issue> queryAll(@Param("param") BaseParam param);


    void insert(Issue issue);

    void update(Issue issue);

    void delete(IssueBO issue);

    List<Issue> queryByQuestion(@Param("param") BaseParam param, @Param("question") String question);
}
