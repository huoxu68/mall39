package com.cskaoyan.mapper;

import com.cskaoyan.bean.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UserMapper {

    List<User> query(@Param("sort") String sort,@Param("order") String order, @Param("username") String username, @Param("mobile") String mobile);

    User queryById(Integer id);

    Integer update(User user);
}
