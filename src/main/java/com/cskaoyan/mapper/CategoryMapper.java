package com.cskaoyan.mapper;

import com.cskaoyan.bean.category.CategoryL1;
import com.cskaoyan.bean.category.CategoryListL1;
import com.cskaoyan.bean.category.CategoryListL2;

import java.util.List;

public interface CategoryMapper {
    List<CategoryListL1> queryAllL1();

    List<CategoryListL2> queryAllL2(Integer id);

    List<CategoryL1> queryL1();

    void updateDeletedById(Integer id);
}
