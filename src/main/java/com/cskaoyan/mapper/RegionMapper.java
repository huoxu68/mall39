package com.cskaoyan.mapper;

import com.cskaoyan.bean.region.AreaOfCity;
import com.cskaoyan.bean.region.CityOfProvinveOfListOfDataOfRegionVO;
import com.cskaoyan.bean.region.ProvinceListOfDataOfRegionVO;

import java.util.List;

public interface RegionMapper {
    List<ProvinceListOfDataOfRegionVO> selectAllProvince();

    List<CityOfProvinveOfListOfDataOfRegionVO> selectCityByPid(Integer id);

    List<AreaOfCity> selectAreaByPid(Integer id);
}
