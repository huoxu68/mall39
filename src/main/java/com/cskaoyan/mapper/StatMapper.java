package com.cskaoyan.mapper;

import com.cskaoyan.bean.GoodsStatistics;
import com.cskaoyan.bean.OrderStatistics;
import com.cskaoyan.bean.UserStatistics;

import java.util.List;

public interface StatMapper {
    List<UserStatistics> queryUserStat();

    List<OrderStatistics> queryOrderStat();

    List<GoodsStatistics> queryGoodsStat();
}
