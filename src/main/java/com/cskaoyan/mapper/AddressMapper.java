package com.cskaoyan.mapper;

import com.cskaoyan.bean.Address;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface AddressMapper {
    List<Address> query(@Param("sort") String sort, @Param("order") String order, @Param("userId") Integer userId, @Param("name") String name);


    // 根据id 查询出userid
    Integer selectAddressOfUserIdById(Integer userId);
}
