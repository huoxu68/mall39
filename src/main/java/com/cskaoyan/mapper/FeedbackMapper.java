package com.cskaoyan.mapper;

import com.cskaoyan.bean.Feedback;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface FeedbackMapper {
    List<Feedback> query(@Param("sort") String sort, @Param("order") String order, @Param("username") String username, @Param("id") Integer id);
}
