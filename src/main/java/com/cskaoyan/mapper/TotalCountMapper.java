package com.cskaoyan.mapper;

public interface TotalCountMapper {
    Integer selectUserCount();

    Integer selectGoodsCount();

    Integer selectProductCount();

    Integer selectOrderCount();
}
