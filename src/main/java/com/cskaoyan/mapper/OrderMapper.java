package com.cskaoyan.mapper;

import com.cskaoyan.bean.MarketOrderGoods;
import com.cskaoyan.bean.bo.AftersaleSubmitBo;
import com.cskaoyan.bean.po.OrderGoods;
import com.cskaoyan.bean.po.ShipChannel;
import com.cskaoyan.bean.vo.OrderInfoOfDetileOfWx;
import com.cskaoyan.bean.vo.OrderListVO;
import com.cskaoyan.bean.vo.OrderVO;
import javafx.scene.chart.PieChart;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

public interface OrderMapper {
    List<OrderGoods> queryOrderGoodsByOrderId(Integer id);

    OrderVO queryOrderById(Integer id);

    List<OrderListVO> queryOrdersByParams(@Param("order") String order, @Param("sort") String sort, @Param("start") Date start, @Param("end") Date end, @Param("orderSn") String orderSn, @Param("orderStatusArray") List<Integer> orderStatusArray, @Param("userId") Integer userId);

    void refundById(@Param("orderId") Integer orderId, @Param("refundMoney") Double refundMoney, @Param("refundTime") Date refundTime);

    void updateById(@Param("orderId") Integer orderId, @Param("shipChannel") String shipChannel, @Param("shipSn") String shipSn, @Param("date") Date date);

    List<ShipChannel> queryChannels();

    // 通过 id 找到goodsId
     MarketOrderGoods selectOrderGoodsOfGoodsIdById(Integer orderGoodsId);

    OrderInfoOfDetileOfWx queryOrderInfoById(Integer orderId);

    Integer updateDeletedByOrderId(Integer orderId,Date updateTime);

    Integer updateConfirmByOrderId(Integer orderId, Date date);

    // 通过id 查询数据库中的是否已有评论
    String selectAddCommentById(Integer id);

    // 更改售后相关的order数据
    Integer updateAftersaleOfOrder(Short aftersaleStatus,Integer orderId, Date updateTime);

    // 查询order表中comments 数据
    Integer selectOrderOfCommentsByorderId(Integer orderId);
}
