package com.cskaoyan.mapper;

import com.cskaoyan.bean.vo.MyCoupon;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CouPonMapper {
    List<MyCoupon> queryCouponsByUserId(@Param("id")Integer id, @Param("status") Integer status);
}
