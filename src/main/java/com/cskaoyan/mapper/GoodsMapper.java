package com.cskaoyan.mapper;

import com.cskaoyan.bean.*;
import com.cskaoyan.bean.bo.GoodsMsgBo;
import com.cskaoyan.bean.vo.Children;
import com.cskaoyan.bean.vo.GoodsListVo;
import com.cskaoyan.bean.vo.ProductsVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GoodsMapper {

    // 商品的查询的一系列操作
    List<GoodsListVo> selectGoddsListMsg(@Param("sort") String sort, @Param("order") String order, @Param("goodsMsgBo") GoodsMsgBo goodsMsgBo);

    // 打印父类目
    List<Children> queryCategory();
    // 打印子类目
    List<Children> queryCateGoryOfChildren(Integer value);

    // 打印商品品牌
    List<Children> queryBrand();

    // 通过id查询商品的信息
    MarketGoods selectGoodsMsgById(Integer id);
    // 通过子类目的的id 得到 父类目的id
    Integer selectCategoryIdByPid(Integer categoryId);
    // 通过goodsId 查找products
    List<MarketGoodsProduct> selectGoodsProductsByGoodsId(Integer goodsId);
    // 通过goodsId去查找attributes
    List<MarketGoodsAttribute> selectGoodsAttributesByGoodsId(Integer goodsId);
    // 通过goodsId 去查找specifications
    List<MarketGoodsSpecification> selectGoodsSpecificationByGoodsId(Integer goodsId);

    // 删除商品
    void deleteGoodsMsg(Integer id);
    void deleteAttribute(Integer goodsId);
    void deleteProdut(Integer goodsId);
    void deleteSpecification(Integer goodsId);
}
