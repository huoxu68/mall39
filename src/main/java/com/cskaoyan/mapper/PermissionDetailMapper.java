package com.cskaoyan.mapper;

import com.cskaoyan.bean.role.po.PermissionDetail;

import java.util.List;

/**
 * @author zxy
 * @version 1.0.0
 * @ClassName PermissionDetailMapper.java
 * @Description TODO
 * @createTime 2022年06月06日 21:09:00
 */
public interface PermissionDetailMapper {

    List<PermissionDetail> selectAllType1();

    List<PermissionDetail> selectAllType2(Integer id);

    List<String> selectPermissionDetailsByPermissionName(List<String> permissionNames);

    String selectLabelByAPI(String api);
}
