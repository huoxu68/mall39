package com.cskaoyan.mapper;

import com.cskaoyan.bean.Footprint;
import com.cskaoyan.bean.vo.WxFootprintListVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface FootprintMapper {
    List<Footprint> query(@Param("sort") String sort, @Param("order") String order, @Param("userId") Integer userId, @Param("goodsId") Integer goodsId);

    // wx 小程序打印足迹
    List<WxFootprintListVo> selectWxQuery(Integer userId);

}
