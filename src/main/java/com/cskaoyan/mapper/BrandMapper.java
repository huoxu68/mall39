package com.cskaoyan.mapper;

import com.cskaoyan.bean.BaseData;
import com.cskaoyan.bean.brand.BrandList;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface BrandMapper {
    List<BrandList> queryAll(@Param("id") Integer id, @Param("name") String name);

    void updateDeleteById(Integer id);

    void updateAllBrand(@Param("brandList") BrandList brandList);
    BrandList queryBrandById(Integer id);
}
