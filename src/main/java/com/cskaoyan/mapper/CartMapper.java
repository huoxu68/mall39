package com.cskaoyan.mapper;

import com.cskaoyan.bean.MarketCart;
import org.apache.ibatis.annotations.Param;

import java.util.Date;

public interface CartMapper {
    MarketCart queryInfo(@Param("id") Integer id);

    Integer queryGoodCount(Integer id);

    Integer queryCheckedCount(Integer id);

    Integer queryMinId();

    Integer queryMaxId();

    MarketCart queryGoodById(int id);

    void updateChecked(@Param("productId") Integer productId, @Param("isChecked") Integer isChecked);

    int updateNumber(@Param("id") Integer id, @Param("number") Integer number, @Param("updateTime") Date updateTime);


    String queryFreightPrice();

    String queryMinFreightPrice();

    void updateDeleted(@Param("id") Integer pId);

    MarketCart queryByGoodsId(Integer goodsId);

    void updateDeletedByCartID(Integer cartId);

    Double selectAmountByUserId(Integer id);

    Double selectCheckedAmountByUserId(Integer id);
}
