package com.cskaoyan.mapper;

import com.cskaoyan.bean.Admin;
import com.cskaoyan.bean.Log;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface LogMapper {

    List<Log> queryLogs(@Param("sort") String sort, @Param("order") String order);

}
