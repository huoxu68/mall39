package com.cskaoyan.mapper;

import com.cskaoyan.bean.Collect;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CollectMapper {
    List<Collect> query(@Param("sort") String sort, @Param("order") String order, @Param("userId") Integer userId, @Param("valueId") Integer valueId);
}
