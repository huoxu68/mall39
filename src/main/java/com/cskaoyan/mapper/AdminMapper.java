package com.cskaoyan.mapper;

import com.cskaoyan.bean.Admin;
import com.cskaoyan.bean.MarketAdmin;
import com.cskaoyan.bean.MarketAdminExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface AdminMapper {

    List<Admin> queryAdmins(@Param("sort") String sort, @Param("order") String order, @Param("username") String username);

    Integer insertAdmin(Admin admin);

    Integer updateAdmin(Admin admin);
}
