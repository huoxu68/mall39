package com.cskaoyan.util;

import com.cskaoyan.bean.HandleOption;
import com.cskaoyan.bean.vo.OrderInfoOfDetileOfWx;

/**
 * @ClassName: HandleOptionSettingUtile
 * @Description: handleOption的设置工具
 * @Since: 2022/06/07 22:06
 * @Author: FanLujia
 */

public class HandleOptionSettingUtile {

    public static HandleOption setHandleOption(OrderInfoOfDetileOfWx orderInfo) {

        Short orderStatus = orderInfo.getOrderStatus();
        HandleOption handleOption = new HandleOption();

        if (orderStatus == 101) { //未付款
            orderInfo.setOrderStatusText("未付款");
            handleOption.setCancel(true);
            handleOption.setPay(true);
        } else if (orderStatus == 102) { //用户取消
            orderInfo.setOrderStatusText("用户取消");
            handleOption.setDelete(true);
            handleOption.setRebuy(true);
        } else if (orderStatus == 103) { //系统取消
            orderInfo.setOrderStatusText("系统取消");
            handleOption.setDelete(true);
            handleOption.setRebuy(true);
        } else if (orderStatus == 201) { //已付款
            orderInfo.setOrderStatusText("已付款");
            handleOption.setAftersale(true);
        } else if (orderStatus == 301) { //已发货
            orderInfo.setOrderStatusText("已发货");
            handleOption.setConfirm(true);
            handleOption.setAftersale(true);
        } else if (orderStatus == 202) { //申请退款
            orderInfo.setOrderStatusText("申请退款");
            handleOption.setRebuy(true);
        } else if (orderStatus == 203) { //已退款
            orderInfo.setOrderStatusText("已退款");
            handleOption.setRefund(true);
            handleOption.setDelete(true);
            handleOption.setRebuy(true);
        } else if (orderStatus == 401) { //用户收货
            orderInfo.setOrderStatusText("用户收货");
            handleOption.setComment(true);
            handleOption.setDelete(true);
            handleOption.setRebuy(true);
        } else if (orderStatus == 402) { //系统收货
            orderInfo.setOrderStatusText("系统收货");
            handleOption.setComment(true);
            handleOption.setDelete(true);
            handleOption.setRebuy(true);
        }

        return handleOption;

    }

}