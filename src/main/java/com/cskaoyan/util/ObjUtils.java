package com.cskaoyan.util;

import java.lang.reflect.Field;

/**
 * @ClassName: ObjUtils
 * @Description: 将引用类型的对象中值是空字符串的成员变量转换成null
 * @Since: 2022/06/05 10:22
 * @Author: ZhangHuixiang
 */

public class ObjUtils {

    /**
     * 将引用类型的对象中值是空字符串的成员变量转换成null
     *
     * @param o : 定义的Bean类
     * @return void
     * @author ZhangHuixiang
     * @since 2022/06/05 15:03
     */
    public static void emptyStringToNull(Object o) {
        Class<?> aClass = o.getClass();
        Field[] declaredFields = aClass.getDeclaredFields();
        for (Field declaredField : declaredFields) {
            try {
                declaredField.setAccessible(true);
                String simpleName = declaredField.getType().getSimpleName();
                Object obj = declaredField.get(o);
                if ("String".equals(simpleName) && "".equals(obj)) {
                    declaredField.set(o, null);
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
    }

}
