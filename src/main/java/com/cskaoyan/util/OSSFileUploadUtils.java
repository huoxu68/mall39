package com.cskaoyan.util;

import com.aliyun.oss.OSSClient;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.util.UUID;

/**
 * @ClassName: OSSFileUploadUtils
 * @Description:
 * @Since: 2022/06/07 17:09
 * @Author: ZhangHuixiang
 */

public class OSSFileUploadUtils {

    private static final String ACCESS_ID = "LTAI5tJy8H3VScbwdDpEZPHY";
    private static final String SECRET_KEY = "fXgyyVxwjrUNOnurRkiZbwxGPuUbUa";
    private static final String BUCKET_NAME = "huoxu";
    private static final String END_POINT = "oss-cn-nanjing.aliyuncs.com";
    private static OSSClient ossClient = new OSSClient(END_POINT, ACCESS_ID, SECRET_KEY);

    public static String[] upload(MultipartFile file) {
        String originalFilename = file.getOriginalFilename();
        int i = originalFilename.lastIndexOf(".");
        originalFilename = (UUID.randomUUID() + "" + originalFilename.substring(i)).replace("-","");
        try {
            InputStream inputStream = file.getInputStream();
            ossClient.putObject(BUCKET_NAME, originalFilename, inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new String[]{"http://" + BUCKET_NAME + "." + END_POINT + "/" + originalFilename, originalFilename};
    }
}
