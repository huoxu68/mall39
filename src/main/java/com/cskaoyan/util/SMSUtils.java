package com.cskaoyan.util;

import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsRequest;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsResponse;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.exceptions.ServerException;
import com.aliyuncs.profile.DefaultProfile;
import com.google.gson.Gson;

/**
 * @ClassName: SMSUtils
 * @Description:
 * @Since: 2022/06/07 18:44
 * @Author: ZhangHuixiang
 */

public class SMSUtils {
    private static final String ACCESS_ID = "LTAI5tJy8H3VScbwdDpEZPHY";
    private static final String SECRET_KEY = "fXgyyVxwjrUNOnurRkiZbwxGPuUbUa";

    public static String sendMessage(String mobile) {
        DefaultProfile profile = DefaultProfile.getProfile("cn-hangzhou", ACCESS_ID, SECRET_KEY);
        /** use STS Token
         DefaultProfile profile = DefaultProfile.getProfile(
         "<your-region-id>",           // The region ID
         "<your-access-key-id>",       // The AccessKey ID of the RAM account
         "<your-access-key-secret>",   // The AccessKey Secret of the RAM account
         "<your-sts-token>");          // STS Token
         **/

        IAcsClient client = new DefaultAcsClient(profile);

        SendSmsRequest request = new SendSmsRequest();
        request.setSignName("阿里云短信测试");
        request.setTemplateCode("SMS_154950909");
        request.setPhoneNumbers(mobile);
        String code = randomCode();
        request.setTemplateParam("{\"code\":\"" + code + "\"}");

        try {
            SendSmsResponse response = client.getAcsResponse(request);
            System.out.println(new Gson().toJson(response));
        } catch (ServerException e) {
            e.printStackTrace();
        } catch (ClientException e) {
            System.out.println("ErrCode:" + e.getErrCode());
            System.out.println("ErrMsg:" + e.getErrMsg());
            System.out.println("RequestId:" + e.getRequestId());
        }

        return code;
    }

    private static String randomCode() {
        StringBuffer stringBuffer = new StringBuffer();
        for (int i = 0; i < 6; i++) {
            stringBuffer.append((int) (Math.random() * 10));
        }
        return stringBuffer.toString();
    }
}
