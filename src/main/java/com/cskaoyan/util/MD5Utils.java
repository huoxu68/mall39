package com.cskaoyan.util;

import java.security.MessageDigest;

/**
 * @ClassName: MD5Utils
 * @Description:
 * @Since: 2022/06/06 17:36
 * @Author: ZhangHuixiang
 */

public class MD5Utils {

    private static String getMD5(String content) throws Exception {
        MessageDigest messageDigest = MessageDigest.getInstance("md5");
        byte[] digest = messageDigest.digest(content.getBytes());
        StringBuffer stringBuffer = new StringBuffer();
        for (byte b : digest) {
            int temp = b & 0xff;
            String s = Integer.toHexString(temp);
            if (s.length() == 1) {
                stringBuffer.append(0);
            }
            stringBuffer.append(s);
        }

        return stringBuffer.toString();
    }

    private static String getMD5(String content, String salt) throws Exception {
        return getMD5(content + "%" + salt + "*$");
    }

    public static String getMultiMd5(String content, String salt) throws Exception {
        return getMD5(getMD5(getMD5(content)), salt);
    }
}
