package com.cskaoyan.convert;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author zhongyimin
 * @since 2022/06/05 16:02
 */
@Component
public class String2DateConvert implements Converter<String, Date> {
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    @Override
    public Date convert(String s) {
        Date parse = null;
        try {
            parse = sdf.parse(s);
        } catch (ParseException e) {
            parse = null;
        }
        return parse;
    }
}
