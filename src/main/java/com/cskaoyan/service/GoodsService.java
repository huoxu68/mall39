package com.cskaoyan.service;

import com.cskaoyan.bean.*;
import com.cskaoyan.bean.bo.GoodsCreateBo;
import com.cskaoyan.bean.bo.GoodsMsgBo;
import com.cskaoyan.bean.param.BaseParam;
import com.cskaoyan.bean.vo.GoodsCategoryVo;
import com.cskaoyan.bean.vo.GoodsListVo;
import com.cskaoyan.bean.vo.GoodsQueryMsgVo;

/**
 * @ClassName：GoodsService
 * @description: 处理有关商品的请求
 * @Date 10:35 2022/6/4
 * @author：liguihao
 */
public interface GoodsService {

    // 显示商品信息
    BaseData<GoodsListVo> goodsList(BaseParam baseParam , GoodsMsgBo goodsMsgBo);

    // 父子类目以及品牌商
    GoodsCategoryVo goodsCategory();

    // 修改信息时，信息的回溯
    GoodsQueryMsgVo goodsMsgById(Integer id);

    // 获取商品的总数
    long selectGoodsCount();

    // 商品的删除
    void deleteGoodsMsg(Integer id);

    // 商品的上架er
    void createNewGoods(GoodsCreateBo goodsCreateBo);

    // 商品的修改
    void updateGoodsMsg(GoodsCreateBo goodsCreateBo);

    // 根据一级商品类目的id获取该一级类目下的所有类目
    WxGoodsCategory selectGoodsCategoryById(Integer id);

    // 根据商品id获取商品详细信息
    GoodsDetailData selectGoodsDetailByGoodsId(Integer id);

    // 根据商品id获取同类目的商品信息
    BaseData<MarketGoods> selectRelatedGoodsByGoodsId(Integer id);

    BaseData<MarketGoods> selectGoodsByCategoryIdAndKeyword(BaseParam baseParam, String keyword, Integer categoryId);
}
