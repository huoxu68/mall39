package com.cskaoyan.service;

import com.cskaoyan.bean.Address;
import com.cskaoyan.bean.BaseData;
import com.cskaoyan.bean.MarketAddress;
import com.cskaoyan.bean.param.BaseParam;

public interface AddressService {
    BaseData<Address> query(BaseParam baseParam, Integer userId, String name);

    // // wx打印地址栏
    // BaseData<MarketAddress> addressList();

    // // 修改wx 的地址信息 或者新建
    // Integer updateAddressMsg(MarketAddress marketAddress);


    // // 删除此地址信息
    // void deleteAdress(Integer id);

    // // 修改地址信息时候，地址信息的回显
    // MarketAddress addressMsgById(Integer id);

    BaseData<MarketAddress> list(Integer id);

    MarketAddress detail(Integer id);

    void save(MarketAddress marketAddress, Integer id);

    void delete(Integer id);

}
