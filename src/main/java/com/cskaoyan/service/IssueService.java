package com.cskaoyan.service;

import com.cskaoyan.bean.BaseData;
import com.cskaoyan.bean.BaseRespVo;
import com.cskaoyan.bean.MarketIssue;
import com.cskaoyan.bean.bo.IssueBO;
import com.cskaoyan.bean.param.BaseParam;
import com.cskaoyan.bean.po.Issue;

import java.util.List;

public interface IssueService {
    BaseRespVo<Object> list(BaseParam param,String question);

    BaseRespVo<Issue> create(String answer, String question);

    BaseRespVo<Issue> update(IssueBO issue);

    void delete(IssueBO issue);

    // 打印wx 端 帮助中心的详细信息
    BaseData<MarketIssue> issueList(Integer page, Integer limit);
}
