package com.cskaoyan.service;

import com.cskaoyan.bean.MarketFeedback;

public interface WxFeedbackService {
    void submitMsg(MarketFeedback marketFeedback);
}
