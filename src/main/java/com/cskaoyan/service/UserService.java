package com.cskaoyan.service;

import com.cskaoyan.bean.BaseData;
import com.cskaoyan.bean.RegisterBo;
import com.cskaoyan.bean.User;
import com.cskaoyan.bean.UserIndexData;
import com.cskaoyan.bean.param.BaseParam;

/**
 * @ClassName: UserService
 * @Description: 处理有关User的请求
 * @Since: 2022/06/03 13:34
 * @Author: ZhangHuixiang
 */

public interface UserService {
    BaseData<User> query(BaseParam baseParam, String username, String mobile);

    User queryByUserId(Integer id);

    Integer update(User user);

    UserIndexData queryOrderCountByUserId();

    Boolean insertUser(RegisterBo registerBo);

    Boolean updatePasswordByMobile(RegisterBo registerBo);
}
