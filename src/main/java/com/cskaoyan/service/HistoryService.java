package com.cskaoyan.service;

import com.cskaoyan.bean.BaseData;
import com.cskaoyan.bean.SearchHistory;
import com.cskaoyan.bean.param.BaseParam;

public interface HistoryService {
    BaseData<SearchHistory> query(BaseParam baseParam, Integer userId, String keyword);
}
