package com.cskaoyan.service;

import com.cskaoyan.bean.BaseData;
import com.cskaoyan.bean.MarketCategory;
import com.cskaoyan.bean.category.CategoryL1;
import com.cskaoyan.bean.category.CategoryListL1;

public interface CategoryService {
    BaseData<CategoryListL1> queryAll();

    BaseData<CategoryL1> queryL1();

    void updateDeletedById(Integer id);

    MarketCategory addCategory(MarketCategory category);


    Integer updateCategory(MarketCategory category);
}
