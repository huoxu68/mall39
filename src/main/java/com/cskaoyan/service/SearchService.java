package com.cskaoyan.service;

import com.cskaoyan.bean.BaseRespVo;

/**
 * @author zxy
 * @version 1.0.0
 * @ClassName SearchService.java
 * @Description TODO
 * @createTime 2022年06月07日 11:14:00
 */
public interface SearchService {
    BaseRespVo getIndex();

    void clearhistory();

    BaseRespVo helper(String keyword);
}
