package com.cskaoyan.service;

import com.cskaoyan.bean.BaseData;
import com.cskaoyan.bean.Footprint;
import com.cskaoyan.bean.param.BaseParam;
import com.cskaoyan.bean.vo.WxFootprintListVo;

public interface FootprintService {
    BaseData<Footprint> query(BaseParam baseParam, Integer userId, Integer goodsId);

    // wx 小程序足迹的打印
    BaseData<WxFootprintListVo> footprintList(Integer page, Integer limit);

    // wx 小程足迹的删除
    void updateFootprint(Integer id);
}
