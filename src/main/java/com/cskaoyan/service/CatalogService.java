package com.cskaoyan.service;

import com.cskaoyan.bean.BaseRespVo;

/**
 * @author zxy
 * @version 1.0.0
 * @ClassName CatalogService.java
 * @Description TODO
 * @createTime 2022年06月07日 09:16:00
 */
public interface CatalogService {

    BaseRespVo getCurrent(Integer id);

    BaseRespVo index();

}
