package com.cskaoyan.service;

import com.cskaoyan.bean.HomeIndexData;

public interface HomeService {
    HomeIndexData getIndexData();
}
