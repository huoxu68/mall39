package com.cskaoyan.service;

import com.cskaoyan.bean.BaseStatistics;
import com.cskaoyan.bean.GoodsStatistics;
import com.cskaoyan.bean.OrderStatistics;
import com.cskaoyan.bean.UserStatistics;

public interface StatService {
    BaseStatistics<UserStatistics> queryUserStat();

    BaseStatistics<OrderStatistics> queryOrderStat();

    BaseStatistics<GoodsStatistics> queryGoodsStat();
}
