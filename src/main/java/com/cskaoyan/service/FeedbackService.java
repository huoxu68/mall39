package com.cskaoyan.service;

import com.cskaoyan.bean.BaseData;
import com.cskaoyan.bean.Feedback;
import com.cskaoyan.bean.param.BaseParam;

public interface FeedbackService {
    BaseData<Feedback> query(BaseParam baseParam, String username, Integer id);
}
