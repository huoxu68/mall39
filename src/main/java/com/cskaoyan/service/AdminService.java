package com.cskaoyan.service;

import com.cskaoyan.bean.Admin;
import com.cskaoyan.bean.BaseData;
import com.cskaoyan.bean.MarketAdmin;
import com.cskaoyan.bean.param.BaseParam;

public interface AdminService {

    //查询显示用户
    BaseData<Admin> queryAdmins(BaseParam baseParam, String username);

    Integer addAdmin(Admin admin);

    MarketAdmin updateAdmin(MarketAdmin admin);

    Integer deleteAdmin(MarketAdmin admin);
}
