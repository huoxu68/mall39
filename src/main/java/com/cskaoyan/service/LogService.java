package com.cskaoyan.service;

import com.cskaoyan.bean.BaseData;
import com.cskaoyan.bean.Log;
import com.cskaoyan.bean.MarketLog;
import com.cskaoyan.bean.param.BaseParam;

public interface LogService {

    BaseData<MarketLog> queryLogs(BaseParam baseParam, String adminName);

}
