package com.cskaoyan.service;

import com.cskaoyan.bean.MarketCart;
import com.cskaoyan.bean.wx_cart_index.*;

public interface CartService {
    //向购物车添加货品
    Integer addGoods(MarketCart cart);

    //返回购物车物品总数（包含选中还有没有选中的）
    Integer queryCounts();

    //获得购物车的index界面
    Cart queryCart();


    //改变checked状态
    Cart updateChecked(CheckedBo checkedBo);

    int updateGoodsNum(UpdateGoodsBo updateGoodsBo);

    CheckoutVO queryCheckout(CheckoutBO checkoutBO);

    //直接购买
    Integer fastAdd(MarketCart cart);

    Cart dalete(Integer[] pIds);
}
