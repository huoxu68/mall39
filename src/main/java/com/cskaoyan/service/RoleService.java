package com.cskaoyan.service;

import com.cskaoyan.bean.*;
import com.cskaoyan.bean.role.bo.CreateBo;
import com.cskaoyan.bean.role.bo.PermissionBo;
import com.cskaoyan.bean.role.bo.UpdateBo;
import com.cskaoyan.bean.role.vo.CreateVo;
import com.cskaoyan.bean.param.BaseParam;
import com.cskaoyan.bean.vo.RoleInAdmin;

/**
 * @author zxy
 * @version 1.0.0
 * @ClassName RoleService.java
 * @Description TODO
 * @createTime 2022年06月04日 16:36:00
 */


public interface RoleService {

    BaseRespVo<MarketRole> createRole(CreateBo createBo);

    void deleteRole(CreateVo createVo);

    void updateRole(UpdateBo updateBo);

    BaseData<RoleInAdmin> queryRole();

    BaseData<MarketRole> selectRole(BaseParam baseParam, String name);

    InfoData queryRolesByIds(MarketAdmin primaryPrincipal);

    BaseRespVo getpermissions(Integer role);

    BaseRespVo insertpermissions(PermissionBo permissionBo);



}
