package com.cskaoyan.service;

public interface ProfileService {
    Boolean updatePassword(String oldPassword, String newPassword);
}
