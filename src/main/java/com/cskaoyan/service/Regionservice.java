package com.cskaoyan.service;

import com.cskaoyan.bean.BaseData;
import com.cskaoyan.bean.region.ProvinceListOfDataOfRegionVO;

public interface Regionservice {
    BaseData<ProvinceListOfDataOfRegionVO> selectAllRegion();
}
