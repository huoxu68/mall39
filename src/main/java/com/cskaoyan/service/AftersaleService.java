package com.cskaoyan.service;

import com.cskaoyan.bean.MarketAftersale;
import com.cskaoyan.bean.vo.AftersaleDetailDateVo;

public interface AftersaleService {

    Integer addSubmitAftersale(MarketAftersale marketAftersale);

    AftersaleDetailDateVo detailAftersale(Integer orderId);

}
