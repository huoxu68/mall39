package com.cskaoyan.service;

import com.cskaoyan.bean.BaseData;
import com.cskaoyan.bean.MarketBrand;
import com.cskaoyan.bean.brand.BrandList;
import com.cskaoyan.bean.param.BaseParam;

/**
 * @author jcp
 * @Description
 * @date 2022年06月04日 19:56
 */
public interface BrandService {
    //显示所有brand
    BaseData<BrandList> queryAll(Integer id ,String name);

    //逻辑删除 把deleted 改成1
    void deleteById(Integer id);

    //update brand
    BrandList updateBrand(BrandList brandList);
    //  增加新的brand
    MarketBrand insertNewBrand(MarketBrand marketBrand);


    // wx小程序
    BaseData<MarketBrand> list(BaseParam baseParam);

    MarketBrand detail(Integer id);
}
