package com.cskaoyan.service;

import com.cskaoyan.bean.*;
import com.cskaoyan.bean.param.BaseParam;
import com.cskaoyan.bean.vo.CommonCoupon;
import com.cskaoyan.bean.vo.MyCoupon;

import java.util.List;

public interface CouponService {
    BaseData<MarketCoupon> list(BaseParam baseParam, String name, Short type, Short status);

    MarketCoupon update(MarketCoupon coupon);

    MarketCoupon create(MarketCoupon coupon);

    void delete(MarketCoupon coupon);

    BaseData<MarketCouponUser> listuser(BaseParam baseParam, Integer couponId, Integer userId, Short status);

    MarketCoupon read(Integer id);

    BaseRespVo<BaseData> couponListByUser(MarketUser previousPrincipals, Integer status, Integer page, Integer limit);

    BaseRespVo<BaseData<CommonCoupon>> wxListByType(Integer limit, Integer page, MarketUser user);

    BaseRespVo receiveCoupon(Integer couponId, MarketUser user);

    BaseRespVo selectCouponList(Integer cartId);

    Boolean exchange(Object code, MarketUser user);


}
