package com.cskaoyan.service;

import com.cskaoyan.bean.*;
import com.cskaoyan.bean.BaseData;
import com.cskaoyan.bean.BaseRespVo;
import com.cskaoyan.bean.MarketComment;
import com.cskaoyan.bean.MarketOrder;
import com.cskaoyan.bean.bo.AftersaleSubmitBo;
import com.cskaoyan.bean.bo.OrderCommentBo;
import com.cskaoyan.bean.param.BaseParam;
import com.cskaoyan.bean.param.OrderListParam;
import com.cskaoyan.bean.po.ShipChannel;
import com.cskaoyan.bean.vo.OrderDetailInfoVO;
import com.cskaoyan.bean.vo.OrderDetailOfWx;
import com.cskaoyan.bean.vo.OrderListVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface OrderService {
    BaseRespVo<OrderDetailInfoVO> detailInfo(Integer id);

    BaseRespVo<BaseData<OrderListVO>> listByParams(BaseParam param, OrderListParam orderListParam);

    void refundById(Integer orderId, Double refundMoney);

    List<ShipChannel> getChanel();

    void ship(MarketOrder order, Integer orderId);

    // 评论的回复
    Boolean replyComment(MarketComment marketComment);

    /**
     * 小程序订单相关方法
     *
     * @author FanLujia
     * @since 2022/06/07 10:53
     */

    BaseData listAllByShowType(Short orderStatus, Integer page, Integer limit);

    OrderDetailOfWx orderAllDetileByOrderId(Integer orderId);

    Integer refundByIdOfWx(Integer orderId);

    Integer deleteByIdOfWx(Integer orderId);

    Integer confirmByIdOfWx(Integer orderId);

    MarketOrderGoods goodsOrderInComment(Integer orderId, Integer goodsId);

    // wx 小程序端 订单中的评价
    void orderComment(OrderCommentBo orderCommentBo);

    BaseRespVo addOrder(Map map, MarketUser user);

    void payById(Integer orderId);
}
