package com.cskaoyan.service;

import com.cskaoyan.bean.BaseData;
import com.cskaoyan.bean.MarketTopic;
import com.cskaoyan.bean.param.BaseParam;
import com.cskaoyan.bean.vo.TopicReadDataVo;

public interface TopicService {
    BaseData<MarketTopic> list(BaseParam baseParam, String title, String subtitle);

    MarketTopic insertTopic(MarketTopic marketTopic);

    MarketTopic updateTopic(MarketTopic marketTopic);

    Integer deleteTopic(MarketTopic marketTopic);

    TopicReadDataVo readTopic(Integer topicId);

}
