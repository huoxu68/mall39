package com.cskaoyan.service;

import com.cskaoyan.bean.SystemConfig;

/**
 * @author zxy
 * @version 1.0.0
 * @ClassName ConfigService.java
 * @Description TODO
 * @createTime 2022年06月05日 21:01:00
 */
public interface ConfigService {

    SystemConfig selectAllConfig();

    Boolean updateConfig(SystemConfig config);
}
