package com.cskaoyan.service;

import com.cskaoyan.bean.BaseData;
import com.cskaoyan.bean.Collect;
import com.cskaoyan.bean.param.BaseParam;
import com.cskaoyan.bean.vo.CollectVo;

public interface CollectService {
    BaseData<Collect> query(BaseParam baseParam, Integer userId, Integer valueId);

    void addordelete(Byte type, Integer valueId, Integer userId);

    BaseData<CollectVo> list(BaseParam baseParam, Byte type, Integer id);
}
