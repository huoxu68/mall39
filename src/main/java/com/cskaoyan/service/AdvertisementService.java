package com.cskaoyan.service;

import com.cskaoyan.bean.BaseData;
import com.cskaoyan.bean.MarketAd;
import com.cskaoyan.bean.param.BaseParam;

public interface AdvertisementService {


    BaseData<MarketAd> adlList(BaseParam baseParam, String name, String content);

    MarketAd createAd(MarketAd advertisement);

    MarketAd update(MarketAd advertisement);

    void delete(MarketAd advertisement);

}
