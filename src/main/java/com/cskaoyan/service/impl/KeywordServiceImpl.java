package com.cskaoyan.service.impl;

import com.cskaoyan.bean.BaseData;
import com.cskaoyan.bean.BaseRespVo;
import com.cskaoyan.bean.MarketKeyword;
import com.cskaoyan.bean.MarketKeywordExample;
import com.cskaoyan.bean.param.BaseParam;
import com.cskaoyan.mapper.MarketKeywordMapper;
import com.cskaoyan.service.KeywordService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author zhongyimin
 * @since 2022/06/05 19:50
 */
@Service
public class KeywordServiceImpl implements KeywordService {

    @Autowired
    MarketKeywordMapper mapper;

    @Override
    public BaseRespVo queryByKeywordAndUrl(BaseParam param, String keyword, String url) {
        MarketKeywordExample example = new MarketKeywordExample();
        MarketKeywordExample.Criteria criteria = example.createCriteria();
        if (keyword != null && !"".equals(keyword)) {
            criteria.andKeywordLike("%" + keyword + "%");
        }
        if (url != null && !"".equals(url)) {
            criteria.andUrlLike("%" + url + "%");
        }
        criteria.andDeletedEqualTo(false);
        PageHelper.startPage(param.getPage(),param.getLimit());
        List<MarketKeyword> list = mapper.selectByExample(example);

        PageInfo<MarketKeyword> marketKeywordPageInfo = new PageInfo<>(list);
        BaseData<MarketKeyword> keywordsData = new BaseData<>();
        keywordsData.setTotal((int) marketKeywordPageInfo.getTotal());
        keywordsData.setPages(marketKeywordPageInfo.getPages());
        keywordsData.setPage(marketKeywordPageInfo.getPageNum());
        keywordsData.setLimit(marketKeywordPageInfo.getPageSize());
        keywordsData.setList(list);


        return BaseRespVo.ok(keywordsData);
    }

    @Override
    public MarketKeyword create(MarketKeyword marketKeyword) {
        if (marketKeyword.getIsDefault() == true){
            //如果新增一个默认，需要判断数据库默认的记录
            MarketKeywordExample example = new MarketKeywordExample();
            MarketKeywordExample.Criteria criteria = example.createCriteria();
            criteria.andIsDefaultEqualTo(true);
            List<MarketKeyword> list = mapper.selectByExample(example);
            if (list.size() != 0){
                ArrayList<Integer> list1 = new ArrayList<>();
                for (MarketKeyword keyword : list) {
                    list1.add(keyword.getId());
                }
                // 存在默认的 将已有的默认的全部设为0
                MarketKeyword record = new MarketKeyword();
                record.setIsDefault(false);
                MarketKeywordExample example1 = new MarketKeywordExample();
                MarketKeywordExample.Criteria criteria1 = example1.createCriteria();
                criteria1.andIdIn(list1);
                mapper.updateByExampleSelective(record, example1);

            }

        }
        Date date = new Date();
        marketKeyword.setAddTime(date);
        marketKeyword.setUpdateTime(date);
        marketKeyword.setDeleted(false);
        marketKeyword.setSortOrder(100);
        mapper.insert(marketKeyword);
        return marketKeyword;
    }

    @Override
    public MarketKeyword update(MarketKeyword marketKeyword) {
        if (marketKeyword.getIsDefault() == true){
            //如果新增一个默认，需要判断数据库默认的记录
            MarketKeywordExample example = new MarketKeywordExample();
            MarketKeywordExample.Criteria criteria = example.createCriteria();
            criteria.andIsDefaultEqualTo(true);
            List<MarketKeyword> list = mapper.selectByExample(example);
            if (list.size() != 0){
                ArrayList<Integer> list1 = new ArrayList<>();
                for (MarketKeyword keyword : list) {
                    list1.add(keyword.getId());
                }
                // 存在默认的 将已有的默认的全部设为0
                MarketKeyword record = new MarketKeyword();
                record.setIsDefault(false);
                MarketKeywordExample example1 = new MarketKeywordExample();
                MarketKeywordExample.Criteria criteria1 = example1.createCriteria();
                criteria1.andIdIn(list1);
                mapper.updateByExampleSelective(record, example1);

            }

        }
        marketKeyword.setUpdateTime(new Date());
        mapper.updateByPrimaryKeySelective(marketKeyword);
        return marketKeyword;
    }

    @Override
    public void delete(MarketKeyword marketKeyword) {
        marketKeyword.setDeleted(true);
        marketKeyword.setUpdateTime(new Date());
        mapper.updateByPrimaryKeySelective(marketKeyword);

    }
}
