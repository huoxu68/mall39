package com.cskaoyan.service.impl;

import com.cskaoyan.bean.*;
import com.cskaoyan.bean.bo.OrderCommentBo;
import com.cskaoyan.bean.param.BaseParam;
import com.cskaoyan.bean.param.OrderListParam;
import com.cskaoyan.bean.po.OrderGoods;
import com.cskaoyan.bean.po.ShipChannel;
import com.cskaoyan.bean.vo.*;
import com.cskaoyan.mapper.*;
import com.cskaoyan.service.OrderService;
import com.cskaoyan.util.HandleOptionSettingUtile;
import com.cskaoyan.util.ObjUtils;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * @author zhongyimin
 * @since 2022/06/04 18:17
 */
@Service
public class OrderServiceImpl implements OrderService {
    @Autowired
    OrderMapper orderMapper;
    @Autowired
    UserMapper userMapper;

    @Autowired
    MarketOrderMapper marketOrderMapper;

    @Autowired
    MarketOrderGoodsMapper marketOrderGoodsMapper;

    @Autowired
    MarketCommentMapper marketCommentMapper;

    @Autowired
    MarketCartMapper marketCartMapper;
    @Autowired
    MarketAddressMapper marketAddressMapper;
    @Autowired
    MarketSystemMapper marketSystemMapper;

    @Autowired
    MarketCouponMapper marketCouponMapper;

    @Autowired
    MarketCouponUserMapper marketCouponUserMapper;

    @Override
    public BaseRespVo<OrderDetailInfoVO> detailInfo(Integer id) {
        List<OrderGoods> orderGoods = orderMapper.queryOrderGoodsByOrderId(id);

        OrderVO order = orderMapper.queryOrderById(id);
        if (order == null||orderGoods.size() == 0){
            // 代表记录查不到
            UserInOrder userInOrder = new UserInOrder();
            userInOrder.setNickname("");
            userInOrder.setAvatar("");
            OrderDetailInfoVO orderDetailInfoVO = new OrderDetailInfoVO();
            orderDetailInfoVO.setOrder(null);
            orderDetailInfoVO.setOrderGoods(orderGoods);
            orderDetailInfoVO.setUser(userInOrder);
            return BaseRespVo.invalidParameter("not found");
        }
        //再根据userId查询
        User user = userMapper.queryById(order.getUserId());
        UserInOrder userInOrder = new UserInOrder();
        if (user != null) {
            userInOrder.setAvatar(user.getAvatar());
            userInOrder.setNickname(user.getNickname());
        }
        OrderDetailInfoVO orderDetailInfoVO = new OrderDetailInfoVO();
        orderDetailInfoVO.setOrder(order);
        orderDetailInfoVO.setOrderGoods(orderGoods);
        orderDetailInfoVO.setUser(userInOrder);
        return BaseRespVo.ok(orderDetailInfoVO);
    }

    @Override
    public BaseRespVo<BaseData<OrderListVO>> listByParams(BaseParam param, OrderListParam orderListParam) {
        PageHelper.startPage(param.getPage(), param.getLimit());
        // 先查符合条件的orderList
        String order = param.getOrder();
        String sort = param.getSort();
        Date[] timeArray = orderListParam.getTimeArray();
        Date start = orderListParam.getStart();
        Date end = orderListParam.getEnd();
        Integer userId = orderListParam.getUserId();
        Integer orderId = orderListParam.getOrderId();
        String orderSn = orderListParam.getOrderSn();
        List<Integer> orderStatusArray = orderListParam.getOrderStatusArray();
        List<OrderListVO> orderListVOS = orderMapper.queryOrdersByParams(order, sort, start, end, orderSn, orderStatusArray, userId);
        BaseData<OrderListVO> listVOBaseData = new BaseData<>();
        listVOBaseData.setList(orderListVOS);

        PageInfo<OrderListVO> orderListVOPageInfo = new PageInfo<>(orderListVOS);
        listVOBaseData.setList(orderListVOS);
        listVOBaseData.setPage(orderListVOPageInfo.getPageNum());
        listVOBaseData.setLimit(orderListVOPageInfo.getPageSize());
        listVOBaseData.setPages(orderListVOPageInfo.getPages());
        listVOBaseData.setTotal((int) orderListVOPageInfo.getTotal());

        return BaseRespVo.ok(listVOBaseData);
    }

    @Override
    public void refundById(Integer orderId, Double refundMoney) {
        Date refundTime = new Date();

        orderMapper.refundById(orderId, refundMoney, refundTime);
    }


    @Override
    public List<ShipChannel> getChanel() {
        List<ShipChannel> channels = orderMapper.queryChannels();
        return channels;
    }

    @Override
    public void ship(MarketOrder order, Integer orderId) {
        MarketOrderExample marketOrderExample = new MarketOrderExample();
        MarketOrderExample.Criteria criteria = marketOrderExample.createCriteria();
        criteria.andIdEqualTo(orderId);
        order.setOrderStatus((short) 301);
        marketOrderMapper.updateByExampleSelective(order, marketOrderExample);
    }

    /**
     * 回复评论, 根据传入的id 查询是否已有评论
     *
     * @param marketComment
     */
    @Override
    public Boolean replyComment(MarketComment marketComment) {
        // 数据库中查询数据，判断是否有评论
        String addContent = orderMapper.selectAddCommentById(marketComment.getId());
        if (!StringUtils.isEmpty(addContent)) {
            return true;
        }
        marketCommentMapper.updateByPrimaryKeySelective(marketComment);
        return false;
    }

    /**
     * 小程序订单相关操作
     * @author FanLujia
     * @since 2022/06/07 10:57
     */

    @Override
    public BaseData listAllByShowType(Short orderStatus,Integer page,Integer limit) {
        // showType=0&page=1&limit=10
        // 订单状态 用数组接收
        // Integer[] statusMult = new Integer[2];
        // if (orderStatus == 1){
        //     statusMult[0] = 101;
        // } else if (orderStatus == 2){
        //     statusMult[0] = 201;
        // } else if (orderStatus == 3){
        //     statusMult[0] = 301;
        // } else if (orderStatus == 4){
        //     statusMult[0] = 401;
        //     statusMult[1] = 402;
        // }

        //用list接收
        List<Integer> statusMult = new LinkedList<>();
        if (orderStatus == 1){
            statusMult.add(101);
        } else if (orderStatus == 2){
            statusMult.add(201);
        } else if (orderStatus == 3){
            statusMult.add(301);
        } else if (orderStatus == 4){
            statusMult.add(401);
            statusMult.add(402);
        }
        //获取userId
        Subject subject = SecurityUtils.getSubject();
        MarketUser primaryPrincipal = (MarketUser) subject.getPrincipals().getPrimaryPrincipal();
        Integer userId = primaryPrincipal.getId();

        PageHelper.startPage(page,limit);
        List<OrderInfoOfDetileOfWx> orderList = marketOrderMapper.selectOrderAllByShowType(statusMult,userId);
        for (OrderInfoOfDetileOfWx orderInfo : orderList) {
            //设置权限显示类的数据
            HandleOption handleOption = HandleOptionSettingUtile.setHandleOption(orderInfo);
            orderInfo.setHandleOption(handleOption);
            if (orderInfo.getGrouponPrice() != null && orderInfo.getGrouponPrice().doubleValue() > 0){
                orderInfo.setIsGroupin(true);
            } else {
                orderInfo.setIsGroupin(false);
            }
            orderInfo.setOrderStatus(null);
            //处理显示为null的情况，赋值为0
            if (orderInfo.getActualPrice() == null){
                orderInfo.setActualPrice(BigDecimal.valueOf(0));
            }
            if (orderInfo.getFreightPrice() == null){
                orderInfo.setFreightPrice(BigDecimal.valueOf(0));
            }

        }

        PageInfo<OrderListOfWx> pageInfo = new PageInfo(orderList);
        Integer total = ((int) pageInfo.getTotal());
        int pages = pageInfo.getPages();

        //OrderListOfWx返回的类
        BaseData<OrderInfoOfDetileOfWx> orderBaseData = new BaseData<>();
        orderBaseData.setLimit(limit);
        orderBaseData.setPage(page);
        orderBaseData.setPages(pages);
        orderBaseData.setTotal(total);
        orderBaseData.setList(orderList);

        return orderBaseData;
    }

    @Override
    public OrderDetailOfWx orderAllDetileByOrderId(Integer orderId) {
        //订单信息
        OrderInfoOfDetileOfWx orderInfo = orderMapper.queryOrderInfoById(orderId);

        HandleOption handleOption = HandleOptionSettingUtile.setHandleOption(orderInfo);
        orderInfo.setHandleOption(handleOption);

        //处理前端显示为null的情况，赋值为0
        if (orderInfo.getActualPrice() == null){
            orderInfo.setActualPrice(BigDecimal.valueOf(0));
        }
        if (orderInfo.getFreightPrice() == null){
            orderInfo.setFreightPrice(BigDecimal.valueOf(0));
        }

        //获取订单id查询商品信息
        List<OrderGoods> orderGoods = orderMapper.queryOrderGoodsByOrderId(orderId);

        //封装Vo类
        OrderDetailOfWx orderDetailOfWx = new OrderDetailOfWx();
        orderDetailOfWx.setOrderGoods(orderGoods);
        orderDetailOfWx.setOrderInfo(orderInfo);
        orderDetailOfWx.setExpressInfo(null);

        return orderDetailOfWx;
    }

    //申请退款
    @Override
    public Integer refundByIdOfWx(Integer orderId) {
        MarketOrder marketOrder = marketOrderMapper.selectByPrimaryKey(orderId);
        BigDecimal actualPrice = marketOrder.getActualPrice();
        double actualPriceValue = actualPrice.doubleValue();
        try {
            orderMapper.refundById(orderId,actualPriceValue,new Date());
            return 200;
        }catch (Exception e){
            return 400;
        }

    }

    @Override
    public Integer deleteByIdOfWx(Integer orderId) {
        Integer affactRows = orderMapper.updateDeletedByOrderId(orderId,new Date());
        return affactRows;

    }

    @Override
    public Integer confirmByIdOfWx(Integer orderId) {
        //更改订单状态、确认收货时间、更新时间
        Integer affactRows = orderMapper.updateConfirmByOrderId(orderId,new Date());
        //
        return affactRows;

    }

    @Override
    public MarketOrderGoods goodsOrderInComment(Integer orderId, Integer goodsId) {
        MarketOrderGoodsExample orderGoodsExample = new MarketOrderGoodsExample();
        MarketOrderGoodsExample.Criteria criteria = orderGoodsExample.createCriteria();
        criteria.andOrderIdEqualTo(orderId);
        criteria.andGoodsIdEqualTo(goodsId);
        List<MarketOrderGoods> orderGoodsList = marketOrderGoodsMapper.selectByExample(orderGoodsExample);
        return orderGoodsList.get(0);
    }

    /**
     * 订单中的评价
     * @param orderCommentBo
     */
    @Override
    public void orderComment(OrderCommentBo orderCommentBo) {
        // 通过order_goodsId 找到 goodsId
        MarketOrderGoods marketOrderGoods1 = orderMapper.selectOrderGoodsOfGoodsIdById(orderCommentBo.getOrderGoodsId());
         Integer goodsId = marketOrderGoods1.getGoodsId();
         Integer orderId = marketOrderGoods1.getOrderId();
        // 获取userid
        Subject subject = SecurityUtils.getSubject();
        MarketUser primaryPrincipal = (MarketUser) subject.getPrincipals().getPrimaryPrincipal();
        Integer userId = primaryPrincipal.getId();
        // 存储评论信息
        MarketComment marketComment = new MarketComment();
        marketComment.setDeleted(false);
        marketComment.setValueId(goodsId);
        marketComment.setUpdateTime(new Date());
        marketComment.setAddTime(new Date());
        marketComment.setUserId(userId);
        marketComment.setContent(orderCommentBo.getContent());
        if(orderCommentBo.getPicUrls().length>0){
            marketComment.setHasPicture(true);
        }else{
            marketComment.setHasPicture(false);
        }
        marketComment.setPicUrls(orderCommentBo.getPicUrls());
        marketComment.setStar(orderCommentBo.getStar());
        marketComment.setType((byte) 0);
        marketCommentMapper.insertSelective(marketComment);
        // 根据订单id 去修改订单中的信息
        MarketOrderGoods marketOrderGoods = new MarketOrderGoods();
        marketOrderGoods.setId(orderCommentBo.getOrderGoodsId());
        marketOrderGoods.setComment(marketComment.getId());
        marketOrderGoods.setUpdateTime(new Date());
        marketOrderGoodsMapper.updateByPrimaryKeySelective(marketOrderGoods);


        // 通过orderId 查询order表中的comments 数量 ，
        Integer comments = orderMapper.selectOrderOfCommentsByorderId(orderId);
        // 将得到comments-1 后   重新修改
        MarketOrder marketOrder = new MarketOrder();
        marketOrder.setUpdateTime(new Date());
        marketOrder.setComments((short) (comments-1));
        marketOrder.setId(orderId);
        marketOrderMapper.updateByPrimaryKeySelective(marketOrder);
    }

    @Override
    public BaseRespVo addOrder(Map map, MarketUser user) {
        Integer addressId = (Integer) map.get("addressId");
        Integer couponId = (Integer) map.get("couponId");
        Integer userCouponId = (Integer) map.get("userCouponId");
        String message = (String) map.get("message");
        Integer cartId = (Integer) map.get("cartId");
        // 根据用户编号和选中状态查询购物车
        MarketCartExample example = new MarketCartExample();
        MarketCartExample.Criteria criteria = example.createCriteria();
        criteria.andUserIdEqualTo(user.getId());
        criteria.andCheckedEqualTo(true);
        criteria.andDeletedEqualTo(false);

        List<MarketCart> marketCarts = new LinkedList<>();
        if (cartId == 0) {
            marketCarts = marketCartMapper.selectByExample(example);
        }else {
            MarketCart marketCart = marketCartMapper.selectByPrimaryKey(cartId);
            marketCarts.add(marketCart);
        }
        Double goodsPrice = 0.0;
        for (MarketCart marketCart : marketCarts) {
            double v = marketCart.getPrice() * marketCart.getNumber();
            goodsPrice += v;
        }
        MarketAddress marketAddress = marketAddressMapper.selectByPrimaryKey(addressId);
        // 查最低减免运费的最低商品金额
        MarketSystemExample example1 = new MarketSystemExample();
        MarketSystemExample.Criteria criteria1 = example1.createCriteria();
        criteria1.andKeyNameEqualTo("market_express_freight_min");
        List<MarketSystem> marketSystems = marketSystemMapper.selectByExample(example1);
        Integer minMoney = 0;
        if (marketSystems.size() == 1) {
            MarketSystem marketSystem = marketSystems.get(0);
            minMoney = Integer.valueOf(marketSystem.getKeyValue());
        }
        // 查询运费
        MarketSystemExample example2 = new MarketSystemExample();
        MarketSystemExample.Criteria criteria2 = example2.createCriteria();
        criteria2.andKeyNameEqualTo("market_express_freight_value");
        List<MarketSystem> marketSystems1 = marketSystemMapper.selectByExample(example2);
        Integer freightPrice = 0;

        if (marketSystems1.size() == 1) {
            MarketSystem marketSystem = marketSystems1.get(0);
            if (goodsPrice < minMoney) {
                freightPrice = Integer.valueOf(marketSystem.getKeyValue());
            }
        }

        // 查询优惠券信息
        Integer couponPrice = 0;
        MarketCoupon marketCoupon;
        if (couponId != -1 && couponId != 0){
            marketCoupon = marketCouponMapper.selectByPrimaryKey(couponId);
            BigDecimal discount = marketCoupon.getDiscount();
            couponPrice = discount.intValue();
        }
        MarketOrder record = new MarketOrder();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd");
        Date date = new Date();
        String format = simpleDateFormat.format(date);
        Random random = new Random();
        String fourRandom = random.nextInt(10000) + "";
        int randLength = fourRandom.length();
        if (randLength < 4) {
            for (int i = 1; i <= 4 - randLength; i++)
                fourRandom = "0" + fourRandom;
        }
        String orderSn = format + user.getId() + fourRandom;
        Double orderPrice = 0.0;
        orderPrice = goodsPrice + freightPrice - couponPrice;
        record.setUserId(user.getId());
        record.setOrderSn(orderSn);
        record.setOrderStatus((short) 201);
        record.setConsignee(marketAddress.getName());
        record.setMobile(marketAddress.getTel());
        String address = marketAddress.getProvince() + marketAddress.getCity() + marketAddress.getCounty() + marketAddress.getAddressDetail();

        record.setAddress(address);
        if ("".equals(message)) {
            record.setMessage(null);
        }
        record.setGoodsPrice(BigDecimal.valueOf(goodsPrice));
        record.setFreightPrice(BigDecimal.valueOf(freightPrice));
        record.setCouponPrice(BigDecimal.valueOf(couponPrice));
        record.setOrderPrice(BigDecimal.valueOf(orderPrice));
        record.setActualPrice(BigDecimal.valueOf(orderPrice));
        record.setAddTime(date);
        record.setUpdateTime(date);
        record.setComments((short) marketCarts.size());
        BigDecimal integralPrice = new BigDecimal(0);
        record.setIntegralPrice(integralPrice);
        record.setGrouponPrice(integralPrice);

        record.setDeleted(false);
        ObjUtils.emptyStringToNull(record);
        marketOrderMapper.insertSelective(record);

        Integer orderId = record.getId();
        LinkedList<Integer> productsId = new LinkedList<>();
        // 更新market_order_goods表格
        for (MarketCart marketCart : marketCarts) {
            productsId.add(marketCart.getProductId());
            MarketOrderGoods marketOrderGoods = new MarketOrderGoods();
            marketOrderGoods.setOrderId(orderId);
            marketOrderGoods.setGoodsId(marketCart.getGoodsId());
            marketOrderGoods.setGoodsName(marketCart.getGoodsName());
            marketOrderGoods.setGoodsSn(marketCart.getGoodsSn());
            marketOrderGoods.setProductId(marketCart.getProductId());
            Short number = marketCart.getNumber().shortValue();

            marketOrderGoods.setNumber(number);
            marketOrderGoods.setPrice(BigDecimal.valueOf(marketCart.getPrice()));
            marketOrderGoods.setSpecifications(marketCart.getSpecifications());
            marketOrderGoods.setPicUrl(marketCart.getPicUrl());
            marketOrderGoods.setComment(0);
            Date addTime = new Date();
            marketOrderGoods.setAddTime(addTime);
            marketOrderGoods.setUpdateTime(addTime);
            marketOrderGoods.setDeleted(false);

            marketOrderGoodsMapper.insertSelective(marketOrderGoods);

        }
        if (couponId != -1 && couponId != 0) {


            // 更新coupon_user的优惠券使用信息
            MarketCouponUser couponUserRecord = new MarketCouponUser();
            couponUserRecord.setId(userCouponId);
            couponUserRecord.setStatus((short) 1);
            couponUserRecord.setUpdateTime(new Date());
            couponUserRecord.setOrderId(orderId);

            couponUserRecord.setUsedTime(new Date());
            marketCouponUserMapper.updateByPrimaryKeySelective(couponUserRecord);
        }

        MarketCartExample marketCartExample = new MarketCartExample();
        MarketCartExample.Criteria criteria3 = marketCartExample.createCriteria();
        criteria3.andUserIdEqualTo(user.getId());
        criteria3.andProductIdIn(productsId);
        MarketCart record1 = new MarketCart();
        record1.setDeleted(true);
        marketCartMapper.updateByExampleSelective(record1,marketCartExample);
        OrderSubmit orderSubmit = new OrderSubmit();
        orderSubmit.setGrouponLinkId(0);
        orderSubmit.setOrderId(orderId);
        return BaseRespVo.ok(orderSubmit);
    }

    @Override
    public void payById(Integer orderId) {
        // 根据订单编号更改订单的状态

    }


}
