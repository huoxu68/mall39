package com.cskaoyan.service.impl;

import com.cskaoyan.bean.*;
import com.cskaoyan.bean.param.BaseParam;
import com.cskaoyan.mapper.MarketOrderMapper;
import com.cskaoyan.mapper.MarketUserMapper;
import com.cskaoyan.mapper.UserMapper;
import com.cskaoyan.service.UserService;
import com.cskaoyan.util.MD5Utils;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * @ClassName: UserServiceImpl
 * @Description:
 * @Since: 2022/06/03 13:34
 * @Author: ZhangHuixiang
 */

@Service
@Transactional
public class UserServiceImpl implements UserService {

    @Autowired
    UserMapper userMapper;

    @Autowired
    MarketOrderMapper marketOrderMapper;

    @Autowired
    MarketUserMapper marketUserMapper;

    @Override
    public BaseData<User> query(BaseParam baseParam, String username, String mobile) {

        if (!ObjectUtils.isEmpty(username)){
            username = "%" + username + "%";
        }

        if (!ObjectUtils.isEmpty(mobile)) {
            mobile = "%" + mobile + "%";
        }

        PageHelper.startPage(baseParam.getPage(), baseParam.getLimit());
        List<User> list = userMapper.query(baseParam.getSort(), baseParam.getOrder(), username, mobile);
        PageInfo<User> userPageInfo = new PageInfo<>(list);

        BaseData<User> userData = new BaseData<>();
        userData.setTotal((int) userPageInfo.getTotal());
        userData.setPage(userPageInfo.getPageNum());
        userData.setPages(userPageInfo.getPages());
        userData.setLimit(userPageInfo.getPageSize());
        userData.setList(userPageInfo.getList());

        return userData;
    }

    @Override
    public User queryByUserId(Integer id) {
        User user = userMapper.queryById(id);

        return user;
    }

    @Override
    public Integer update(User user) {

        String multiMd5 = null;
        try {
            multiMd5 = MD5Utils.getMultiMd5(user.getPassword(), user.getPassword());
        } catch (Exception e) {
            e.printStackTrace();
        }

        MarketUser marketUser = marketUserMapper.selectByPrimaryKey(user.getId());
        if (user.getPassword().equals(marketUser.getPassword()) || marketUser.getPassword().equals(multiMd5)) {
            user.setPassword(null);
        } else {
            user.setPassword(multiMd5);
        }
        return userMapper.update(user);
    }

    @Override
    public UserIndexData queryOrderCountByUserId() {
        Subject subject = SecurityUtils.getSubject();
        if (subject.getPrincipals() == null) {
            return null;
        }
        MarketUser primaryPrincipal = (MarketUser) subject.getPrincipals().getPrimaryPrincipal();
        Integer userId = primaryPrincipal.getId();
        // 查询未付款状态的订单数
        MarketOrderExample marketOrderExample = new MarketOrderExample();
        MarketOrderExample.Criteria criteria = marketOrderExample.createCriteria();
        criteria.andDeletedEqualTo(false);
        criteria.andUserIdEqualTo(userId);
        criteria.andOrderStatusEqualTo((short) 101);
        Integer unpaid = Math.toIntExact(marketOrderMapper.countByExample(marketOrderExample));

        // 查询未发货状态的订单数
        marketOrderExample = new MarketOrderExample();
        criteria = marketOrderExample.createCriteria();
        criteria.andDeletedEqualTo(false);
        criteria.andUserIdEqualTo(userId);
        criteria.andOrderStatusEqualTo((short) 201);
        Integer unship = Math.toIntExact(marketOrderMapper.countByExample(marketOrderExample));

        // 查询未收货状态的订单数
        marketOrderExample = new MarketOrderExample();
        criteria = marketOrderExample.createCriteria();
        criteria.andDeletedEqualTo(false);
        criteria.andUserIdEqualTo(userId);
        criteria.andOrderStatusEqualTo((short) 301);
        Integer unrecv = Math.toIntExact(marketOrderMapper.countByExample(marketOrderExample));

        // 查询未评论状态的订单数
        Integer uncomment = marketOrderMapper.selectUnCommentCountByUserId(userId);

        UserIndexData userIndexData = new UserIndexData();
        OrderCount orderCount = new OrderCount();
        orderCount.setUnpaid(unpaid);
        orderCount.setUnship(unship);
        orderCount.setUnrecv(unrecv);
        orderCount.setUncomment(uncomment == null ? 0 : uncomment);
        userIndexData.setOrder(orderCount);
        return userIndexData;
    }

    @Override
    public Boolean insertUser(RegisterBo registerBo) {

        MarketUser marketUser = new MarketUser();
        marketUser.setUsername(registerBo.getUsername());
        marketUser.setNickname(registerBo.getUsername());
        marketUser.setPassword(registerBo.getPassword());
        marketUser.setMobile(registerBo.getMobile());
        marketUser.setWeixinOpenid(registerBo.getWxCode());
        marketUser.setAvatar("https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif");

        Byte b = 0;
        marketUser.setStatus(b);
        marketUser.setAddTime(new Date());
        marketUser.setUpdateTime(new Date());
        marketUser.setUserLevel(b);

        int affectedRows = marketUserMapper.insertSelective(marketUser);

        return affectedRows == 1;
    }

    @Override
    public Boolean updatePasswordByMobile(RegisterBo registerBo) {

        MarketUser marketUser = new MarketUser();
        marketUser.setPassword(registerBo.getPassword());

        MarketUserExample userExample = new MarketUserExample();
        MarketUserExample.Criteria criteria = userExample.createCriteria();
        criteria.andDeletedEqualTo(false);
        criteria.andMobileEqualTo(registerBo.getMobile());

        int affectedRows = marketUserMapper.updateByExampleSelective(marketUser, userExample);

        return affectedRows == 1;
    }
}
