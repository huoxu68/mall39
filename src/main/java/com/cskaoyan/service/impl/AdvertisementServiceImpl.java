package com.cskaoyan.service.impl;

import com.cskaoyan.bean.*;
import com.cskaoyan.bean.param.BaseParam;
import com.cskaoyan.mapper.MarketAdMapper;
import com.cskaoyan.service.AdvertisementService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * @ClassName: AdvertisementServiceImpl
 * @Description:
 * @Since: 2022/06/05 20:22
 * @Author: 甯稿叴
 */

@Service
@Transactional
public class AdvertisementServiceImpl implements AdvertisementService {

    @Autowired
    MarketAdMapper marketAdMapper;


    @Override
    public BaseData<MarketAd> adlList(BaseParam baseParam, String name, String content) {
        PageHelper.startPage(baseParam.getPage(), baseParam.getLimit());
        MarketAdExample marketAdExample = new MarketAdExample();
        MarketAdExample.Criteria criteria = marketAdExample.createCriteria();
        if (name != null && name != "") {
            criteria.andNameLike("%" + name + "%");
        }
        if (content != null && content != "") {
            criteria.andContentLike("%" + content + "%");
        }
        criteria.andDeletedEqualTo(false);
        marketAdExample.setOrderByClause(baseParam.getSort() + " " + baseParam.getOrder());

        List<MarketAd> marketAds = marketAdMapper.selectByExample(marketAdExample);

        PageInfo pageInfo = new PageInfo(marketAds);
        Integer total = (int) pageInfo.getTotal();
        int pages = pageInfo.getPages();
        BaseData<MarketAd> baseData = new BaseData<>(total, pages, baseParam.getLimit(), baseParam.getPage(), marketAds);
        return baseData;


    }


    @Override
    public MarketAd createAd(MarketAd advertisement) {
        marketAdMapper.insertSelective(advertisement);
        return advertisement;


    }


    @Override
    public MarketAd update(MarketAd advertisement) {
        advertisement.setUpdateTime(new Date());
        marketAdMapper.updateByPrimaryKeySelective(advertisement);
        return advertisement;


    }


    @Override
    public void delete(MarketAd advertisement) {
        advertisement.setDeleted(true);
        advertisement.setUpdateTime(new Date());
        marketAdMapper.updateByPrimaryKeySelective(advertisement);


    }



}
