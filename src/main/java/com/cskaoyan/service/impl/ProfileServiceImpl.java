package com.cskaoyan.service.impl;

import com.cskaoyan.bean.MarketAdmin;
import com.cskaoyan.mapper.MarketAdminMapper;
import com.cskaoyan.service.ProfileService;
import com.cskaoyan.util.MD5Utils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @ClassName: ProfileServiceImpl
 * @Description:
 * @Since: 2022/06/07 12:35
 * @Author: ZhangHuixiang
 */

@Service
@Transactional
public class ProfileServiceImpl implements ProfileService {

    @Autowired
    MarketAdminMapper marketAdminMapper;

    @Override
    public Boolean updatePassword(String oldPassword, String newPassword) {
        try {
            oldPassword = MD5Utils.getMultiMd5(oldPassword, oldPassword);
            newPassword = MD5Utils.getMultiMd5(newPassword, newPassword);
        } catch (Exception e) {
            e.printStackTrace();
        }

        Subject subject = SecurityUtils.getSubject();
        if (subject.getPrincipals() == null) {
            return false;
        }

        MarketAdmin primaryPrincipal = (MarketAdmin) subject.getPrincipals().getPrimaryPrincipal();
        int affectedRows = 0;
        if (primaryPrincipal.getPassword().equals(oldPassword)) {
            primaryPrincipal.setPassword(newPassword);
            affectedRows = marketAdminMapper.updateByPrimaryKey(primaryPrincipal);
        }

        return affectedRows == 1;
    }
}
