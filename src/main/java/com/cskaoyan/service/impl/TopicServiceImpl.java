package com.cskaoyan.service.impl;

import com.cskaoyan.bean.BaseData;
import com.cskaoyan.bean.MarketGoods;
import com.cskaoyan.bean.MarketTopic;
import com.cskaoyan.bean.MarketTopicExample;
import com.cskaoyan.bean.bo.TopicBo;
import com.cskaoyan.bean.param.BaseParam;
import com.cskaoyan.bean.vo.TopicReadDataVo;
import com.cskaoyan.mapper.MarketGoodsMapper;
import com.cskaoyan.mapper.MarketTopicMapper;
import com.cskaoyan.service.TopicService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @ClassName: TopicServiceImpl
 * @Description:
 * @Since: 2022/06/05 17:42
 * @Author: 甯稿叴
 */

@Service
@Transactional
public class TopicServiceImpl implements TopicService {


    @Autowired
    MarketTopicMapper marketTopicMapper;
    @Autowired
    MarketGoodsMapper marketGoodsMapper;

    @Override
    public BaseData<MarketTopic> list(BaseParam baseParam, String title, String subtitle) {
        PageHelper.startPage(baseParam.getPage(), baseParam.getLimit());
        MarketTopicExample marketTopicExample = new MarketTopicExample();
        MarketTopicExample.Criteria criteria = marketTopicExample.createCriteria();


        if (title != null && title != "") {
            criteria.andTitleLike("%" + title + "%");
        }
        if (subtitle != null && subtitle != "") {
            criteria.andSubtitleLike("%" + subtitle + "%");
        }
        criteria.andDeletedEqualTo(false);
        marketTopicExample.setOrderByClause(baseParam.getSort() + " " + baseParam.getOrder());

        List<MarketTopic> marketTopics = marketTopicMapper.selectByExample(marketTopicExample);

        PageInfo pageInfo = new PageInfo(marketTopics);
        Integer total = (int) pageInfo.getTotal();
        int pages = pageInfo.getPages();
        BaseData<MarketTopic> baseData = new BaseData<>(total, pages, baseParam.getLimit(), baseParam.getPage(), marketTopics);
        return baseData;

    }

    /**
     * topic模块的添加/更改/逻辑删除/更改页面回显
     *
     * @author FanLujia
     * @since 2022/06/06 17:48
     */

    @Override
    public MarketTopic insertTopic(MarketTopic marketTopic) {
        marketTopic.setAddTime(new Date());
        marketTopic.setUpdateTime(new Date());
        int affectRows = marketTopicMapper.insertSelective(marketTopic);
        return marketTopic;

    }

    @Override
    public MarketTopic updateTopic(MarketTopic marketTopic) {

        marketTopic.setUpdateTime(new Date());
        int affectRows = marketTopicMapper.updateByPrimaryKeySelective(marketTopic);
        return marketTopic;

    }

    @Override
    public Integer deleteTopic(MarketTopic marketTopic) {
        marketTopic.setDeleted(true);
        marketTopic.setUpdateTime(new Date());
        marketTopicMapper.updateByPrimaryKeySelective(marketTopic);
        return 200;
    }

    @Override
    public TopicReadDataVo readTopic(Integer topicId) {
        // MarketTopic marketTopic = marketTopicMapper.selectByPrimaryKey(topicId);
        // String goods = marketTopic.getGoods();
        // String substring = goods.substring(goods.indexOf("[") + 1, goods.indexOf("]"));
        // String[] split = substring.split(",");
        // for (String s : split) {
        //     int goodsId = Integer.parseInt(s);
        //     System.out.println(goodsId);
        // }
        TopicBo topicBo = marketTopicMapper.selectById(topicId);
        Integer[] goodsIds = topicBo.getGoods();
        //查询封装的商品数组
        List<MarketGoods> goodsList = new ArrayList<>();
        for (Integer goodsId : goodsIds) {
            MarketGoods marketGoods = marketGoodsMapper.selectByPrimaryKey(goodsId);
            goodsList.add(marketGoods);
        }
        //封装结果
        TopicReadDataVo topicReadDataVo = new TopicReadDataVo();
        topicReadDataVo.setTopic(topicBo);
        topicReadDataVo.setGoodsList(goodsList);
        return topicReadDataVo;
    }


}
