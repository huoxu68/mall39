package com.cskaoyan.service.impl;

import com.cskaoyan.bean.MarketSystem;
import com.cskaoyan.bean.MarketSystemExample;
import com.cskaoyan.bean.SystemConfig;
import com.cskaoyan.mapper.MarketSystemMapper;
import com.cskaoyan.service.ConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

/**
 * @author zxy
 * @version 1.0.0
 * @ClassName ConfigServiceImpl.java
 * @Description TODO
 * @createTime 2022年06月05日 21:01:00
 */
@Service
public class ConfigServiceImpl implements ConfigService {
    @Autowired
    MarketSystemMapper marketSystemMapper;

    @Override
    public SystemConfig selectAllConfig() {
        MarketSystemExample marketSystemExample = new MarketSystemExample();
        MarketSystemExample.Criteria criteria = marketSystemExample.createCriteria();
        criteria.andDeletedEqualTo(false);
        List<MarketSystem> marketSystems = marketSystemMapper.selectByExample(marketSystemExample);
        SystemConfig systemConfig = new SystemConfig();
        Class<? extends SystemConfig> aClass = systemConfig.getClass();
        for (MarketSystem marketSystem : marketSystems) {
            Method declaredMethod = null;
            try {
                declaredMethod = aClass.getDeclaredMethod("set" + marketSystem.getKeyName().substring(0, 1).toUpperCase() + marketSystem.getKeyName().substring(1), String.class);
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            }
            if (declaredMethod != null) {
                try {
                    declaredMethod.invoke(systemConfig, marketSystem.getKeyValue());
                } catch (InvocationTargetException | IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        }
        return systemConfig;
    }

    @Override
    public Boolean updateConfig(SystemConfig config) {
        Class<MarketSystem> marketSystemClass = MarketSystem.class;
        Class<? extends SystemConfig> configClass = config.getClass();
        Field[] declaredFields = configClass.getDeclaredFields();
        List<MarketSystem> marketSystems = new ArrayList<>();
        for (Field declaredField : declaredFields) {
            MarketSystem marketSystem = null;
            try {
                marketSystem = marketSystemClass.newInstance();
            } catch (InstantiationException | IllegalAccessException e) {
                e.printStackTrace();
            }
            if (marketSystem == null) {
                return false;
            }
            declaredField.setAccessible(true);
            String name = declaredField.getName();
            try {
                Method setKeyName = marketSystemClass.getDeclaredMethod("setKeyName", String.class);
                Method setKeyValue = marketSystemClass.getDeclaredMethod("setKeyValue", String.class);
                setKeyName.setAccessible(true);
                setKeyValue.setAccessible(true);
                setKeyName.invoke(marketSystem, name);
                setKeyValue.invoke(marketSystem, declaredField.get(config));
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }
            marketSystems.add(marketSystem);
        }

        for (MarketSystem marketSystem : marketSystems) {
            MarketSystemExample marketSystemExample = new MarketSystemExample();
            MarketSystemExample.Criteria criteria = marketSystemExample.createCriteria();
            criteria.andDeletedEqualTo(false);
            criteria.andKeyNameEqualTo(marketSystem.getKeyName());
            marketSystemMapper.updateByExampleSelective(marketSystem, marketSystemExample);
        }

        return true;
    }
}
