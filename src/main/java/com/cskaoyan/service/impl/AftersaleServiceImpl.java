package com.cskaoyan.service.impl;

import com.cskaoyan.bean.*;
import com.cskaoyan.bean.vo.AftersaleDetailDateVo;
import com.cskaoyan.mapper.MarketAftersaleMapper;
import com.cskaoyan.mapper.MarketOrderGoodsMapper;
import com.cskaoyan.mapper.MarketOrderMapper;
import com.cskaoyan.mapper.OrderMapper;
import com.cskaoyan.service.AftersaleService;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Random;

/**
 * @ClassName: AftersaleServiceImpl
 * @Description: 售后
 * @Since: 2022/06/09 14:44
 * @Author: FanLujia
 */

@Service
public class AftersaleServiceImpl implements AftersaleService {

    @Autowired
    MarketAftersaleMapper marketAftersaleMapper;
    @Autowired
    MarketOrderMapper marketOrderMapper;
    @Autowired
    OrderMapper orderMapper;
    @Autowired
    MarketOrderGoodsMapper marketOrderGoodsMapper;

    @Override
    public Integer addSubmitAftersale(MarketAftersale marketAftersale) {
        //获取userId
        Subject subject = SecurityUtils.getSubject();
        MarketUser primaryPrincipal = (MarketUser) subject.getPrincipals().getPrimaryPrincipal();
        Integer userId = primaryPrincipal.getId();
        marketAftersale.setUserId(userId);

        //获取AfterSale_Sn
        Date date = new Date();
        marketAftersale.setAddTime(date);
        marketAftersale.setUpdateTime(date);
        marketAftersale.setStatus((short) 1);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMddHHmm");
        String format = simpleDateFormat.format(date);
        int num = (int) (Math.random()*10000);
        String sn =format + num;
        marketAftersale.setAftersaleSn(sn);

        //添加售后记录
        Integer affectRows = marketAftersaleMapper.insertSelective(marketAftersale);

        //更改order表aftersale_status
        Integer orderId = marketAftersale.getOrderId();
        orderMapper.updateAftersaleOfOrder((short) 1,orderId,date);

        return affectRows;

    }

    @Override
    public AftersaleDetailDateVo detailAftersale(Integer orderId) {
        Subject subject = SecurityUtils.getSubject();
        MarketUser primaryPrincipal = (MarketUser) subject.getPrincipals().getPrimaryPrincipal();
        Integer userId = primaryPrincipal.getId();

        //订单order数据
        MarketOrder marketOrder = marketOrderMapper.selectByPrimaryKey(orderId);

        //orderGoods数据
        MarketOrderGoodsExample orderGoodsExample = new MarketOrderGoodsExample();
        MarketOrderGoodsExample.Criteria orderGoodsCriteria = orderGoodsExample.createCriteria();
        orderGoodsCriteria.andOrderIdEqualTo(orderId);
        List<MarketOrderGoods> orderGoodsList = marketOrderGoodsMapper.selectByExample(orderGoodsExample);

        //售后数据
        MarketAftersaleExample aftersaleExample = new MarketAftersaleExample();
        MarketAftersaleExample.Criteria criteria = aftersaleExample.createCriteria();
        criteria.andOrderIdEqualTo(orderId);
        criteria.andUserIdEqualTo(userId);
        List<MarketAftersale> marketAftersales = marketAftersaleMapper.selectByExample(aftersaleExample);

        AftersaleDetailDateVo detailDateVo = new AftersaleDetailDateVo();
        //使用IF判断
        // if (marketAftersales.size() != 0){
        //     MarketAftersale aftersale = marketAftersales.get(0);
        //     detailDateVo.setAftersale(aftersale);
        // }else {
        //     detailDateVo.setAftersale(null);
        // }

        try {
            MarketAftersale aftersale = marketAftersales.get(0);
            detailDateVo.setAftersale(aftersale);
        }catch (Exception e){
            System.out.println("aftersale出错，可能为空");
            detailDateVo.setAftersale(null);
        }
        detailDateVo.setOrder(marketOrder);
        detailDateVo.setOrderGoods(orderGoodsList);
        return detailDateVo;

    }


}