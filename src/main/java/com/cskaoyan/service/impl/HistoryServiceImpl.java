package com.cskaoyan.service.impl;

import com.cskaoyan.bean.BaseData;
import com.cskaoyan.bean.SearchHistory;
import com.cskaoyan.bean.param.BaseParam;
import com.cskaoyan.mapper.HistoryMapper;
import com.cskaoyan.service.HistoryService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

import java.util.List;

/**
 * @ClassName: HistoryServiceImpl
 * @Description: 处理所有前缀为/admin/history的请求
 * @Since: 2022/06/04 13:38
 * @Author: ZhangHuixiang
 */

@Service
@Transactional
public class HistoryServiceImpl implements HistoryService {

    @Autowired
    HistoryMapper historyMapper;

    @Override
    public BaseData<SearchHistory> query(BaseParam baseParam, Integer userId, String keyword) {
        if (!ObjectUtils.isEmpty(keyword)) {
            keyword = "%" + keyword + "%";
        }

        PageHelper.startPage(baseParam.getPage(), baseParam.getLimit());

        List<SearchHistory> list = historyMapper.query(baseParam.getSort(), baseParam.getOrder(), userId, keyword);

        PageInfo<SearchHistory> searchHistoryPageInfo = new PageInfo<>(list);
        BaseData<SearchHistory> searchHistoryData = new BaseData<>();
        searchHistoryData.setTotal((int) searchHistoryPageInfo.getTotal());
        searchHistoryData.setPages(searchHistoryPageInfo.getPages());
        searchHistoryData.setPage(searchHistoryPageInfo.getPageNum());
        searchHistoryData.setLimit(searchHistoryPageInfo.getPageSize());
        searchHistoryData.setList(list);

        return searchHistoryData;
    }
}
