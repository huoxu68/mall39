package com.cskaoyan.service.impl;


import com.cskaoyan.bean.*;
import com.cskaoyan.bean.search.vo.HistoryKeyword;
import com.cskaoyan.bean.search.vo.SearchIndexVo;
import com.cskaoyan.mapper.MarketGoodsMapper;
import com.cskaoyan.mapper.MarketGoodsSpecificationMapper;
import com.cskaoyan.mapper.MarketKeywordMapper;
import com.cskaoyan.mapper.MarketSearchHistoryMapper;
import com.cskaoyan.service.SearchService;
import lombok.Data;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @author zxy
 * @version 1.0.0
 * @ClassName SearchSeviceImpl.java
 * @Description TODO
 * @createTime 2022年06月07日 11:14:00
 */
@Service
public class SearchSeviceImpl implements SearchService {
    @Autowired
    MarketKeywordMapper marketKeywordMapper;

    @Autowired
    MarketSearchHistoryMapper marketSearchHistoryMapper;

    @Autowired
    MarketGoodsMapper marketGoodsMapper;

    @Override
    public BaseRespVo getIndex() {


        //求defaultKeyword的返回值
        MarketKeywordExample marketKeywordExample = new MarketKeywordExample();
        MarketKeywordExample.Criteria criteria = marketKeywordExample.createCriteria();
        criteria.andIsDefaultEqualTo(true);
        List<MarketKeyword> marketKeywords = marketKeywordMapper.selectByExample(marketKeywordExample);



        /*
         * 求historyKeywordList的返回值
         * */

        Subject subject = SecurityUtils.getSubject();
        Integer userId = null;
        if (subject.getPrincipals() != null) {
            userId = ((MarketUser)subject.getPrincipals().getPrimaryPrincipal()).getId();
        }
        List<MarketSearchHistory> marketSearchHistories = marketSearchHistoryMapper.selectByUserId(userId);

        List<HistoryKeyword> historyKeywords = new ArrayList<>();
        for (MarketSearchHistory marketSearchHistory : marketSearchHistories) {
            HistoryKeyword historyKeyword = new HistoryKeyword();
            historyKeyword.setKeyword(marketSearchHistory.getKeyword());
            historyKeywords.add(historyKeyword);
        }


        /*
         * 求hotkeywordKList的返回值
         * */
        MarketKeywordExample marketKeywordExample1 = new MarketKeywordExample();
        MarketKeywordExample.Criteria criteria1 = marketKeywordExample1.createCriteria();
        criteria1.andIsHotEqualTo(true);
        List<MarketKeyword> marketKeywords1 = marketKeywordMapper.selectByExample(marketKeywordExample1);


        SearchIndexVo searchIndexVo = new SearchIndexVo();
        searchIndexVo.setDefaultKeyword(marketKeywords.get(0));
        searchIndexVo.setHistoryKeywordList(historyKeywords);
        searchIndexVo.setHotKeywordList(marketKeywords1);


        return BaseRespVo.ok(searchIndexVo);
    }

    @Override
    public void clearhistory() {
        MarketSearchHistoryExample marketSearchHistoryExample = new MarketSearchHistoryExample();
        MarketSearchHistory marketSearchHistory = new MarketSearchHistory();
        marketSearchHistory.setDeleted(true);
        marketSearchHistoryMapper.updateByExampleSelective(marketSearchHistory, marketSearchHistoryExample);

    }

    @Override
    public BaseRespVo helper(String keyword) {
        List<String> strings = new ArrayList<>();


//        MarketGoodsExample marketGoodsExample = new MarketGoodsExample();
        MarketKeywordExample marketKeywordExample = new MarketKeywordExample();


//        MarketGoodsExample.Criteria criteria = marketGoodsExample.createCriteria();
        MarketKeywordExample.Criteria criteria = marketKeywordExample.createCriteria();

        criteria.andKeywordLike("%" + keyword + "%");
        List<MarketKeyword> marketKeywords = marketKeywordMapper.selectByExample(marketKeywordExample);


        for (MarketKeyword marketGood : marketKeywords) {
            strings.add(marketGood.getKeyword());

        }
        return BaseRespVo.ok(strings);
    }
}
