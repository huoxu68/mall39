package com.cskaoyan.service.impl;

import com.cskaoyan.bean.BaseData;
import com.cskaoyan.bean.region.CityOfProvinveOfListOfDataOfRegionVO;
import com.cskaoyan.bean.region.ProvinceListOfDataOfRegionVO;
import com.cskaoyan.mapper.RegionMapper;
import com.cskaoyan.service.Regionservice;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author jcp
 * @Description
 * @date 2022年06月04日 17:15
 */
@Service
@Transactional
public class RegionServiceImpl implements Regionservice {

    @Autowired
    RegionMapper regionMapper;

    @Override
    public BaseData<ProvinceListOfDataOfRegionVO> selectAllRegion() {
        PageHelper.startPage(1, 10000);

        List<ProvinceListOfDataOfRegionVO> provinces = regionMapper.selectAllProvince();

        for (ProvinceListOfDataOfRegionVO province : provinces) {
            province.setChildren(regionMapper.selectCityByPid(province.getId()));
            for (CityOfProvinveOfListOfDataOfRegionVO child : province.getChildren()) {
                child.setChildren(regionMapper.selectAreaByPid(child.getId()));
            }
        }

        PageInfo<ProvinceListOfDataOfRegionVO> provinceListOfDataOfRegionVOPageInfo = new PageInfo<>(provinces);
        BaseData<ProvinceListOfDataOfRegionVO> provinceData = new BaseData<>();

        provinceData.setTotal((int) provinceListOfDataOfRegionVOPageInfo.getTotal());
        provinceData.setPages(provinceListOfDataOfRegionVOPageInfo.getPages());
        provinceData.setPage(provinceListOfDataOfRegionVOPageInfo.getPageNum());
        provinceData.setLimit(provinceListOfDataOfRegionVOPageInfo.getPageSize());
        provinceData.setList(provinces);

        return provinceData;
    }
}
