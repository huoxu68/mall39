package com.cskaoyan.service.impl;

import com.cskaoyan.bean.BaseData;
import com.cskaoyan.bean.MarketStorage;
import com.cskaoyan.bean.MarketStorageExample;
import com.cskaoyan.bean.param.BaseParam;
import com.cskaoyan.bean.storage.bo.UpdateBo;
import com.cskaoyan.mapper.MarketStorageMapper;
import com.cskaoyan.service.StorageService;
import com.cskaoyan.util.OSSFileUploadUtils;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.util.Date;
import java.util.List;

/**
 * @author zxy
 * @version 1.0.0
 * @ClassName StorageServiceImpl.java
 * @Description TODO
 * @createTime 2022年06月05日 16:40:00
 */
@Service
@Transactional
public class StorageServiceImpl implements StorageService {
    @Autowired
    MarketStorageMapper marketStorageMapper;




    @Override
    public BaseData<MarketStorage> queryByKeyAndName(BaseParam baseParam, String key, String name) {
        PageHelper.startPage(baseParam.getPage(),baseParam.getLimit());

        MarketStorageExample marketStorageExample = new MarketStorageExample();
        marketStorageExample.setOrderByClause(baseParam.getSort() + " " + baseParam.getOrder());
        MarketStorageExample.Criteria criteria = marketStorageExample.createCriteria();
        criteria.andDeletedEqualTo(false);
        if(name != null && !"".equals(name)){
            name = "%" + name + "%";
        }
        if (key != null && !"".equals(key)) {
            criteria.andKeyEqualTo(key);
        }
        List<MarketStorage> marketStorages = marketStorageMapper.selectByDeleteAndKey(key,name,baseParam.getSort(),baseParam.getOrder());

        PageInfo<MarketStorage> pageInfo = new PageInfo<>(marketStorages);
        BaseData<MarketStorage> storageBaseData = new BaseData<>();
        storageBaseData.setTotal((int) pageInfo.getTotal());
        storageBaseData.setPages(pageInfo.getPages());
        storageBaseData.setPage(pageInfo.getPageNum());
        storageBaseData.setLimit(pageInfo.getPageSize());
        storageBaseData.setList(marketStorages);

        return storageBaseData;
    }

    @Override
    public MarketStorage insertStorage(MultipartFile file) {
        String contentType = file.getContentType();
        long size = file.getSize();
        String[] upload = OSSFileUploadUtils.upload(file);
        MarketStorage storage = new MarketStorage();
        storage.setKey(file.getOriginalFilename());
        storage.setName(upload[1]);
        storage.setType(contentType);
        storage.setSize((int) size);
        storage.setUrl(upload[0]);
        storage.setAddTime(new Date());
        storage.setUpdateTime(new Date());

        marketStorageMapper.insertSelective(storage);

        return storage;
    }

    @Override
    public MarketStorage updateName(UpdateBo updateBo) {

        MarketStorageExample marketStorageExample = new MarketStorageExample();
        MarketStorageExample.Criteria criteria = marketStorageExample.createCriteria();
        criteria.andIdEqualTo(updateBo.getId());
        MarketStorage marketStorage = new MarketStorage();
//        marketStorage.setUpdateTime(storage.getUpdateTime());
//        marketStorage.setAddTime(storage.getAddTime());
//        marketStorage.setUrl(storage.getUrl());
//        marketStorage.setSize(storage.getSize());
//        marketStorage.setType(storage.getType());
        marketStorage.setName(updateBo.getName());
        marketStorageMapper.updateByExampleSelective(marketStorage,marketStorageExample);
        List<MarketStorage> marketStorages = marketStorageMapper.selectByExample(marketStorageExample);
        return marketStorages.get(0);
    }

    @Override
    public void delete(UpdateBo updateBo) {
        MarketStorageExample marketStorageExample = new MarketStorageExample();
        MarketStorageExample.Criteria criteria = marketStorageExample.createCriteria();
        criteria.andIdEqualTo(updateBo.getId());
        MarketStorage marketStorage = new MarketStorage();
        marketStorage.setDeleted(true);
        marketStorageMapper.updateByExampleSelective(marketStorage,marketStorageExample);
    }
}
