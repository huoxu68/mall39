package com.cskaoyan.service.impl;

import com.cskaoyan.bean.*;
import com.cskaoyan.mapper.*;
import com.cskaoyan.service.ConfigService;
import com.cskaoyan.service.HomeService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @ClassName: HomeServiceImpl
 * @Description: 处理所有前缀为/wx/home的请求
 * @Since: 2022/06/05 15:56
 * @Author: ZhangHuixiang
 */
@Service
@Transactional
public class HomeServiceImpl implements HomeService {

    @Autowired
    MarketGoodsMapper marketGoodsMapper;

    @Autowired
    MarketCouponMapper marketCouponMapper;

    @Autowired
    MarketBrandMapper marketBrandMapper;

    @Autowired
    MarketTopicMapper marketTopicMapper;

    @Autowired
    MarketCategoryMapper marketCategoryMapper;

    @Autowired
    MarketAdMapper marketAdMapper;

    @Autowired
    ConfigService configService;

    @Override
    public HomeIndexData getIndexData() {
        SystemConfig systemConfig = configService.selectAllConfig();

        MarketGoodsExample newGoodsExample = new MarketGoodsExample();
        MarketGoodsExample.Criteria newGoodsCriteria = newGoodsExample.createCriteria();
        newGoodsCriteria.andDeletedEqualTo(false);
        newGoodsCriteria.andIsNewEqualTo(true);
        PageHelper.startPage(1, Integer.parseInt(systemConfig.getMarket_wx_index_new()));

        List<MarketGoods> newGoods = marketGoodsMapper.selectByExample(newGoodsExample);

        MarketCouponExample example = new MarketCouponExample();
        MarketCouponExample.Criteria criteria = example.createCriteria();
        criteria.andDeletedEqualTo(false);
        criteria.andStatusEqualTo((short) 0);
        List<MarketCoupon> marketCoupons = marketCouponMapper.selectByExample(example);

        PageHelper.startPage(1, Integer.parseInt(systemConfig.getMarket_wx_index_brand()));
        List<MarketBrand> marketBrands = marketBrandMapper.selectByExample(new MarketBrandExample());

        newGoodsExample = new MarketGoodsExample();
        newGoodsCriteria = newGoodsExample.createCriteria();
        newGoodsCriteria.andDeletedEqualTo(false);
        newGoodsCriteria.andIsHotEqualTo(true);
        PageHelper.startPage(1, Integer.parseInt(systemConfig.getMarket_wx_index_hot()));

        List<MarketGoods> hotGoods = marketGoodsMapper.selectByExample(newGoodsExample);

        MarketCategoryExample marketCategoryExample = new MarketCategoryExample();
        marketCategoryExample.setOrderByClause("sort_order");
        MarketCategoryExample.Criteria categoryCriteria = marketCategoryExample.createCriteria();
        categoryCriteria.andDeletedEqualTo(false);
        categoryCriteria.andLevelEqualTo("L1");
        PageHelper.startPage(1, Integer.parseInt(systemConfig.getMarket_wx_catlog_list()));

        List<MarketCategory> marketCategories = marketCategoryMapper.selectByExample(marketCategoryExample);

        FloorGoods floorGoods = null;
        if (marketCategories.size() != 0) {
            floorGoods = new FloorGoods();
            floorGoods.setId(marketCategories.get(0).getId());
            floorGoods.setName(marketCategories.get(0).getName());
            marketCategoryExample = new MarketCategoryExample();
            categoryCriteria = marketCategoryExample.createCriteria();
            categoryCriteria.andDeletedEqualTo(false);
            categoryCriteria.andPidEqualTo(floorGoods.getId());
            List<Integer> categoriesList = marketCategoryMapper.selectByExample(marketCategoryExample)
                    .stream()
                    .map(MarketCategory::getId)
                    .collect(Collectors.toList());
            newGoodsExample = new MarketGoodsExample();
            newGoodsCriteria = newGoodsExample.createCriteria();
            newGoodsCriteria.andDeletedEqualTo(false);
            newGoodsCriteria.andCategoryIdIn(categoriesList);
            PageHelper.startPage(1, Integer.parseInt(systemConfig.getMarket_wx_catlog_goods()));
            floorGoods.setGoodsList(marketGoodsMapper.selectByExample(newGoodsExample));
        }
        List<FloorGoods> floorGoodsList = new ArrayList<>();
        floorGoodsList.add(floorGoods);

        PageHelper.startPage(1, Integer.parseInt(systemConfig.getMarket_wx_index_topic()));
        List<MarketTopic> marketTopics = marketTopicMapper.selectByExample(new MarketTopicExample());

        List<MarketAd> marketAds = marketAdMapper.selectByExample(new MarketAdExample());

        HomeIndexData homeIndexData = new HomeIndexData();
        homeIndexData.setNewGoodsList(newGoods);
        homeIndexData.setCouponList(marketCoupons);
        homeIndexData.setBrandList(marketBrands);
        homeIndexData.setHotGoodsList(hotGoods);
        homeIndexData.setTopicList(marketTopics);
        homeIndexData.setChannel(marketCategories);
        homeIndexData.setFloorGoodsList(floorGoodsList);
        homeIndexData.setBanner(marketAds);

        return homeIndexData;
    }
}
