package com.cskaoyan.service.impl;

import com.cskaoyan.bean.BaseData;
import com.cskaoyan.bean.Feedback;
import com.cskaoyan.bean.param.BaseParam;
import com.cskaoyan.mapper.FeedbackMapper;
import com.cskaoyan.service.FeedbackService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.json.JacksonJsonParser;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

import java.util.List;

/**
 * @ClassName: FeedbackServiceImpl
 * @Description: 处理所有前缀为/admin/feedback的请求
 * @Since: 2022/06/04 14:13
 * @Author: ZhangHuixiang
 */

@Service
@Transactional
public class FeedbackServiceImpl implements FeedbackService {

    @Autowired
    FeedbackMapper feedbackMapper;

    @Override
    public BaseData<Feedback> query(BaseParam baseParam, String username, Integer id) {
        if (!ObjectUtils.isEmpty(username)) {
            username = "%" + username + "%";
        }

        PageHelper.startPage(baseParam.getPage(), baseParam.getLimit());

        List<Feedback> list = feedbackMapper.query(baseParam.getSort(), baseParam.getOrder(), username, id);

        PageInfo<Feedback> feedbackPageInfo = new PageInfo<>(list);
        BaseData<Feedback> feedbackData = new BaseData<>();
        feedbackData.setTotal((int) feedbackPageInfo.getTotal());
        feedbackData.setPages(feedbackPageInfo.getPages());
        feedbackData.setPage(feedbackPageInfo.getPageNum());
        feedbackData.setLimit(feedbackPageInfo.getPageSize());
        feedbackData.setList(list);

        return feedbackData;
    }
}
