package com.cskaoyan.service.impl;

import com.cskaoyan.bean.*;
import com.cskaoyan.bean.BaseData;
import com.cskaoyan.bean.BaseRespVo;
import com.cskaoyan.bean.MarketRoleExample;
import com.cskaoyan.bean.MarketRole;
import com.cskaoyan.bean.role.bo.CreateBo;
import com.cskaoyan.bean.role.bo.PermissionBo;
import com.cskaoyan.bean.role.bo.UpdateBo;
import com.cskaoyan.bean.role.po.PermissionDetail;
import com.cskaoyan.bean.role.vo.CreateVo;
import com.cskaoyan.bean.role.vo.PermissionDetailVo;
import com.cskaoyan.bean.param.BaseParam;
import com.cskaoyan.bean.vo.RoleInAdmin;
import com.cskaoyan.mapper.MarketPermissionMapper;
import com.cskaoyan.mapper.MarketRoleMapper;
import com.cskaoyan.mapper.PermissionDetailMapper;
import com.cskaoyan.mapper.RoleMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author zxy
 * @version 1.0.0
 * @ClassName RoleService.java
 * @Description TODO
 * @createTime 2022年06月04日 16:36:00
 */
@Service
@Transactional
public class RoleServiceImpl implements com.cskaoyan.service.RoleService {
    @Autowired
    RoleMapper roleMapper;

    @Autowired
    MarketRoleMapper marketRoleMapper;

    @Autowired
    MarketPermissionMapper marketPermissionMapper;

    @Autowired
    PermissionDetailMapper permissionDetailMapper;

    @Override
    public BaseRespVo<MarketRole> createRole(CreateBo createBo) {

        Date date = new Date();

        MarketRole marketRole = new MarketRole();
        marketRole.setUpdateTime(date);
        marketRole.setAddTime(date);
        marketRole.setDesc(createBo.getDesc());
        marketRole.setName(createBo.getName());

        marketRoleMapper.insertSelective(marketRole);



        return BaseRespVo.ok(marketRole);
    }

    @Override
    public void deleteRole(CreateVo createVo) {

        MarketRoleExample marketRoleExample = new MarketRoleExample();
        MarketRoleExample.Criteria criteria = marketRoleExample.createCriteria();
        criteria.andIdEqualTo(createVo.getId());
        MarketRole marketRole = new MarketRole();
        marketRole.setDeleted(true);
        marketRoleMapper.updateByExampleSelective(marketRole,marketRoleExample);


//       return roleMapper.deleteRole(createVo);




    }

    @Override
    public void updateRole(UpdateBo updateBo) {
        MarketRole marketRole = new MarketRole();



        MarketRoleExample marketRoleExample = new MarketRoleExample();
        MarketRoleExample.Criteria criteria = marketRoleExample.createCriteria();
        criteria.andIdEqualTo(updateBo.getId());

        marketRole.setDesc(updateBo.getDesc());
        marketRole.setName(updateBo.getName());
        marketRoleMapper.updateByExampleSelective(marketRole,marketRoleExample);


    }

    /**
     * admin显示页面需要的角色信息
     * @author FanLujia
     * @since 2022/06/05 19:51
     */
    @Override
    public BaseData<RoleInAdmin> queryRole() {
        PageHelper.startPage(1,500);
        List<RoleInAdmin> roles =  roleMapper.selectRole();
        PageInfo rolePageInfo = new PageInfo(roles);

        long total = rolePageInfo.getTotal();

        BaseData<RoleInAdmin> roleBaseData = new BaseData<>();
        roleBaseData.setPages(1);
        roleBaseData.setLimit((int) total);
        roleBaseData.setTotal((int) total);
        roleBaseData.setPage(1);
        roleBaseData.setList(roles);

        return roleBaseData;

    }

    /**
     * role数据的查询
     * @author FanLujia
     * @since 2022/06/06 15:54
     */@Override
    public BaseData<com.cskaoyan.bean.MarketRole> selectRole(BaseParam baseParam, String name) {

        //获取传进来的order、sort
        String order = baseParam.getOrder();
        String sort = baseParam.getSort();

        //用MarketRoleExample添加查询条件
        MarketRoleExample roleExample = new MarketRoleExample();
        MarketRoleExample.Criteria criteria = roleExample.createCriteria();
        if (name != null && !"".equals(name)) {
            criteria.andNameLike("%" + name + "%");
        }
        criteria.andDeletedEqualTo(false);
        roleExample.setOrderByClause(sort + " " + order);
        //分页并获取查询结果
        PageHelper.startPage(baseParam.getPage(),baseParam.getLimit());
        List<com.cskaoyan.bean.MarketRole> roleList = marketRoleMapper.selectByExample(roleExample);

        PageInfo<com.cskaoyan.bean.MarketRole> rolePageInfo = new PageInfo<>(roleList);
        int pages = rolePageInfo.getPages();
        long total = rolePageInfo.getTotal();
        BaseData<com.cskaoyan.bean.MarketRole> roleBaseData = new BaseData<>();
        roleBaseData.setPage(baseParam.getPage());
        roleBaseData.setLimit(baseParam.getLimit());
        roleBaseData.setTotal((int) total);
        roleBaseData.setPages(pages);
        roleBaseData.setList(roleList);
        return roleBaseData;

    }

    @Override
    public InfoData queryRolesByIds(MarketAdmin primaryPrincipal) {
        String username = primaryPrincipal.getUsername();
        String avatar = primaryPrincipal.getAvatar();
        Integer[] roleIds = primaryPrincipal.getRoleIds();
        InfoData infoData = new InfoData();
        infoData.setName(username);
        infoData.setAvatar(avatar);

        MarketRoleExample example = new MarketRoleExample();
        MarketRoleExample.Criteria criteria = example.createCriteria();
        criteria.andDeletedEqualTo(false);
        criteria.andIdIn(Arrays.asList(roleIds));
        List<com.cskaoyan.bean.MarketRole> marketRoles = marketRoleMapper.selectByExample(example);

        ArrayList<String> roles = new ArrayList<>();
        for (com.cskaoyan.bean.MarketRole marketRole : marketRoles) {
            roles.add(marketRole.getName());
        }
        infoData.setRoles(roles);
        MarketPermissionExample example1 = new MarketPermissionExample();
        MarketPermissionExample.Criteria criteria1 = example1.createCriteria();
        criteria1.andDeletedEqualTo(false);
        criteria1.andRoleIdIn(Arrays.asList(roleIds));
        List<MarketPermission> marketPermissions = marketPermissionMapper.selectByExample(example1);

        List<String> collect = marketPermissions.stream()
                .map(MarketPermission::getPermission)
                .collect(Collectors.toList());

        for (String s : collect) {
            if ("*".equals(s)) {
                infoData.setPerms(Arrays.asList("*"));
                return infoData;
            }
        }

        List<String> perms = permissionDetailMapper.selectPermissionDetailsByPermissionName(collect);
        infoData.setPerms(perms);

        return infoData;
    }

    @Override
    public BaseRespVo insertpermissions(PermissionBo permissionBo) {

            //当传入的对象中Permission不为null时为对permission表的插入操作

            List<String> permissions = permissionBo.getPermissions();
        MarketPermissionExample example = new MarketPermissionExample();
        MarketPermissionExample.Criteria criteria = example.createCriteria();
        criteria.andRoleIdEqualTo(permissionBo.getRoleId());
        marketPermissionMapper.deleteByExample(example);
            for (String permission : permissions) {
                Date date = new Date();
                MarketPermission marketPermission = new MarketPermission();
                marketPermission.setAddTime(date);
                marketPermission.setUpdateTime(date);
                marketPermission.setRoleId(permissionBo.getRoleId());
                marketPermission.setPermission(permission);
                marketPermissionMapper.insertSelective(marketPermission);
            }
            return BaseRespVo.ok();



    }

    @Override
    public BaseRespVo getpermissions(Integer roleId) {
        MarketPermissionExample marketPermissionExample = new MarketPermissionExample();
        MarketPermissionExample.Criteria criteria = marketPermissionExample.createCriteria();
        criteria.andRoleIdEqualTo(roleId);
        List<MarketPermission> marketPermissions = marketPermissionMapper.selectByExample(marketPermissionExample);
        ArrayList<String> strings = new ArrayList<>();
        for (MarketPermission marketPermission : marketPermissions) {
            strings.add(marketPermission.getPermission());
        }
        //在新建的market-permission-detail表中获取返回的systemPermissions数据
        List<PermissionDetail> permissionDetailType1List = permissionDetailMapper.selectAllType1();
        for (PermissionDetail permissionDetail1 : permissionDetailType1List) {
            permissionDetail1.setChildren(permissionDetailMapper.selectAllType2(permissionDetail1.getNum()));
            for (PermissionDetail child : permissionDetail1.getChildren()) {
                child.setChildren(permissionDetailMapper.selectAllType2(child.getNum()));
            }
        }

        PermissionDetailVo permissionDetailVo = new PermissionDetailVo();
        permissionDetailVo.setAssignedPermissions(strings);
        permissionDetailVo.setSystemPermissions(permissionDetailType1List);

        return BaseRespVo.ok(permissionDetailVo);
    }

}
