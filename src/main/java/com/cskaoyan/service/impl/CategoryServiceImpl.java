package com.cskaoyan.service.impl;

import com.cskaoyan.bean.BaseData;
import com.cskaoyan.bean.MarketCategory;
import com.cskaoyan.bean.MarketCategoryExample;
import com.cskaoyan.bean.category.CategoryL1;
import com.cskaoyan.bean.category.CategoryListL1;
import com.cskaoyan.mapper.CategoryMapper;
import com.cskaoyan.mapper.MarketCategoryMapper;
import com.cskaoyan.service.CategoryService;
import com.cskaoyan.util.ObjUtils;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * @author jcp
 * @Description
 * @date 2022年06月05日 15:22
 */

@Service
@Transactional
public class CategoryServiceImpl implements CategoryService {

   @Autowired
    CategoryMapper categoryMapper;
    @Override
    public BaseData<CategoryListL1> queryAll() {
        PageHelper.startPage(1,500);

        List<CategoryListL1> categoryListL1s = categoryMapper.queryAllL1();

        for (CategoryListL1 listL1 : categoryListL1s) {
            listL1.setChildren(categoryMapper.queryAllL2(listL1.getId()));
        }

        PageInfo<CategoryListL1> pageInfo = new PageInfo<>();
        BaseData<CategoryListL1> baseData = new BaseData<>();
        baseData.setTotal((int) pageInfo.getTotal());
        baseData.setList(categoryListL1s);
        baseData.setLimit(pageInfo.getSize());
        baseData.setPage(pageInfo.getPageNum());
        baseData.setPages(pageInfo.getPages());

        return baseData;
    }

    @Override
    public BaseData<CategoryL1> queryL1() {
        PageHelper.startPage(1,500);
        List<CategoryL1> categoryL1s =categoryMapper.queryL1();
        PageInfo<CategoryL1> l1PageInfo = new PageInfo<>();
        BaseData<CategoryL1> l1BaseData = new BaseData<>();
        l1BaseData.setPages(l1PageInfo.getPages());
        l1BaseData.setPage(l1PageInfo.getPageNum());
        l1BaseData.setLimit(l1PageInfo.getSize());
        l1BaseData.setTotal((int) l1PageInfo.getTotal());
        l1BaseData.setList(categoryL1s);
        return l1BaseData;
    }

    @Override
    public void updateDeletedById(Integer id) {
        categoryMapper.updateDeletedById(id);
    }

    @Autowired
    MarketCategoryMapper marketCategoryMapper;

    @Override
    public MarketCategory addCategory(MarketCategory category) {
        category.setUpdateTime(new Date());
        category.setUpdateTime(new Date());
        marketCategoryMapper.insertSelective(category);//插入参数
        MarketCategory marketCategory = marketCategoryMapper.selectByPrimaryKey(category.getId());
        return marketCategory;
    }

    @Override
    public Integer updateCategory(MarketCategory category) {
        ObjUtils.emptyStringToNull(category);
        MarketCategoryExample example = new MarketCategoryExample();
        MarketCategoryExample.Criteria criteria = example.createCriteria();
        criteria.andIdEqualTo(category.getId());
        int i = marketCategoryMapper.updateByExampleSelective(category, example);
        return i;
    }


}
