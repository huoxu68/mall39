package com.cskaoyan.service.impl;

import com.cskaoyan.bean.BaseStatistics;
import com.cskaoyan.bean.GoodsStatistics;
import com.cskaoyan.bean.OrderStatistics;
import com.cskaoyan.bean.UserStatistics;
import com.cskaoyan.mapper.StatMapper;
import com.cskaoyan.service.StatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @ClassName: StatServiceImpl
 * @Description: 处理所有前缀为/admin/stat的请求
 * @Since: 2022/06/04 14:57
 * @Author: ZhangHuixiang
 */
@Service
@Transactional
public class StatServiceImpl implements StatService {

    @Autowired
    StatMapper statMapper;

    @Override
    public BaseStatistics<UserStatistics> queryUserStat() {
        List<UserStatistics> list = statMapper.queryUserStat();
        BaseStatistics<UserStatistics> userStatistics = new BaseStatistics<>(list);
        return userStatistics;
    }

    @Override
    public BaseStatistics<OrderStatistics> queryOrderStat() {
        List<OrderStatistics> list = statMapper.queryOrderStat();
        BaseStatistics<OrderStatistics> orderStatistics = new BaseStatistics<>(list);
        return orderStatistics;
    }

    @Override
    public BaseStatistics<GoodsStatistics> queryGoodsStat() {
        List<GoodsStatistics> list = statMapper.queryGoodsStat();
        BaseStatistics<GoodsStatistics> goodsStatistics = new BaseStatistics<>(list);
        return goodsStatistics;
    }
}
