package com.cskaoyan.service.impl;

import ch.qos.logback.core.pattern.FormatInfo;
import com.cskaoyan.bean.*;
import com.cskaoyan.bean.bo.GoodsCreateBo;
import com.cskaoyan.bean.bo.GoodsMsgBo;
import com.cskaoyan.bean.param.BaseParam;
import com.cskaoyan.bean.vo.*;
import com.cskaoyan.mapper.GoodsMapper;
import com.cskaoyan.mapper.MarketGoodsMapper;
import com.cskaoyan.mapper.*;
import com.cskaoyan.service.GoodsService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * 商品的具体实现类
 *
 * @author 李贵灏
 * @ClassName:GoodsServiceImpl
 * @since 2022/06/04 10:38
 */
@Service
@Transactional
public class GoodsServiceImpl implements GoodsService {

    @Autowired
    GoodsMapper goodsMapper;

    @Autowired
    MarketGoodsMapper marketGoodsMapper;

    @Autowired
    MarketCategoryMapper marketCategoryMapper;

    @Autowired
    MarketGoodsSpecificationMapper marketGoodsSpecificationMapper;

    @Autowired
    MarketGoodsProductMapper marketGoodsProductMapper;

    @Autowired
    MarketGoodsAttributeMapper marketGoodsAttributeMapper;

    @Autowired
    MarketGrouponRulesMapper marketGrouponRulesMapper;

    @Autowired
    MarketIssueMapper marketIssueMapper;

    @Autowired
    MarketCommentMapper marketCommentMapper;

    @Autowired
    MarketCollectMapper marketCollectMapper;

    @Autowired
    MarketBrandMapper marketBrandMapper;

    @Autowired
    MarketFootprintMapper footprintMapper;

    @Autowired
    MarketKeywordMapper keywordMapper;

    @Autowired
    MarketSearchHistoryMapper searchHistoryMapper;

    /**
     * 显示出商品页所有的信息情况
     *
     * @param baseParam
     * @return
     */
    @Override
    public BaseData<GoodsListVo> goodsList(BaseParam baseParam, GoodsMsgBo goodsMsgBo) {
        // 判断name是否为空  模糊查询
        if (!ObjectUtils.isEmpty(goodsMsgBo.getName())) {
            goodsMsgBo.setName("%" + goodsMsgBo.getName() + "%");
        }
        // 分页
        PageHelper.startPage(baseParam.getPage(), baseParam.getLimit());
        List<GoodsListVo> list = goodsMapper.selectGoddsListMsg(baseParam.getSort(), baseParam.getOrder(), goodsMsgBo);
        PageInfo<GoodsListVo> goodsListVoPageInfo = new PageInfo<>(list);
        BaseData<GoodsListVo> baseData = new BaseData<>();
        baseData.setPage(goodsListVoPageInfo.getPageNum());
        baseData.setTotal((int) goodsListVoPageInfo.getTotal());
        baseData.setPages(goodsListVoPageInfo.getPages());
        baseData.setLimit(goodsListVoPageInfo.getPageSize());
        baseData.setList(list);
        return baseData;
    }

    /**
     * @return 将所有的父子类目返回 以及将其商品的品牌商返回
     * @Date 20:34 2022/6/4
     */
    @Override
    public GoodsCategoryVo goodsCategory() {
        List<Children> father = goodsMapper.queryCategory();
        List<CategoryList> categoryLists = new LinkedList<>();
        for (Children children : father) {
            CategoryList categoryList = new CategoryList();
            categoryList.setValue(children.getValue());
            categoryList.setLabel(children.getLabel());
            categoryList.setChildren(goodsMapper.queryCateGoryOfChildren(children.getValue()));
            categoryLists.add(categoryList);
        }
        List<Children> brandList = goodsMapper.queryBrand();
        MarketKeywordExample keywordExample = new MarketKeywordExample();
        MarketKeywordExample.Criteria criteria = keywordExample.createCriteria();
        criteria.andDeletedEqualTo(false);
        List<MarketKeyword> marketKeywords = keywordMapper.selectByExample(keywordExample);

        GoodsCategoryVo goodsCategoryVo = new GoodsCategoryVo();
        goodsCategoryVo.setCategoryList(categoryLists);
        goodsCategoryVo.setBrandList(brandList);
        goodsCategoryVo.setAllKeywords(marketKeywords);
        return goodsCategoryVo;
    }

    /**
     * 商品详细信息的回显
     *
     * @param id
     * @return
     */
    @Override
    public GoodsQueryMsgVo goodsMsgById(Integer id) {

        // 通过id查询goods详细信息
        MarketGoods goodsListVo = goodsMapper.selectGoodsMsgById(id);
        // 通过子类目的的id 得到 父类目的id
        Integer ids = goodsMapper.selectCategoryIdByPid(goodsListVo.getCategoryId());
        List<Integer> integers = new LinkedList<>();
        integers.add(ids);
        integers.add(goodsListVo.getCategoryId());
        // 通过goodsId 查找products
        List<MarketGoodsProduct> productsVos = goodsMapper.selectGoodsProductsByGoodsId(id);
        // 通过goodsId去查找attributes
        List<MarketGoodsAttribute> attributesVos = goodsMapper.selectGoodsAttributesByGoodsId(id);
        // 通过goodsId 去查找specifications
        List<MarketGoodsSpecification> specificationsVos
                = goodsMapper.selectGoodsSpecificationByGoodsId(id);

        GoodsQueryMsgVo goodsQueryMsgVo = new GoodsQueryMsgVo();
        goodsQueryMsgVo.setCategoryIds(integers);
        goodsQueryMsgVo.setGoods(goodsListVo);
        goodsQueryMsgVo.setProducts(productsVos);
        goodsQueryMsgVo.setAttributes(attributesVos);
        goodsQueryMsgVo.setSpecifications(specificationsVos);
        return goodsQueryMsgVo;
    }

    /**
     * 获取商品的总数量
     *
     * @return long
     * @author ZhangHuixiang
     * @since 2022/06/05 20:05
     */
    @Override
    public long selectGoodsCount() {
        return marketGoodsMapper.countByExample(new MarketGoodsExample());
    }

    /**
     * 根据商品的id ，删除此商品的信息，
     *
     * @param id
     */
    @Override
    public void deleteGoodsMsg(Integer id) {
        // 将market_goods以及相关的 中的是否删除置为1;
        MarketGoods marketGoods = new MarketGoods();
        marketGoods.setId(id);
        marketGoods.setDeleted(true);
        marketGoodsMapper.updateByPrimaryKeySelective(marketGoods);
        // goodsMapper.deleteGoodsMsg(id);
        // MarketGoodsAttribute marketGoodsAttribute = new MarketGoodsAttribute();
        // marketGoodsAttribute.setGoodsId(id);
        // MarketGoodsAttributeExample marketGoodsAttributeExample = new MarketGoodsAttributeExample();
        // MarketGoodsAttributeExample.Criteria criteria = marketGoodsAttributeExample.createCriteria();
        // criteria.a
        // marketGoodsAttributeMapper.updateByExampleSelective()
        // goodsMapper.deleteAttribute(id);
        // goodsMapper.deleteProdut(id);
        // goodsMapper.deleteSpecification(id);
    }

    /**
     * 商品的上架，新增商品
     *
     * @param goodsCreateBo
     */
    @Override
    public void createNewGoods(GoodsCreateBo goodsCreateBo) {
        // 需要对商品设置现价
        List<MarketGoodsProduct> products1 = goodsCreateBo.getProducts();
        BigDecimal price = products1.get(0).getPrice();
        for (MarketGoodsProduct marketGoodsProduct : products1) {
            if(marketGoodsProduct.getPrice().compareTo(price)==-1){
                price = marketGoodsProduct.getPrice();
            }
        }
        // 对商品信息的添加
        Date date = new Date();
        MarketGoods goods = goodsCreateBo.getGoods();
        // 设置goods 的创建时间和修改时间
        goods.setRetailPrice(price);
        goods.setAddTime(date);
        goods.setUpdateTime(date);
        goods.setDeleted(false);
        marketGoodsMapper.insertSelective(goods);
        Integer goodsId = goods.getId();
        // 新增的规格    attributes
        List<MarketGoodsAttribute> attributes = goodsCreateBo.getAttributes();
        for (int i = 0; i < attributes.size(); i++) {
            attributes.get(i).setAddTime(date);
            attributes.get(i).setUpdateTime(date);
            attributes.get(i).setDeleted(false);
            attributes.get(i).setGoodsId(goodsId);
            marketGoodsAttributeMapper.insertSelective(attributes.get(i));
        }
        // products
        List<MarketGoodsProduct> products = goodsCreateBo.getProducts();
        for (int i = 0; i < products.size(); i++) {
            products.get(i).setId(null);
            products.get(i).setAddTime(date);
            products.get(i).setUpdateTime(date);
            products.get(i).setGoodsId(goodsId);
            products.get(i).setDeleted(false);
            marketGoodsProductMapper.insertSelective(products.get(i));
        }

        // specification
        List<MarketGoodsSpecification> specifications = goodsCreateBo.getSpecifications();
        for (int i = 0; i < specifications.size(); i++) {
            specifications.get(i).setAddTime(date);
            specifications.get(i).setUpdateTime(date);
            specifications.get(i).setGoodsId(goodsId);
            specifications.get(i).setDeleted(false);
            marketGoodsSpecificationMapper.insertSelective(specifications.get(i));
        }
    }

    /**
     * 商品的修改
     *
     * @param goodsCreateBo
     */
    @Override
    public void updateGoodsMsg(GoodsCreateBo goodsCreateBo) {
        List<MarketGoodsProduct> products1 = goodsCreateBo.getProducts();
        BigDecimal price = products1.get(0).getPrice();
        for (MarketGoodsProduct marketGoodsProduct : products1) {
            if(marketGoodsProduct.getPrice().compareTo(price)==-1){
                price = marketGoodsProduct.getPrice();
            }
        }
        MarketGoods goods = goodsCreateBo.getGoods();
        goods.setRetailPrice(price);
        // 通过id 去修改内容值
        marketGoodsMapper.updateByPrimaryKeySelective(goods);
        Date date = new Date();
        Integer goodsId = goods.getId();
        List<MarketGoodsProduct> products = goodsCreateBo.getProducts();
        // 判断修改时间是否为空
        for (int i = 0; i < products.size(); i++) {
            if (StringUtils.isEmpty(products.get(i).getId())) {
                products.get(i).setAddTime(date);
                products.get(i).setUpdateTime(date);
                products.get(i).setDeleted(false);
                products.get(i).setGoodsId(goodsId);
                marketGoodsProductMapper.insertSelective(products.get(i));
            }else if (StringUtils.isEmpty(products.get(i).getUpdateTime())) {
                products.get(i).setUpdateTime(date);
                marketGoodsProductMapper.updateByPrimaryKeySelective(products.get(i));
            }else {
                marketGoodsProductMapper.updateByPrimaryKeySelective(products.get(i));
            }

        }

        List<MarketGoodsAttribute> attributes = goodsCreateBo.getAttributes();
        // 判断修改时间是否为空
        for (int i = 0; i < attributes.size(); i++) {
            if (StringUtils.isEmpty(attributes.get(i).getId())) {
                attributes.get(i).setAddTime(date);
                attributes.get(i).setUpdateTime(date);
                attributes.get(i).setDeleted(false);
                attributes.get(i).setGoodsId(goodsId);
                marketGoodsAttributeMapper.insertSelective(attributes.get(i));
            }else if (StringUtils.isEmpty(attributes.get(i).getUpdateTime())) {
                attributes.get(i).setUpdateTime(date);
                marketGoodsAttributeMapper.updateByPrimaryKeySelective(attributes.get(i));
            }else {
                marketGoodsAttributeMapper.updateByPrimaryKeySelective(attributes.get(i));
            }
        }
        List<MarketGoodsSpecification> specifications = goodsCreateBo.getSpecifications();
        // 判断修改时间是否为空
        for (int i = 0; i < specifications.size(); i++) {
            if (StringUtils.isEmpty(specifications.get(i).getId())) {
                specifications.get(i).setAddTime(date);
                specifications.get(i).setUpdateTime(date);
                specifications.get(i).setDeleted(false);
                specifications.get(i).setGoodsId(goodsId);
                marketGoodsSpecificationMapper.insertSelective(specifications.get(i));
            }else if (StringUtils.isEmpty(specifications.get(i).getUpdateTime())) {
                specifications.get(i).setUpdateTime(date);
                marketGoodsSpecificationMapper.updateByPrimaryKeySelective(specifications.get(i));
            }else{
                marketGoodsSpecificationMapper.updateByPrimaryKeySelective(specifications.get(i));
            }
        }
    }

    @Override
    public WxGoodsCategory selectGoodsCategoryById(Integer id) {
        MarketCategory marketCategory = marketCategoryMapper.selectByPrimaryKey(id);
        WxGoodsCategory category = new WxGoodsCategory();
        if ("L1".equals(marketCategory.getLevel())) {
            MarketCategoryExample marketCategoryExample = new MarketCategoryExample();
            MarketCategoryExample.Criteria categoryExampleCriteria = marketCategoryExample.createCriteria();
            categoryExampleCriteria.andDeletedEqualTo(false);
            categoryExampleCriteria.andPidEqualTo(marketCategory.getId());
            List<MarketCategory> marketCategories = marketCategoryMapper.selectByExample(marketCategoryExample);

            category.setParentCategory(marketCategory);
            if (marketCategories.size() > 0) {
                category.setCurrentCategory(marketCategories.get(0));
                category.setBrotherCategory(marketCategories);
            }
            return category;
        }
        MarketCategoryExample marketCategoryExample = new MarketCategoryExample();
        MarketCategoryExample.Criteria categoryExampleCriteria = marketCategoryExample.createCriteria();
        categoryExampleCriteria.andDeletedEqualTo(false);
        categoryExampleCriteria.andPidEqualTo(marketCategory.getPid());
        List<MarketCategory> marketCategories = marketCategoryMapper.selectByExample(marketCategoryExample);

        category.setParentCategory(marketCategoryMapper.selectByPrimaryKey(marketCategory.getPid()));
        category.setCurrentCategory(marketCategory);
        if (marketCategories.size() > 0) {
            category.setBrotherCategory(marketCategories);
        }

        return category;
    }

    @Override
    public GoodsDetailData selectGoodsDetailByGoodsId(Integer id) {
        GoodsDetailData goodsDetailData = new GoodsDetailData();

        List<SpecificationsInGoodsDetail> specificationList = marketGoodsSpecificationMapper.selectAllSpecificationsByGoodsId(id);
        for (SpecificationsInGoodsDetail specificationsInGoodsDetail : specificationList) {
            specificationsInGoodsDetail.setValueList(marketGoodsSpecificationMapper.selectAllSpecificationsByGoodsIdAndName(specificationsInGoodsDetail.getName(), id));
        }
        goodsDetailData.setSpecificationList(specificationList);


        MarketGrouponRulesExample grouponRulesExample = new MarketGrouponRulesExample();
        MarketGrouponRulesExample.Criteria grouponRulesCriteria = grouponRulesExample.createCriteria();
        grouponRulesCriteria.andDeletedEqualTo(false);
        grouponRulesCriteria.andGoodsIdEqualTo(id);
        goodsDetailData.setGroupon(marketGrouponRulesMapper.selectByExample(grouponRulesExample));

        MarketIssueExample issueExample = new MarketIssueExample();
        MarketIssueExample.Criteria issueCriteria = issueExample.createCriteria();
        issueCriteria.andDeletedEqualTo(false);
        goodsDetailData.setIssue(marketIssueMapper.selectByExample(issueExample));

        Subject subject = SecurityUtils.getSubject();
        MarketUser marketUser = null;
        if (subject.getPrincipals() == null) {
            marketUser = new MarketUser();
            marketUser.setId(-1);
        } else {
            marketUser = (MarketUser) subject.getPrincipals().getPrimaryPrincipal();
        }
        MarketCollectExample collectExample = new MarketCollectExample();
        MarketCollectExample.Criteria collectCriteria = collectExample.createCriteria();
        collectCriteria.andDeletedEqualTo(false);
        collectCriteria.andValueIdEqualTo(id);
        collectCriteria.andUserIdEqualTo(marketUser.getId());
        goodsDetailData.setUserHasCollect(marketCollectMapper.selectByExample(collectExample).size() == 1);

        MarketCommentExample commentExample = new MarketCommentExample();
        MarketCommentExample.Criteria commentCriteria = commentExample.createCriteria();
        commentCriteria.andDeletedEqualTo(false);
        commentCriteria.andValueIdEqualTo(id);
        List<MarketComment> marketComments = marketCommentMapper.selectByExample(commentExample);
        CommentsInGoodsDetail comment = new CommentsInGoodsDetail();
        comment.setData(marketComments);
        comment.setCount(marketComments.size());
        goodsDetailData.setComment(comment);

        MarketGoods marketGoods = marketGoodsMapper.selectByPrimaryKey(id);
        goodsDetailData.setInfo(marketGoods);
        goodsDetailData.setShare(!ObjectUtils.isEmpty(marketGoods.getShareUrl()));

        MarketGoodsAttributeExample attributeExample = new MarketGoodsAttributeExample();
        MarketGoodsAttributeExample.Criteria attributeCriteria = attributeExample.createCriteria();
        attributeCriteria.andDeletedEqualTo(false);
        attributeCriteria.andGoodsIdEqualTo(id);
        goodsDetailData.setAttribute(marketGoodsAttributeMapper.selectByExample(attributeExample));

        MarketBrandExample brandExample = new MarketBrandExample();
        MarketBrandExample.Criteria brandCriteria = brandExample.createCriteria();
        brandCriteria.andDeletedEqualTo(false);
        brandCriteria.andIdEqualTo(marketGoods.getBrandId());
        List<MarketBrand> marketBrands = marketBrandMapper.selectByExample(brandExample);
        if (marketBrands != null && marketBrands.size() == 1) {
            goodsDetailData.setBrand(marketBrands.get(0));
        }

        MarketGoodsProductExample productExample = new MarketGoodsProductExample();
        MarketGoodsProductExample.Criteria productCriteria = productExample.createCriteria();
        productCriteria.andDeletedEqualTo(false);
        productCriteria.andGoodsIdEqualTo(id);
        goodsDetailData.setProductList(marketGoodsProductMapper.selectByExample(productExample));

        if (marketUser.getId() != -1) {
            MarketFootprint footprint = new MarketFootprint();
            footprint.setUserId(marketUser.getId());
            footprint.setGoodsId(id);
            MarketFootprintExample example = new MarketFootprintExample();
            MarketFootprintExample.Criteria criteria = example.createCriteria();
            criteria.andDeletedEqualTo(false);
            criteria.andUserIdEqualTo(marketUser.getId());
            criteria.andGoodsIdEqualTo(id);
            List<MarketFootprint> marketFootprints = footprintMapper.selectByExample(example);
            if (marketFootprints.size() != 1) {
                footprint.setAddTime(new Date());
                footprint.setUpdateTime(new Date());
                footprintMapper.insertSelective(footprint);
            } else {
                footprint.setUpdateTime(new Date());
                footprint.setId(marketFootprints.get(0).getId());
                footprintMapper.updateByPrimaryKeySelective(footprint);
            }
        }

        return goodsDetailData;
    }

    @Override
    public BaseData<MarketGoods> selectRelatedGoodsByGoodsId(Integer id) {
        MarketGoods marketGoods = marketGoodsMapper.selectByPrimaryKey(id);
        MarketGoodsExample goodsExample = new MarketGoodsExample();
        MarketGoodsExample.Criteria criteria = goodsExample.createCriteria();
        criteria.andDeletedEqualTo(false);
        criteria.andCategoryIdEqualTo(marketGoods.getCategoryId());
        criteria.andIdNotEqualTo(id);
        PageHelper.startPage(1, 6);
        List<MarketGoods> list = marketGoodsMapper.selectByExample(goodsExample);

        PageInfo<MarketGoods> pageInfo = new PageInfo<>(list);
        BaseData<MarketGoods> goodsData = new BaseData<>();
        goodsData.setTotal((int) pageInfo.getTotal());
        goodsData.setPages(pageInfo.getPages());
        goodsData.setPage(pageInfo.getPageNum());
        goodsData.setLimit(pageInfo.getPageSize());
        goodsData.setList(list);

        return goodsData;
    }

    @Override
    public BaseData<MarketGoods> selectGoodsByCategoryIdAndKeyword(BaseParam baseParam, String keyword, Integer categoryId) {
        if (!ObjectUtils.isEmpty(keyword)) {
            MarketSearchHistory marketSearchHistory = new MarketSearchHistory();
            Subject subject = SecurityUtils.getSubject();
            Integer userId = 0;
            if (subject.getPrincipals() != null) {
                userId = ((MarketUser) subject.getPrincipals().getPrimaryPrincipal()).getId();
            }
            MarketSearchHistoryExample marketSearchHistoryExample = new MarketSearchHistoryExample();
            MarketSearchHistoryExample.Criteria criteria = marketSearchHistoryExample.createCriteria();
            criteria.andDeletedEqualTo(false);
            criteria.andUserIdEqualTo(userId);
            criteria.andKeywordEqualTo(keyword);
            List<MarketSearchHistory> marketSearchHistories = searchHistoryMapper.selectByExample(marketSearchHistoryExample);
            if (marketSearchHistories.size() == 1) {
                MarketSearchHistory marketSearchHistory1 = marketSearchHistories.get(0);
                marketSearchHistory.setId(marketSearchHistory1.getId());
                marketSearchHistory.setUpdateTime(new Date());
                searchHistoryMapper.updateByPrimaryKeySelective(marketSearchHistory);
            } else {
                marketSearchHistory.setUserId(userId);
                marketSearchHistory.setKeyword(keyword);
                marketSearchHistory.setFrom("wx");
                marketSearchHistory.setAddTime(new Date());
                marketSearchHistory.setUpdateTime(new Date());
                marketSearchHistory.setDeleted(false);
                searchHistoryMapper.insertSelective(marketSearchHistory);
            }
            keyword = "%" + keyword + "%";
        }

        PageHelper.startPage(baseParam.getPage(), baseParam.getLimit());
        List<MarketGoods> marketGoods = marketGoodsMapper.selectByKeywordOrCategoryId(keyword, categoryId);
        PageInfo<MarketGoods> pageInfo = new PageInfo<>(marketGoods);

        BaseData<MarketGoods> goodsBaseData = new BaseData<>();
        goodsBaseData.setTotal(((int) pageInfo.getTotal()));
        goodsBaseData.setPages(pageInfo.getPages());
        goodsBaseData.setPage(pageInfo.getPageNum());
        goodsBaseData.setLimit(pageInfo.getPageSize());
        goodsBaseData.setList(marketGoods);

        return goodsBaseData;
    }
}
