package com.cskaoyan.service.impl;

import com.cskaoyan.bean.*;
import com.cskaoyan.bean.param.BaseParam;
import com.cskaoyan.bean.vo.CollectVo;
import com.cskaoyan.mapper.CollectMapper;
import com.cskaoyan.mapper.MarketCollectMapper;
import com.cskaoyan.mapper.MarketGoodsMapper;
import com.cskaoyan.service.CollectService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @ClassName: CollectServiceImpl
 * @Description: 处理所有前缀为/admin/collect, /wx/collect的请求
 * @Since: 2022/06/03 22:16
 * @Author: ZhangHuixiang
 */

@Service
@Transactional
public class CollectServiceImpl implements CollectService {

    @Autowired
    CollectMapper collectMapper;

    @Autowired
    MarketCollectMapper marketCollectMapper;

    @Autowired
    MarketGoodsMapper marketGoodsMapper;

    @Override
    public BaseData<Collect> query(BaseParam baseParam, Integer userId, Integer valueId) {

        PageHelper.startPage(baseParam.getPage(), baseParam.getLimit());

        List<Collect> list = collectMapper.query(baseParam.getSort(), baseParam.getOrder(), userId, valueId);

        PageInfo<Collect> collectPageInfo = new PageInfo<>(list);

        BaseData<Collect> collectBaseData = new BaseData<>();
        collectBaseData.setTotal(((int) collectPageInfo.getTotal()));
        collectBaseData.setPages(collectPageInfo.getPages());
        collectBaseData.setPage(collectPageInfo.getPageNum());
        collectBaseData.setLimit(collectPageInfo.getPageSize());
        collectBaseData.setList(list);

        return collectBaseData;
    }

    @Override
    public void addordelete(Byte type, Integer valueId, Integer userId) {

        MarketCollectExample marketCollectExample = new MarketCollectExample();
        MarketCollectExample.Criteria criteria = marketCollectExample.createCriteria();
        criteria.andUserIdEqualTo(userId);
        criteria.andDeletedEqualTo(false);
        criteria.andTypeEqualTo(type);
        criteria.andValueIdEqualTo(valueId);
        MarketCollect marketCollect = new MarketCollect();
        List<MarketCollect> marketCollects = marketCollectMapper.selectByExample(marketCollectExample);
        if (marketCollects.size() == 0) {
            marketCollect.setType(type);
            marketCollect.setValueId(valueId);
            marketCollect.setUserId(userId);
            marketCollect.setAddTime(new Date());
            marketCollect.setUpdateTime(new Date());
            marketCollect.setDeleted(false);
            marketCollectMapper.insertSelective(marketCollect);
        } else {
            marketCollect.setDeleted(true);
            marketCollect.setUpdateTime(new Date());
            marketCollectMapper.updateByExampleSelective(marketCollect, marketCollectExample);
        }
    }

    @Override
    public BaseData<CollectVo> list(BaseParam baseParam, Byte type, Integer id) {
        MarketCollectExample marketCollectExample = new MarketCollectExample();
        MarketCollectExample.Criteria criteria = marketCollectExample.createCriteria();
        criteria.andUserIdEqualTo(id);
        criteria.andTypeEqualTo(type);
        criteria.andDeletedEqualTo(false);
        List<MarketCollect> marketCollects = marketCollectMapper.selectByExample(marketCollectExample);


        PageHelper.startPage(baseParam.getPage(), baseParam.getLimit());
        PageInfo pageInfo = new PageInfo(marketCollects);
        Integer total = (int) pageInfo.getTotal();
        int pages = pageInfo.getPages();
        List<CollectVo> collectVos = new ArrayList<>();

        for (MarketCollect marketCollect : marketCollects) {
            MarketGoodsExample marketGoodsExample = new MarketGoodsExample();
            MarketGoodsExample.Criteria criteria1 = marketGoodsExample.createCriteria();
            criteria1.andIdEqualTo(marketCollect.getValueId());
            criteria1.andDeletedEqualTo(false);
            List<MarketGoods> marketGoods = marketGoodsMapper.selectByExample(marketGoodsExample);
            if (marketGoods.size() != 0) {
                MarketGoods marketGoods1 = marketGoods.get(0);
                CollectVo collectVo = new CollectVo();
                collectVo.setBrief(marketGoods1.getBrief());
                collectVo.setPicUrl(marketGoods1.getPicUrl());
                collectVo.setValueId(marketCollect.getValueId());
                collectVo.setName(marketGoods1.getName());
                collectVo.setId(marketCollect.getId());
                collectVo.setType(marketCollect.getType());
                collectVo.setRetailPrice(marketGoods1.getRetailPrice());
                collectVos.add(collectVo);
            }
        }
        BaseData<CollectVo> baseData = new BaseData<>(total, pages, baseParam.getLimit(), baseParam.getPage(), collectVos);
        return baseData;
    }
}
