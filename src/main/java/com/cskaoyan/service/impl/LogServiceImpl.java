package com.cskaoyan.service.impl;

import com.cskaoyan.bean.*;
import com.cskaoyan.bean.param.BaseParam;
import com.cskaoyan.mapper.MarketLogMapper;
import com.cskaoyan.service.LogService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @ClassName: LogServiceImpl
 * @Description: 操作日志的功能处理
 * @Since: 2022/06/04 23:10
 * @Author: FanLujia
 */

@Service
public class LogServiceImpl implements LogService {

    @Autowired
    MarketLogMapper marketLogMapper;

    @Override
    public BaseData<MarketLog> queryLogs(BaseParam baseParam, String name) {
        //用MarketRoleExample添加查询条件
        MarketLogExample logExample = new MarketLogExample();
        MarketLogExample.Criteria criteria = logExample.createCriteria();
        if (name != null && !"".equals(name)) {
            criteria.andAdminLike("%" + name + "%");
        }
        logExample.setOrderByClause(baseParam.getSort() + " " + baseParam.getOrder());

        //分页并获取查询结果
        PageHelper.startPage(baseParam.getPage(), baseParam.getLimit());
        List<MarketLog> logList = marketLogMapper.selectByExample(logExample);
        PageInfo<MarketLog> logPageInfo = new PageInfo<>(logList);
        //封装返回结果
        BaseData<MarketLog> logBaseData = new BaseData<>();
        logBaseData.setPage(baseParam.getPage());
        logBaseData.setLimit(baseParam.getLimit());
        logBaseData.setPages(logPageInfo.getPages());
        logBaseData.setTotal((int) logPageInfo.getTotal());
        logBaseData.setList(logList);
        return logBaseData;
    }
}