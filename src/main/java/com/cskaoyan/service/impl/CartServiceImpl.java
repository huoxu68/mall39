package com.cskaoyan.service.impl;

import com.cskaoyan.bean.*;
import com.cskaoyan.bean.wx_cart_index.*;
import com.cskaoyan.mapper.*;
import com.cskaoyan.service.CartService;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 * @author jcp
 * @Description
 * @date 2022年06月06日 22:38
 */

@Service
@Transactional
public class CartServiceImpl implements CartService {

    @Autowired
    CartMapper cartMapper;
    @Autowired
    MarketCartMapper marketCartMapper;

    /**
     * @return 购物车中的总数 count = number 的和
     * @Description: 向cart中插入用户id 商品信息
     * @Author: jcp
     * @Date: 2022/6/6 22:45
     * @Param cart中的数据有: userid goodsid productid 添加商品的数量 number
     **/
    @Override
    public Integer addGoods(MarketCart cart) {
        Subject subject = SecurityUtils.getSubject();
        MarketUser user = (MarketUser) subject.getPrincipals().getPrimaryPrincipal();
        //查询购物车中的原有信息
        Integer min = cartMapper.queryMinId();
        Integer max = cartMapper.queryMaxId();


        if (min != null && max != null) {

            MarketCartExample marketCartExample = new MarketCartExample();
            MarketCartExample.Criteria criteria = marketCartExample.createCriteria();
            criteria.andIdBetween(min, max);
            criteria.andDeletedEqualTo(false);
            List<MarketCart> marketCarts = marketCartMapper.selectByExample(marketCartExample);
            List<Integer> goodsids = new ArrayList<>();//用来存放购物车中所有商品的goodsid
            Iterator<MarketCart> iterator = marketCarts.iterator();
            while (iterator.hasNext()) {
                MarketCart cart1 = iterator.next();
                Integer goodsId = cart1.getGoodsId();
                goodsids.add(goodsId);
            }
            //传入的参数 已经存在在购物车中了 此时只需要 更新number
            if (goodsids.contains(cart.getGoodsId())) {
                MarketCart cart2 = cartMapper.queryByGoodsId(cart.getGoodsId());
                Integer number = cart2.getNumber();
                Integer updateNum = number + cart.getNumber();
                cartMapper.updateNumber(cart2.getId(), updateNum, new Date());
                Integer count = cartMapper.queryGoodCount(user.getId());
                return count;
            }
        }
        //根据 传入的 goodsId 查出相关的信息封装到marketCart
        MarketCart marketCart = cartMapper.queryInfo(cart.getGoodsId());
        //将marketCart中的数据插入到marketCart表中
        marketCart.setUserId(cart.getUserId());
        marketCart.setNumber(cart.getNumber());
        marketCart.setAddTime(new Date());
        marketCart.setUpdateTime(new Date());
        marketCartMapper.insertSelective(marketCart);
        //查询marketCart表中的所有货品的number  其中条件是 deleted = 0
        Integer count = cartMapper.queryGoodCount(user.getId());
        return count;
    }

    //获取购物车商品的总数
    @Override
    public Integer queryCounts() {
        Subject subject = SecurityUtils.getSubject();
        MarketUser user = (MarketUser) subject.getPrincipals().getPrimaryPrincipal();

        Integer count = cartMapper.queryGoodCount(user.getId());
        return count == null ? 0 : count;
    }


    @Override
    public Cart queryCart() {
        Subject subject = SecurityUtils.getSubject();
        if (subject.getPrincipals() == null) {
            return null;
        }
        MarketUser user = (MarketUser) subject.getPrincipals().getPrimaryPrincipal();
        Integer id = user.getId();
        Cart cart = new Cart();
        CartTotal cartTotal = new CartTotal();
        MarketCartExample cartExample = new MarketCartExample();
        MarketCartExample.Criteria criteria = cartExample.createCriteria();
        criteria.andUserIdEqualTo(id);
        criteria.andDeletedEqualTo(false);
        List<MarketCart> marketCarts = marketCartMapper.selectByExample(cartExample);

        Integer goodsCount = cartMapper.queryGoodCount(id);
        if (goodsCount == null) {
            goodsCount = 0;
        }
        Integer checkedCount = cartMapper.queryCheckedCount(id);
        if (checkedCount == null) {
            checkedCount = 0;
        }
        //goodsAmount  加入购入车所有商品的总价
        Double goodsAmount = cartMapper.selectAmountByUserId(id);
        if (goodsAmount == null) {
            goodsAmount = 0.0;
        }
        Double checkedGoodsAmount = cartMapper.selectCheckedAmountByUserId(id);
        if (checkedGoodsAmount == null) {
            checkedGoodsAmount = 0.0;
        }

        cartTotal.setCheckedGoodsCount(goodsCount);
        cartTotal.setCheckedGoodsCount(checkedCount);
        cartTotal.setGoodsAmount(goodsAmount);
        cartTotal.setCheckedGoodsAmount(checkedGoodsAmount);

        cart.setCartTotal(cartTotal);
        cart.setCartList(marketCarts);

        return cart;
    }

    @Override
    public Cart updateChecked(CheckedBo checkedBo) {
        List<Integer> productIds = checkedBo.getProductIds();
        Integer isChecked = checkedBo.getIsChecked();
        for (Integer id : productIds) {
            cartMapper.updateChecked(id, isChecked);
        }
        Cart cart = queryCart();
        return cart;
    }


    @Override
    public int updateGoodsNum(UpdateGoodsBo updateGoodsBo) {
        Date updateTime = new Date();
        int affectRows = cartMapper.updateNumber(updateGoodsBo.getId(), updateGoodsBo.getNumber(), updateTime);
        return affectRows;
    }

    @Autowired
    MarketAddressMapper addressMapper;
    @Autowired
    MarketCouponMapper couponMapper;
    @Autowired
    MarketCouponUserMapper couponUserMapper;
    @Autowired
    MarketGrouponRulesMapper grouponRulesMapper;

    @Override
    public CheckoutVO queryCheckout(CheckoutBO checkoutBO) {
        CheckoutVO checkoutVO = new CheckoutVO();
        Subject subject = SecurityUtils.getSubject();
        MarketUser user = (MarketUser) subject.getPrincipals().getPrimaryPrincipal();

        //获取地址信息
        Integer addressId = Integer.parseInt(checkoutBO.getAddressId());
        MarketAddress marketAddress = addressMapper.selectByPrimaryKey(addressId);

        Integer cartId = Integer.parseInt(checkoutBO.getCartId());
        List<MarketCart> marketCarts = null;
        if (cartId == 0) {
            //获取购物车信息
            // 选中的购物车信息都要查
            MarketCartExample cartExample = new MarketCartExample();
            MarketCartExample.Criteria criteria = cartExample.createCriteria();
            criteria.andUserIdEqualTo(user.getId());
            criteria.andDeletedEqualTo(false);
            criteria.andCheckedEqualTo(true);
            marketCarts = marketCartMapper.selectByExample(cartExample);
        }else{
            //获取购物车信息
            MarketCartExample cartExample = new MarketCartExample();
            MarketCartExample.Criteria criteria = cartExample.createCriteria();
            // criteria.andUserIdEqualTo(user.getId());
            // criteria.andDeletedEqualTo(false);
            // criteria.andCheckedEqualTo(true);
            criteria.andIdEqualTo(cartId);
            marketCarts = marketCartMapper.selectByExample(cartExample);
            // cartMapper.updateDeletedByCartID(cartId);
        }


        Integer couponId = Integer.parseInt(checkoutBO.getCouponId()); //优惠券信息及规则表
        Integer userCouponId = Integer.parseInt(checkoutBO.getUserCouponId());//优惠券用户使用表id
        Integer grouponRulesId = Integer.parseInt(checkoutBO.getGrouponRulesId()); //团购规则表

        Integer coupon = 0;//优惠券折扣
        Integer groupCoupon = 0;//团购优惠
        Integer limit = 0; //优惠券允许使用的张数
        //取出三个表信息
        if (couponId > 0) {
            MarketCoupon marketCoupon = couponMapper.selectByPrimaryKey(couponId);
            BigDecimal discount = marketCoupon.getDiscount();
            coupon = discount.intValue();//更新优惠券折扣
            limit = Integer.parseInt(marketCoupon.getLimit().toString());
        }
//        MarketCouponUser marketCouponUser = couponUserMapper.selectByPrimaryKey(userCouponId);
        if (grouponRulesId > 0) {
            MarketGrouponRules marketGrouponRules = grouponRulesMapper.selectByPrimaryKey(grouponRulesId);
            BigDecimal rulesDiscount = marketGrouponRules.getDiscount();
            groupCoupon = rulesDiscount.intValue();//更新团购优惠
        }

        //取出运费
        String freightPrice = cartMapper.queryFreightPrice();
        Integer freight = Integer.parseInt(freightPrice);//运费
        //取出包邮范围
        String minFreightPrice = cartMapper.queryMinFreightPrice();
        Integer minFreight = Integer.parseInt(minFreightPrice); //超过此值 需要加运费

        Double checkedGoodsAmount = 0.0;  //总共的订单费用
        for (MarketCart marketCart : marketCarts) {
            checkedGoodsAmount += marketCart.getPrice() * marketCart.getNumber();
        }

        if (checkedGoodsAmount > minFreight) {
            freight = 0;
        }
        Double actualPrice = checkedGoodsAmount - coupon - groupCoupon + freight;
        Double orderTotalPrice = actualPrice;

        checkoutVO.setGrouponPrice(groupCoupon);
        checkoutVO.setGrouponRulesId(grouponRulesId);
        checkoutVO.setActualPrice(actualPrice);
        checkoutVO.setOrderTotalPrice(orderTotalPrice);
        checkoutVO.setCartId(cartId);
        checkoutVO.setCouponPrice(coupon);
        checkoutVO.setUserCouponId(userCouponId);
        checkoutVO.setAvailableCouponLength(limit);
        checkoutVO.setCouponId(couponId);
        checkoutVO.setFreightPrice(freight);
        checkoutVO.setGoodsTotalPrice(checkedGoodsAmount);
        checkoutVO.setAddressId(addressId);
        checkoutVO.setCheckedAddress(marketAddress);
        checkoutVO.setCheckedGoodsList(marketCarts);

        return checkoutVO;
    }

    @Override
    public Integer fastAdd(MarketCart cart) {
        Subject subject = SecurityUtils.getSubject();
        MarketUser user = (MarketUser) subject.getPrincipals().getPrimaryPrincipal();
        Integer id = cart.getGoodsId();
        //根据 传入的 goodsId 查出相关的信息封装到marketCart
        MarketCart marketCart = cartMapper.queryInfo(cart.getGoodsId());
        //将marketCart中的数据插入到marketCart表中
        marketCart.setUserId(user.getId());
        marketCart.setNumber(cart.getNumber());
        marketCartMapper.insertSelective(marketCart);
        return marketCart.getId();
    }

    @Override
    public Cart dalete(Integer[] pIds) {
        for (Integer id : pIds) {
            cartMapper.updateDeleted(id);
        }
        Cart cart = queryCart();
        return cart;
    }


}
