package com.cskaoyan.service.impl;

import com.cskaoyan.bean.*;
import com.cskaoyan.bean.param.BaseParam;
import com.cskaoyan.bean.vo.CommonCoupon;
import com.cskaoyan.bean.vo.MyCoupon;
import com.cskaoyan.mapper.*;
import com.cskaoyan.service.CouponService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;


import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.Date;
import java.util.List;

/**
 * @ClassName: CouponServiceImpl
 * @Description:
 * @Since: 2022/06/04 22:31
 * @Author: changxing
 */


@Service
@Transactional
public class CouponServiceImpl implements CouponService {

    @Autowired
    MarketCouponMapper couponMapper;
    @Autowired
    MarketCouponUserMapper marketCouponUserMapper;

    @Autowired
    CouPonMapper mapper;
    @Autowired
    MarketCartMapper marketCartMapper;

    @Autowired
    MarketGoodsMapper marketGoodsMapper;

    @Override
    public BaseData<MarketCoupon> list(BaseParam baseParam, String name, Short type, Short status) {
        PageHelper.startPage(baseParam.getPage(), baseParam.getLimit());
        MarketCouponExample marketCouponExample = new MarketCouponExample();
        MarketCouponExample.Criteria criteria = marketCouponExample.createCriteria();
        // where name like ? and status = ? and deleted = ? and type = ?order by ?

        if (name != null && name != "") {
            criteria.andNameLike("%" + name + "%");
        }
        if (status != null) {
            criteria.andStatusEqualTo(status);
        }

        if (type != null) {
            criteria.andTypeEqualTo(type);
        }
        criteria.andDeletedEqualTo(false);
        marketCouponExample.setOrderByClause(baseParam.getSort() + " " + baseParam.getOrder());

        List<MarketCoupon> couponList = couponMapper.selectByExample(marketCouponExample);

        PageInfo pageInfo = new PageInfo(couponList);
        Integer total = (int) pageInfo.getTotal();
        int pages = pageInfo.getPages();
        BaseData<MarketCoupon> baseData = new BaseData<>(total, pages, baseParam.getLimit(), baseParam.getPage(), couponList);
        return baseData;
    }

    @Override
    public MarketCoupon update(MarketCoupon coupon) {
        if (coupon.getEndTime() != null) {
            if (coupon.getEndTime().getTime() < new Date().getTime()) {
                coupon.setStatus((short) 1);
            }
        }

        if (coupon.getTotal() < 0) {
            coupon.setStatus((short) 2);
        }

        if (coupon.getEndTime() != null) {
            if (coupon.getEndTime().getTime() >= new Date().getTime() && coupon.getTotal() >= 0) {
                coupon.setStatus((short) 0);
            }
        }

        if (coupon.getDays() > 0) {
            if (coupon.getTotal() >= 0) {
                coupon.setStatus((short) 0);
            }
        }
        coupon.setUpdateTime(new Date());
        couponMapper.updateByPrimaryKeySelective(coupon);
        return coupon;
    }

    @Override
    public MarketCoupon create(MarketCoupon coupon) {
        if (coupon.getType() == 2) {
            coupon.setCode(getRandomCode());
        }

        coupon.setDeleted(false);
        coupon.setAddTime(new Date());
        coupon.setUpdateTime(new Date());
        couponMapper.insertSelective(coupon);
        return coupon;
    }

    private String getRandomCode() {
        String pool = "1230456789ABCDEFGHIJKMLNOPQRSTUVWXYZ";
        StringBuffer stringBuffer = new StringBuffer();
        for (int i = 0; i < 8; i++) {
            stringBuffer.append(pool.charAt((int) (Math.random() * pool.length())));
        }
        return stringBuffer.toString();
    }

    @Override
    public void delete(MarketCoupon coupon) {
        coupon.setDeleted(true);
        coupon.setUpdateTime(new Date());
        couponMapper.updateByPrimaryKeySelective(coupon);
    }

    @Override
    public BaseData<MarketCouponUser> listuser(BaseParam baseParam, Integer couponId, Integer userId, Short status) {
        PageHelper.startPage(baseParam.getPage(), baseParam.getLimit());
        MarketCouponUserExample marketCouponUserExample = new MarketCouponUserExample();
        MarketCouponUserExample.Criteria criteria = marketCouponUserExample.createCriteria();
        // where couponId = ? and userId = ? and status = ?order by ?
        if (couponId != null) {

            criteria.andCouponIdEqualTo(couponId);
        }
        if (userId != null) {
            criteria.andUserIdEqualTo(userId);
        }
        if (status != null) {
            criteria.andStatusEqualTo(status);
        }
        criteria.andDeletedEqualTo(false);
        marketCouponUserExample.setOrderByClause(baseParam.getSort() + " " + baseParam.getOrder());

        List<MarketCouponUser> marketCouponUsers = marketCouponUserMapper.selectByExample(marketCouponUserExample);

        PageInfo pageInfo = new PageInfo(marketCouponUsers);
        Integer total = (int) pageInfo.getTotal();
        int pages = pageInfo.getPages();
        BaseData<MarketCouponUser> baseData = new BaseData<>(total, pages, baseParam.getLimit(), baseParam.getPage(), marketCouponUsers);
        return baseData;

    }

    @Override
    public MarketCoupon read(Integer id) {
        MarketCouponExample marketCouponExample = new MarketCouponExample();
        MarketCouponExample.Criteria criteria = marketCouponExample.createCriteria();
        if (!ObjectUtils.isEmpty(id)) {
            criteria.andIdEqualTo(id);
        }
        criteria.andDeletedEqualTo(false);
        List<MarketCoupon> marketCoupons = couponMapper.selectByExample(marketCouponExample);
        return marketCoupons.get(0);
    }

    @Override
    public BaseRespVo<BaseData> couponListByUser(MarketUser previousPrincipals, Integer status, Integer page, Integer limit) {
        // 通过用户信息查询该用户所拥有的优惠劵
        PageHelper.startPage(page, limit);
        Integer id = previousPrincipals.getId();
        List<MyCoupon> myCoupons = mapper.queryCouponsByUserId(id, status);
        for (MyCoupon myCoupon : myCoupons) {
            myCoupon.setAvailable(false);
        }
        BaseData<MyCoupon> myCouponBaseData = new BaseData<>();
        myCouponBaseData.setList(myCoupons);
        PageInfo<MyCoupon> myCouponPageInfo = new PageInfo<>(myCoupons);
        myCouponBaseData.setLimit(myCouponPageInfo.getPageSize());
        myCouponBaseData.setPage(myCouponPageInfo.getPageNum());
        myCouponBaseData.setPages(myCouponPageInfo.getPages());
        myCouponBaseData.setTotal((int) myCouponPageInfo.getTotal());
        return BaseRespVo.ok(myCouponBaseData);
    }

    @Override
    public BaseRespVo<BaseData<CommonCoupon>> wxListByType(Integer limit, Integer page, MarketUser user) {
        PageHelper.startPage(page, limit);
        // 此时需要查询该用户可以领取的优惠劵，即type为0的通用券,且status需要为0
        // 通用券
        MarketCouponExample example = new MarketCouponExample();
        MarketCouponExample.Criteria criteria = example.createCriteria();
        criteria.andTypeEqualTo((short) 0);
        criteria.andStatusEqualTo((short) 0);
        List<MarketCoupon> commonCoupons = couponMapper.selectByExample(example);
        ArrayList<CommonCoupon> coupons = new ArrayList<>();
        for (MarketCoupon commonCoupon : commonCoupons) {
            CommonCoupon coupon = new CommonCoupon();
            coupon.setId(commonCoupon.getId());
            coupon.setDays(commonCoupon.getDays());
            coupon.setMin(commonCoupon.getMin());
            coupon.setDiscount(commonCoupon.getDiscount());
            coupon.setDesc(commonCoupon.getDesc());
            coupon.setName(commonCoupon.getName());
            coupon.setTag(commonCoupon.getTag());
            coupons.add(coupon);
        }
        PageInfo<MarketCoupon> marketCouponPageInfo = new PageInfo<>(commonCoupons);
        BaseData<CommonCoupon> commonCouponBaseData = new BaseData<>();
        commonCouponBaseData.setList(coupons);
        commonCouponBaseData.setTotal((int) marketCouponPageInfo.getTotal());
        commonCouponBaseData.setPages(marketCouponPageInfo.getPages());
        commonCouponBaseData.setPage(marketCouponPageInfo.getPageNum());
        commonCouponBaseData.setLimit(marketCouponPageInfo.getPageSize());

        return BaseRespVo.ok(commonCouponBaseData);

    }

    @Override
    public BaseRespVo receiveCoupon(Integer couponId, MarketUser user) {
        // 先获取该优惠劵信息
        MarketCoupon marketCoupon = couponMapper.selectByPrimaryKey(couponId);
        if (marketCoupon.getType() == 2) {
            // 仅兑换领券
            BaseRespVo<Object> respVo = new BaseRespVo<>();
            respVo.setErrno(740);
            respVo.setErrmsg("该优惠券使用兑换码领取");
            return respVo;
        }
        // 再查看该用户获取该优惠劵信息
        MarketCouponUserExample example = new MarketCouponUserExample();
        MarketCouponUserExample.Criteria criteria = example.createCriteria();
        criteria.andUserIdEqualTo(user.getId());
        criteria.andCouponIdEqualTo(couponId);
        List<MarketCouponUser> couponUserList = marketCouponUserMapper.selectByExample(example);
        if (marketCoupon.getLimit() != 0 && couponUserList.size() == marketCoupon.getLimit()) {
            BaseRespVo<Object> respVo = new BaseRespVo<>();
            respVo.setErrno(740);
            respVo.setErrmsg("优惠券已经领取过");
            return respVo;
        }
        // 优惠劵发放完了（total为1表示发放完了）时total置为负数
        if (marketCoupon.getTotal() < 0) {
            BaseRespVo<Object> respVo = new BaseRespVo<>();
            respVo.setErrno(740);
            respVo.setErrmsg("优惠券已经领取完了，下次再来吧");
            return respVo;
        }
        // 可以领取，将该优惠劵信息和用户进行绑定，然后将优惠券的total减一
        MarketCouponUser marketCouponUser = new MarketCouponUser();
        marketCouponUser.setUserId(user.getId());
        marketCouponUser.setCouponId(couponId);
        marketCouponUser.setStatus((short) 0);
        Date date = new Date();
        marketCouponUser.setStartTime(date);
        // 计算截止时间
        Long days = Long.valueOf(marketCoupon.getDays());
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String time = dateFormat.format(date.getTime() + days * 24 * 60 * 60 * 1000);
        Date endTime = null;
        try {
            endTime = dateFormat.parse(time);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        marketCouponUser.setEndTime(endTime);
        marketCouponUser.setAddTime(date);
        marketCouponUser.setUpdateTime(date);
        marketCouponUser.setDeleted(false);
        marketCouponUserMapper.insert(marketCouponUser);

        // 更新优惠劵数量
        MarketCoupon marketCoupon1 = new MarketCoupon();
        marketCoupon1.setId(couponId);
        if (marketCoupon.getTotal() == 1) {
            marketCoupon1.setTotal(-1);

        } else if (marketCoupon.getTotal() == 0) {
            marketCoupon1.setTotal(0);
        } else {
            marketCoupon1.setTotal(marketCoupon.getTotal() - 1);
        }
        couponMapper.updateByPrimaryKeySelective(marketCoupon1);


        return BaseRespVo.fine();
    }

    @Override
    public BaseRespVo selectCouponList(Integer cartId) {
        Subject subject = SecurityUtils.getSubject();
        if (subject.getPrincipals() == null) {
            return BaseRespVo.invalidData("请先登录");
        }
        MarketUser user = (MarketUser) subject.getPrincipals().getPrimaryPrincipal();
        // 先查购物车内选中的商品
        MarketCartExample example = new MarketCartExample();
        MarketCartExample.Criteria criteria = example.createCriteria();
        criteria.andCheckedEqualTo(true);
        criteria.andUserIdEqualTo(user.getId());

        List<MarketCart> marketCarts = marketCartMapper.selectByExample(example);
        // 根据carts获取goodsIds
        ArrayList<Integer> objects = new ArrayList<>();
        Double totalPrice = 0.0;
        for (MarketCart marketCart : marketCarts) {
            objects.add(marketCart.getGoodsId());
            Double price = marketCart.getPrice();
            totalPrice += price;
        }
        MarketGoodsExample goodsExample = new MarketGoodsExample();
        MarketGoodsExample.Criteria criteria1 = goodsExample.createCriteria();
        if (objects.size() != 0) {
            criteria1.andIdIn(objects);
        }

        List<MarketGoods> marketGoods = marketGoodsMapper.selectByExample(goodsExample);
        // 获取商品的所有id和类目列表
        LinkedList<Integer> goodsIdList = new LinkedList<>();
        LinkedList<Integer> goodsTypeList = new LinkedList<>();
        for (MarketGoods marketGood : marketGoods) {
            goodsIdList.add(marketGood.getId());
            goodsTypeList.add(marketGood.getCategoryId());
        }
        // 获取用户所有可用优惠劵


        List<MyCoupon> couponList = mapper.queryCouponsByUserId(user.getId(), 0);
        // 再做筛选
        ArrayList<Integer> couponIdlist = new ArrayList<>();
        for (MyCoupon myCoupon : couponList) {
            couponIdlist.add(myCoupon.getCid());
        }

        // 获取couponIdlist中优惠劵详细信息
        MarketCouponExample couponExample = new MarketCouponExample();
        MarketCouponExample.Criteria criteria2 = couponExample.createCriteria();

        if (couponIdlist.size() != 0) {

            criteria2.andIdIn(couponIdlist);
        }
        List<MarketCoupon> marketCoupons = couponMapper.selectByExample(couponExample);
        // 可用优惠劵的Id列表
        ArrayList<Integer> canUserCouponIdList = new ArrayList<>();
        for (MarketCoupon marketCoupon : marketCoupons) {
            if (marketCoupon.getGoodsType() == 0) {
                // 无限制， 只需要比对最低金额即可
                // 获取商品总价
                BigDecimal min = marketCoupon.getMin();
                if (totalPrice > min.doubleValue()) {
                    // 价格大于min
                    canUserCouponIdList.add(marketCoupon.getId());
                }
            }
            if (marketCoupon.getGoodsType() == 1) {
                // 类目限制
                // 获取该优惠券的类目使用范围
                Integer[] goodsValue = marketCoupon.getGoodsValue();
                Boolean flag = false;
                for (Integer integer : goodsValue) {
                    if (goodsTypeList.contains(integer)) {
                        flag = true;
                    }
                }
                if (flag) {
                    canUserCouponIdList.add(marketCoupon.getId());
                }

            }
            if (marketCoupon.getGoodsType() == 2) {
                // 商品限制
                Integer[] goodsValue = marketCoupon.getGoodsValue();
                Boolean flag = false;
                for (Integer integer : goodsValue) {
                    if (goodsIdList.contains(integer)) {
                        flag = true;
                    }
                }
                if (flag) {
                    canUserCouponIdList.add(marketCoupon.getId());
                }
            }
        }
        for (MyCoupon myCoupon : couponList) {
            if (canUserCouponIdList.contains(myCoupon.getCid())) {
                myCoupon.setAvailable(true);
            } else {
                myCoupon.setAvailable(false);
            }
        }
        PageInfo<MyCoupon> myCouponPageInfo = new PageInfo<>(couponList);
        BaseData<MyCoupon> myCouponBaseData = new BaseData<>();
        myCouponBaseData.setLimit(myCouponPageInfo.getPageSize());
        myCouponBaseData.setPage(myCouponPageInfo.getPageNum());
        myCouponBaseData.setTotal((int) myCouponPageInfo.getTotal());
        myCouponBaseData.setPages(myCouponPageInfo.getPages());
        myCouponBaseData.setList(couponList);


        return BaseRespVo.ok(myCouponBaseData);
    }

    @Override
    public Boolean exchange(Object code, MarketUser user) {
        MarketCouponExample example = new MarketCouponExample();
        MarketCouponExample.Criteria criteria = example.createCriteria();
        criteria.andCodeEqualTo((String) code);

        List<MarketCoupon> marketCoupons = couponMapper.selectByExample(example);
        if (marketCoupons.size() != 1) {
            return false;
        }
        // 插入到coupon_user表
        MarketCoupon marketCoupon = marketCoupons.get(0);
        Date date = new Date();
        if (marketCoupon.getTimeType() == 0) {
            // 基于领取时间算截止时间
            Short days = marketCoupon.getDays();
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String time = dateFormat.format(date.getTime() + days * 24 * 60 * 60 * 1000);
            Date endTime = null;
            try {
                endTime = dateFormat.parse(time);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            marketCoupon.setEndTime(endTime);
        }
        MarketCouponUser marketCouponUser = new MarketCouponUser();
        marketCouponUser.setUserId(user.getId());
        marketCouponUser.setCouponId(marketCoupon.getId());
        marketCouponUser.setStatus(marketCoupon.getStatus());
        marketCouponUser.setStartTime(date);
        marketCouponUser.setEndTime(marketCoupon.getEndTime());
        marketCouponUser.setAddTime(date);
        marketCouponUser.setUpdateTime(date);
        marketCouponUser.setDeleted(false);

        marketCouponUserMapper.insert(marketCouponUser);


        return true;
    }
}
