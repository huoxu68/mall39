package com.cskaoyan.service.impl;

import com.cskaoyan.bean.BaseData;
import com.cskaoyan.bean.Footprint;
import com.cskaoyan.bean.MarketFootprint;
import com.cskaoyan.bean.MarketUser;
import com.cskaoyan.bean.param.BaseParam;
import com.cskaoyan.bean.vo.WxFootprintListVo;
import com.cskaoyan.mapper.FootprintMapper;
import com.cskaoyan.mapper.MarketFootprintMapper;
import com.cskaoyan.service.FootprintService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @ClassName: FootprintServiceImpl
 * @Description: 处理所有前缀为/admin/footprint的请求
 * @Since: 2022/06/04 13:17
 * @Author: ZhangHuixiang
 */

@Service
@Transactional
public class FootprintServiceImpl implements FootprintService {

    @Autowired
    FootprintMapper footprintMapper;

    @Autowired
    MarketFootprintMapper marketFootprintMapper;

    @Override
    public BaseData<Footprint> query(BaseParam baseParam, Integer userId, Integer goodsId) {
        PageHelper.startPage(baseParam.getPage(), baseParam.getLimit());

        List<Footprint> list = footprintMapper.query(baseParam.getSort(), baseParam.getOrder(), userId, goodsId);

        PageInfo<Footprint> footprintPageInfo = new PageInfo<>(list);
        BaseData<Footprint> footprintData = new BaseData<>();
        footprintData.setTotal((int) footprintPageInfo.getTotal());
        footprintData.setPages(footprintPageInfo.getPages());
        footprintData.setPage(footprintPageInfo.getPageNum());
        footprintData.setLimit(footprintPageInfo.getPageSize());
        footprintData.setList(list);

        return footprintData;
    }

    /**
     * wx 小程序端  打印足迹信息
     * @autor:liguihao
     * @param page
     * @param limit
     * @return
     */
    @Override
    public BaseData<WxFootprintListVo> footprintList(Integer page, Integer limit) {
        PageHelper.startPage(page,limit);
        Subject subject = SecurityUtils.getSubject();
        MarketUser primaryPrincipal = (MarketUser) subject.getPrincipals().getPrimaryPrincipal();
        Integer userId = primaryPrincipal.getId();
        List<WxFootprintListVo> wxFootprintListVoList = footprintMapper.selectWxQuery(userId);
        PageInfo pageInfo = new PageInfo(wxFootprintListVoList);
        long total = pageInfo.getTotal();
        int pages = pageInfo.getPages();
        BaseData<WxFootprintListVo> baseData = new BaseData<>();
        baseData.setList(wxFootprintListVoList);
        baseData.setTotal((int) total);
        baseData.setPage(page);
        baseData.setPages(pages);
        baseData.setLimit(limit);
        return baseData;
    }

    /**
     * 足迹的删除
     * @param id
     */
    @Override
    public void updateFootprint(Integer id) {
        MarketFootprint marketFootprint = new MarketFootprint();
        marketFootprint.setId(id);
        marketFootprint.setDeleted(true);
        marketFootprintMapper.updateByPrimaryKeySelective(marketFootprint);
    }
}
