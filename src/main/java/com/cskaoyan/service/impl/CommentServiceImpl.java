package com.cskaoyan.service.impl;

import com.cskaoyan.bean.BaseData;
import com.cskaoyan.bean.MarketComment;
import com.cskaoyan.bean.MarketCommentExample;
import com.cskaoyan.bean.MarketUser;
import com.cskaoyan.bean.bo.CommentIdBo;
import com.cskaoyan.bean.bo.WxCommentBo;
import com.cskaoyan.bean.param.BaseParam;
import com.cskaoyan.bean.vo.CommentCountVo;
import com.cskaoyan.bean.vo.WxCommentListVo;
import com.cskaoyan.mapper.MarketCommentMapper;
import com.cskaoyan.service.CommentService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.Date;
import java.util.List;

/**
 * comment
 *
 * @author 李贵灏
 * @since 2022/06/05 22:31
 */
@Service
@Transactional
public class CommentServiceImpl implements CommentService {

    @Autowired
    MarketCommentMapper marketCommentMapper;

    /**
     * 评论的删除
     *
     * @param marketComment
     */
    @Override
    public void deleteComment(MarketComment marketComment) {
        marketComment.setDeleted(true);
        marketCommentMapper.updateByPrimaryKey(marketComment);
    }

    /**
     * 评论的查询
     *
     * @param baseParam
     * @return
     */
    @Override
    public BaseData<MarketComment> commentList(BaseParam baseParam, CommentIdBo commentIdBo) {
        PageHelper.startPage(baseParam.getPage(), baseParam.getLimit());
        MarketCommentExample example = new MarketCommentExample();
        MarketCommentExample.Criteria criteria = example.createCriteria();
        if (!StringUtils.isEmpty(commentIdBo.getUserId())) {
            criteria.andUserIdEqualTo(commentIdBo.getUserId());
        }
        if (!StringUtils.isEmpty(commentIdBo.getValueId())) {
            criteria.andValueIdEqualTo(commentIdBo.getValueId());
        }
        criteria.andDeletedEqualTo(false);

        example.setOrderByClause(baseParam.getSort() + " " + baseParam.getOrder());

        List<MarketComment> users = marketCommentMapper.selectByExample(example);

        // 会去获得一些分页信息
        // 上面的查询对应的PageInfo → 把原始查询结果放入到构造方法里
        PageInfo pageInfo = new PageInfo(users);

        // total是总的数据量 → 是否等于users.length?不是 → 指的是如果不分页的情况下最多会查询出来多少条记录
        // 按照上面的查询的查询条件执行的select count(*)
        long total = pageInfo.getTotal();
        int pages = pageInfo.getPages();
        BaseData<MarketComment> baseData = new BaseData<>();
        baseData.setList(users);
        baseData.setPages(pages);
        baseData.setTotal((int) total);
        baseData.setPage(baseParam.getPage());

        return baseData;
    }

    /**
     * 微信小程序端的商品的评论信息的打印
     * @param wxCommentBo
     * @return
     */
    @Override
    public BaseData<WxCommentListVo> wxCommentList(WxCommentBo wxCommentBo) {
        PageHelper.startPage(wxCommentBo.getPage(),wxCommentBo.getLimit());
        Integer type = wxCommentBo.getType();
        Integer valueId = wxCommentBo.getValueId();
        Integer showType = wxCommentBo.getShowType();
        List<WxCommentListVo> commentList = marketCommentMapper.selectWxComment(type,valueId,showType);
        PageInfo pageInfo = new PageInfo(commentList);
        long total = pageInfo.getTotal();
        int pages = pageInfo.getPages();
        BaseData<WxCommentListVo> baseData = new BaseData<>();
        baseData.setList(commentList);
        baseData.setLimit(wxCommentBo.getLimit());
        baseData.setPage(wxCommentBo.getPage());
        baseData.setTotal((int) total);
        baseData.setPages(pages);
        return baseData;
    }

    /**
     * wx 程序端  评论中晒图评论 以及 全部的评论的个数为
     * @param wxCommentBo
     * @return
     */
    @Override
    public CommentCountVo CommentCount(WxCommentBo wxCommentBo) {
        Integer type = wxCommentBo.getType();
        Integer valueId = wxCommentBo.getValueId();
        Integer hasPicture1 = 0;
        Integer hasPicture2 = 1;
        // 查询评论此商品的评论总条数
        Integer nums1= marketCommentMapper.selectCommentCountByHasPicture(type,valueId,hasPicture1);
        // 查询评论此商品的带图片的数量
        Integer nums2= marketCommentMapper.selectCommentCountByHasPicture(type,valueId,hasPicture2);
        CommentCountVo commentCountVo = new CommentCountVo();
        commentCountVo.setHasPicCount(nums2);
        commentCountVo.setAllCount(nums1);
        return commentCountVo;
    }

    /**
     * wx 专题的发表评论
     * @param marketComment
     * @return
     */
    @Override
    public MarketComment commentPost(MarketComment marketComment) {
        Subject subject = SecurityUtils.getSubject();
        MarketUser primaryPrincipal = (MarketUser) subject.getPrincipals().getPrimaryPrincipal();
        Integer userId = primaryPrincipal.getId();
        marketComment.setUserId(userId);
        Date date = new Date();
        marketComment.setAddTime(date);
        marketComment.setUpdateTime(date);
        marketComment.setDeleted(false);
        marketCommentMapper.insertSelective(marketComment);
        return marketComment;
    }

    // /**
    //  * 订单中的评论信息情况
    //  * 订单收货后，如果时间超过7天未评价，则评价逾期
    //  */
    // @Override
    // public void orderComment() {
    //     Date date = new Date();
    //
    // }
}
