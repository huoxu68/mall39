package com.cskaoyan.service.impl;

import com.cskaoyan.bean.TotalCount;
import com.cskaoyan.mapper.TotalCountMapper;
import com.cskaoyan.service.TotalCountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @ClassName: TotalCountServiceImpl
 * @Description: 处理查询总数的请求
 * @Since: 2022/06/03 14:57
 * @Author: ZhangHuixiang
 */

@Service
public class TotalCountServiceImpl implements TotalCountService {

    @Autowired
    TotalCountMapper totalCountMapper;

    @Override
    public TotalCount getTotalCount() {
        TotalCount totalCount = new TotalCount();
        totalCount.setGoodsTotal(totalCountMapper.selectGoodsCount());
        totalCount.setUserTotal(totalCountMapper.selectUserCount());
        totalCount.setProductTotal(totalCountMapper.selectProductCount());
        totalCount.setOrderTotal(totalCountMapper.selectOrderCount());
        return totalCount;
    }
}
