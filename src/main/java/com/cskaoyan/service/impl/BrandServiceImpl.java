package com.cskaoyan.service.impl;

import com.cskaoyan.bean.*;
import com.cskaoyan.bean.brand.BrandList;
import com.cskaoyan.bean.param.BaseParam;
import com.cskaoyan.mapper.BrandMapper;
import com.cskaoyan.mapper.MarketBrandMapper;
import com.cskaoyan.service.BrandService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * @author jcp
 * @Description
 * @date 2022年06月04日 19:56
 */
@Service
@Transactional
public class BrandServiceImpl implements BrandService {
    @Autowired
    BrandMapper brandMapper;

    @Autowired
    MarketBrandMapper marketBrandMapper;

    @Override
    public BaseData<BrandList> queryAll(Integer id, String name) {
        String goodsname = null;
        PageHelper.startPage(1, 1000);
        if (name == null || "".equals(name)) {
            goodsname = null;
        } else {
            goodsname = "%" + name + "%";
        }
        List<BrandList> brandLists = brandMapper.queryAll(id, goodsname);

        PageInfo<BrandList> brandListPageInfo = new PageInfo<>();
        BaseData<BrandList> listBaseData = new BaseData<>();

        listBaseData.setTotal((int) brandListPageInfo.getTotal());
        listBaseData.setPages(brandListPageInfo.getPages());
        listBaseData.setPage(brandListPageInfo.getPageNum());
        listBaseData.setLimit(brandListPageInfo.getSize());
        listBaseData.setList(brandLists);
        return listBaseData;
    }


    @Override
    public void deleteById(Integer id) {
        brandMapper.updateDeleteById(id);
    }

    @Override
    public BrandList updateBrand(BrandList brandList) {
        brandMapper.updateAllBrand(brandList); //更新
        BrandList brandList1 = brandMapper.queryBrandById(brandList.getId()); //查询返回
        return brandList1;
    }


    @Override
    public MarketBrand insertNewBrand(MarketBrand marketBrand) {
        marketBrand.setAddTime(new Date());
        marketBrand.setUpdateTime(new Date());
        marketBrandMapper.insertSelective(marketBrand);
        MarketBrand marketBrand1 = marketBrandMapper.selectByPrimaryKey(marketBrand.getId());
        return marketBrand1;
    }

    @Override
    public BaseData<MarketBrand> list(BaseParam baseParam) {
        PageHelper.startPage(baseParam.getPage(), baseParam.getLimit());
        MarketBrandExample marketBrandExample = new MarketBrandExample();
        MarketBrandExample.Criteria criteria = marketBrandExample.createCriteria();

        criteria.andDeletedEqualTo(false);

        List<MarketBrand> marketBrands = marketBrandMapper.selectByExample(marketBrandExample);

        PageInfo pageInfo = new PageInfo(marketBrands);
        Integer total = (int) pageInfo.getTotal();
        int pages = pageInfo.getPages();
        BaseData<MarketBrand> baseData = new BaseData<>(total, pages, baseParam.getLimit(), baseParam.getPage(), marketBrands);
        return baseData;

    }

    @Override
    public MarketBrand detail(Integer id) {

        MarketBrand marketBrand = marketBrandMapper.selectByPrimaryKey(id);
        return marketBrand;
    }


}
