package com.cskaoyan.service.impl;

import com.cskaoyan.bean.BaseData;
import com.cskaoyan.bean.BaseRespVo;
import com.cskaoyan.bean.MarketIssue;
import com.cskaoyan.bean.MarketIssueExample;
import com.cskaoyan.bean.bo.IssueBO;
import com.cskaoyan.bean.param.BaseParam;
import com.cskaoyan.bean.po.Issue;
import com.cskaoyan.mapper.IssueMapper;
import com.cskaoyan.mapper.MarketIssueMapper;
import com.cskaoyan.service.IssueService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Date;
import java.util.List;

/**
 * @author zhongyimin
 * @since 2022/06/04 16:08
 */
@Service
public class IssueServiceImpl implements IssueService {

    @Autowired
    IssueMapper issueMapper;

    @Autowired
    MarketIssueMapper marketIssueMapper;

    @Override
    public BaseRespVo list(BaseParam param, String question) {
        List<Issue> issues = null;
        if (question == null || "".equals(question)) {
            //question为空或者“”，查询全部
            issues = issueMapper.queryAll(param);
        } else {
            String q = "%" + question + "%";
            issues = issueMapper.queryByQuestion(param, q);
        }
        PageHelper.startPage(param.getPage(), param.getLimit());
        PageInfo<Issue> issuePageInfo = new PageInfo<>(issues);

        BaseData<Issue> data = new BaseData<>();
        data.setTotal((int) issuePageInfo.getTotal());
        data.setLimit(param.getLimit());
        data.setPages(issuePageInfo.getPages());
        data.setPage(issuePageInfo.getPageNum());
        data.setList(issues);

        return BaseRespVo.ok(data);
    }

    @Override
    public BaseRespVo<Issue> create(String answer, String question) {
        Issue issue = new Issue();
        issue.setAnswer(answer);
        issue.setQuestion(question);
        issue.setAddTime(new Date());
        issue.setUpdateTime(new Date());
        issue.setDeleted(false);
        issueMapper.insert(issue);
        BaseRespVo ok = BaseRespVo.ok(issue);
        return ok;
    }

    @Override
    public BaseRespVo<Issue> update(IssueBO issue) {
        Issue newIssue = new Issue();
        System.out.println(issue);
        newIssue.setUpdateTime(new Date());
        newIssue.setAnswer(issue.getAnswer());
        newIssue.setQuestion(issue.getQuestion());
        newIssue.setId(issue.getId());
        newIssue.setAddTime(issue.getAddTime());
        newIssue.setDeleted(issue.getDeleted());

        issueMapper.update(newIssue);
        return BaseRespVo.ok(newIssue);
    }

    @Override
    public void delete(IssueBO issue) {
        issueMapper.delete(issue);
    }

    /**
     * wx 端的 帮助中心的详细信息
     * @param page
     * @param limit
     * @return
     */
    @Override
    public BaseData<MarketIssue> issueList(Integer page, Integer limit) {
        PageHelper.startPage(page,limit);
        MarketIssueExample example = new MarketIssueExample();
        MarketIssueExample.Criteria criteria = example.createCriteria();
        criteria.andDeletedEqualTo(false);
        List<MarketIssue> issueList = marketIssueMapper.selectByExample(example);
        PageInfo pageInfo = new PageInfo(issueList);
        long total = pageInfo.getTotal();
        int pages = pageInfo.getPages();
        BaseData<MarketIssue> marketIssueBaseData = new BaseData<>();
        marketIssueBaseData.setLimit(limit);
        marketIssueBaseData.setPages(pages);
        marketIssueBaseData.setTotal((int) total);
        marketIssueBaseData.setList(issueList);
        marketIssueBaseData.setPage(page);
        return marketIssueBaseData;
    }
}
