package com.cskaoyan.service.impl;

import com.cskaoyan.bean.*;
import com.cskaoyan.bean.Address;
import com.cskaoyan.bean.BaseData;
import com.cskaoyan.bean.MarketAddress;
import com.cskaoyan.bean.MarketAddressExample;
import com.cskaoyan.bean.param.BaseParam;
import com.cskaoyan.mapper.AddressMapper;
import com.cskaoyan.mapper.MarketAddressMapper;
import com.cskaoyan.service.AddressService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.dao.DataAccessException;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

import java.util.Date;
import java.util.List;

/**
 * @ClassName: AddressServiceImpl
 * @Description: 处理所有请求URI前缀为/admin/address，/wx/address的请求
 * @Since: 2022/06/03 21:28
 * @Author: ZhangHuixiang
 */

@Service
@Transactional
public class AddressServiceImpl implements AddressService {

    @Autowired
    AddressMapper addressMapper;

    @Autowired
    MarketAddressMapper marketAddressMapper;

    @Override
    public BaseData<Address> query(BaseParam baseParam, Integer userId, String name) {

        if (!ObjectUtils.isEmpty(name)) {
            name = "%" + name + "%";
        }

        PageHelper.startPage(baseParam.getPage(), baseParam.getLimit());

        List<Address> list = addressMapper.query(baseParam.getSort(), baseParam.getOrder(), userId, name);

        PageInfo<Address> addressPageInfo = new PageInfo<>(list);

        BaseData<Address> addressData = new BaseData<>();
        addressData.setTotal((int) addressPageInfo.getTotal());
        addressData.setPages(addressPageInfo.getPages());
        addressData.setPage(addressPageInfo.getPageNum());
        addressData.setLimit(addressPageInfo.getPageSize());
        addressData.setList(list);

        return addressData;
    }

    // /**
    //  * @autor:liguihao
    //  *  打印地址栏
    //  * @return
    //  */
    // @Override
    // public BaseData<MarketAddress> addressList() {
    //     MarketAddressExample marketAddressExample = new MarketAddressExample();
    //     MarketAddressExample.Criteria criteria = marketAddressExample.createCriteria();
    //     criteria.andDeletedEqualTo(false);
    //     List<MarketAddress> marketAddresses = marketAddressMapper.selectByExample(marketAddressExample);
    //     PageInfo pageInfo = new PageInfo(marketAddresses);
    //     int pages = pageInfo.getPages();
    //     long total = pageInfo.getTotal();
    //     BaseData<MarketAddress> baseData = new BaseData<>();
    //     baseData.setPages(pages);
    //     baseData.setPage(pages);
    //     baseData.setTotal((int) total);
    //     baseData.setLimit((int) total);
    //     baseData.setList(marketAddresses);
    //     return baseData;
    // }

    @Override
    public BaseData<MarketAddress> list(Integer id) {
        MarketAddressExample marketAddressExample = new MarketAddressExample();
        MarketAddressExample.Criteria criteria = marketAddressExample.createCriteria();
        criteria.andUserIdEqualTo(id);
        criteria.andDeletedEqualTo(false);
        List<MarketAddress> marketAddresses = marketAddressMapper.selectByExample(marketAddressExample);

        PageInfo pageInfo = new PageInfo(marketAddresses);
        Integer total = (int) pageInfo.getTotal();
        int pages = pageInfo.getPages();
        BaseData<MarketAddress> baseData = new BaseData<>();
        baseData.setPage(pages);
        baseData.setTotal(total);
        baseData.setPages(pages);
        baseData.setLimit(total);
        baseData.setList(marketAddresses);
        return baseData;
    }

    // /**
    //  * 修改地址信息
    //  */
    // @Override
    // public Integer  updateAddressMsg(MarketAddress marketAddress) {
    //     Subject subject = SecurityUtils.getSubject();
    //     MarketUser primaryPrincipal = (MarketUser) subject.getPrincipals().getPrimaryPrincipal();
    //     Integer userId = primaryPrincipal.getId();
    //     marketAddress.setUserId(userId);
    //     if(marketAddress.getIsDefault()){
    //         // 需要将之前的默认地址的is_default 修改为false
    //         MarketAddress marketAddress1 = new MarketAddress();
    //         marketAddress1.setIsDefault(false);
    //         MarketAddressExample example = new MarketAddressExample();
    //         MarketAddressExample.Criteria criteria = example.createCriteria();
    //         criteria.andUserIdEqualTo(marketAddress.getUserId());
    //         marketAddressMapper.updateByExampleSelective(marketAddress1,example);
    //     }
    //     if(marketAddress.getId()==0){
    //         marketAddress.setId(null);
    //         marketAddressMapper.insertSelective(marketAddress);
    //     }else{
    //         marketAddressMapper.updateByPrimaryKeySelective(marketAddress);
    //     }
    //
    //     return marketAddress.getId();
    // }


    // /**
    //  * 地址的逻辑删除
    //  * @param id
    //  */
    // @Override
    // public void deleteAdress(Integer id) {
    //
    // }
    @Override
    public MarketAddress detail(Integer id) {
        MarketAddressExample marketAddressExample = new MarketAddressExample();
        MarketAddressExample.Criteria criteria = marketAddressExample.createCriteria();
        criteria.andIdEqualTo(id);
        criteria.andDeletedEqualTo(false);
        List<MarketAddress> marketAddresses = marketAddressMapper.selectByExample(marketAddressExample);
        return marketAddresses.get(0);
    }

    @Override
    public void save(MarketAddress marketAddress, Integer id) {
        MarketAddress marketAddress1 = marketAddressMapper.selectByPrimaryKey(marketAddress.getId());
        marketAddress.setUserId(id);
        marketAddress.setDeleted(false);
        marketAddress.setUpdateTime(new Date());
        if (marketAddress.getIsDefault()) {
            MarketAddress marketAddress2 = new MarketAddress();
            marketAddress2.setIsDefault(false);
            marketAddressMapper.updateByExampleSelective(marketAddress2, new MarketAddressExample());
        }
        if (marketAddress1 == null) {
            marketAddress.setAddTime(new Date());
            marketAddressMapper.insertSelective(marketAddress);
        } else {
            marketAddressMapper.updateByPrimaryKeySelective(marketAddress);
        }

    }

    @Override
    public void delete(Integer id) {
        MarketAddress marketAddress = new MarketAddress();
        marketAddress.setId(id);
        marketAddress.setDeleted(true);
        marketAddress.setUpdateTime(new Date());
        marketAddressMapper.updateByPrimaryKeySelective(marketAddress);
    }

    // /**
    //  * 修改地址时候的  地址的回显
    //  * @return
    //  */
    // @Override
    // public MarketAddress addressMsgById(Integer id) {
    //     MarketAddress marketAddress = marketAddressMapper.selectByPrimaryKey(id);
    //     return marketAddress;
    // }

}
