package com.cskaoyan.service.impl;

import com.cskaoyan.bean.MarketFeedback;
import com.cskaoyan.bean.MarketUser;
import com.cskaoyan.mapper.MarketFeedbackMapper;
import com.cskaoyan.service.WxFeedbackService;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

/**
 * @author 李贵灏
 * @since 2022/06/06 23:33
 */
@Component
@Transactional
public class WxFeedbackServiceImpl implements WxFeedbackService {


    @Autowired
    MarketFeedbackMapper marketFeedbackMapper;
    /**
     * wx 端 意见的反馈
     */
    @Override
    public void submitMsg(MarketFeedback marketFeedback) {
        Subject subject = SecurityUtils.getSubject();
        MarketUser primaryPrincipal = (MarketUser) subject.getPrincipals().getPrimaryPrincipal();
        Integer userId = primaryPrincipal.getId();
        String username = primaryPrincipal.getUsername();
        marketFeedback.setUserId(userId);
        marketFeedback.setUsername(username);
        Date date = new Date();
        marketFeedback.setAddTime(date);
        marketFeedback.setUpdateTime(date);
        marketFeedback.setStatus(0);
        marketFeedback.setDeleted(false);
        marketFeedbackMapper.insertSelective(marketFeedback);
    }
}
