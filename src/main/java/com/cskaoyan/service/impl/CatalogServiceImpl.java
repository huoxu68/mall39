package com.cskaoyan.service.impl;

import com.cskaoyan.bean.BaseRespVo;
import com.cskaoyan.bean.MarketCategory;
import com.cskaoyan.bean.MarketCategoryExample;
import com.cskaoyan.bean.category.vo.CategoryIndex;
import com.cskaoyan.bean.category.vo.CurrentVo;
import com.cskaoyan.mapper.MarketCategoryMapper;
import com.cskaoyan.service.CatalogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author zxy
 * @version 1.0.0
 * @ClassName CatalogServiceImpl.java
 * @Description TODO
 * @createTime 2022年06月07日 09:16:00
 */
@Service
public class CatalogServiceImpl implements CatalogService {
    @Autowired
    MarketCategoryMapper marketCategoryMapper;

    @Override
    public BaseRespVo getCurrent(Integer id) {



        MarketCategory marketCategory = marketCategoryMapper.selectByPrimaryKey(id);


        MarketCategoryExample marketCategoryExample1 = new MarketCategoryExample();
        MarketCategoryExample.Criteria criteria = marketCategoryExample1.createCriteria();
        marketCategoryExample1.setOrderByClause("sort_order Asc");

        criteria.andPidEqualTo(id);
        List<MarketCategory> marketCategories1 = marketCategoryMapper.selectByExample(marketCategoryExample1);
        CurrentVo currentVo = new CurrentVo();
        currentVo.setCurrentCategory(marketCategory);
        currentVo.setCurrentSubCategory(marketCategories1);


        return BaseRespVo.ok(currentVo);
    }

    @Override
    public BaseRespVo index() {
        //categoryList 的返回值
        MarketCategoryExample marketCategoryExample = new MarketCategoryExample();
        MarketCategoryExample.Criteria criteria = marketCategoryExample.createCriteria();
        marketCategoryExample.setOrderByClause("sort_order Asc");
        criteria.andLevelEqualTo("l1");

        List<MarketCategory> marketCategories = marketCategoryMapper.selectByExample(marketCategoryExample);

        //currentCategory 的返回值
        MarketCategoryExample marketCategoryExample1 = new MarketCategoryExample();
        marketCategoryExample1.setOrderByClause("sort_order Asc");
        MarketCategoryExample.Criteria criteria1 = marketCategoryExample1.createCriteria();
        criteria1.andLevelEqualTo("l1");
        List<MarketCategory> marketCategories1 = marketCategoryMapper.selectByExample(marketCategoryExample1);


        //currentSubCategory 的返回值
        MarketCategoryExample marketCategoryExample2 = new MarketCategoryExample();
        marketCategoryExample2.setOrderByClause("sort_order Asc");
        MarketCategoryExample.Criteria criteria2 = marketCategoryExample2.createCriteria();

        criteria2.andPidEqualTo(marketCategories1.get(0).getId());
        criteria2.andLevelEqualTo("l2");
        List<MarketCategory> marketCategories2 = marketCategoryMapper.selectByExample(marketCategoryExample2);


        CategoryIndex categoryIndex = new CategoryIndex();
        categoryIndex.setCategoryList(marketCategories);
        categoryIndex.setCurrentCategory(marketCategories1.get(0));
        categoryIndex.setCurrentSubCategory(marketCategories2);



        return BaseRespVo.ok(categoryIndex);
    }
}
