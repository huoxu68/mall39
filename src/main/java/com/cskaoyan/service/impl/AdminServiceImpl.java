package com.cskaoyan.service.impl;

import com.cskaoyan.bean.Admin;
import com.cskaoyan.bean.BaseData;
import com.cskaoyan.bean.MarketAdmin;
import com.cskaoyan.bean.MarketAdminExample;
import com.cskaoyan.bean.param.BaseParam;
import com.cskaoyan.mapper.AdminMapper;
import com.cskaoyan.mapper.MarketAdminMapper;
import com.cskaoyan.service.AdminService;
import com.cskaoyan.util.MD5Utils;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * @ClassName: AdminServiceImpl
 * @Description: 管理员相关操作
 * @Since: 2022/06/04 20:11
 * @Author: FanLujia
 */

@Service
public class AdminServiceImpl implements AdminService {

    @Autowired
    AdminMapper adminMapper;
    @Autowired
    MarketAdminMapper marketAdminMapper;

    /**
     * 查询管理员信息的请求
     *
     * @return BaseData<Admin>
     * @author FanLujia
     * @since 2022/06/04 21:33
     */
    @Override
    public BaseData<Admin> queryAdmins(BaseParam param, String username) {

        //模糊查询的拼接
        if (username != null && !"".equals(username)) {
            username = "%" + username + "%";
        }

        PageHelper.startPage(param.getPage(), param.getLimit());
        List<Admin> admins = adminMapper.queryAdmins(param.getSort(), param.getOrder(), username);
        PageInfo adminsPageInfo = new PageInfo(admins);
        long total = adminsPageInfo.getTotal();
        int pages = adminsPageInfo.getPages();

        BaseData<Admin> adminsBaseData = new BaseData<>();
        adminsBaseData.setTotal((int) total);
        adminsBaseData.setPages(pages);
        adminsBaseData.setLimit(param.getLimit());
        adminsBaseData.setPage(param.getPage());
        adminsBaseData.setList(admins);
        return adminsBaseData;
    }

    @Override
    public Integer addAdmin(Admin admin) {
        String username = admin.getUsername();
        //密码md5加密
        String password = admin.getPassword();
        try {
            admin.setPassword(MD5Utils.getMultiMd5(password, password));
        } catch (Exception e) {
            e.printStackTrace();
        }

        MarketAdminExample adminExample = new MarketAdminExample();
        MarketAdminExample.Criteria criteria = adminExample.createCriteria();
        criteria.andUsernameEqualTo(username);
        long num = marketAdminMapper.countByExample(adminExample);
        //用户名已存在，返回400
        if (num > 0) {
            return 400;
        }
        Date date = new Date();
        admin.setAddTime(date);
        admin.setUpdateTime(date);
        admin.setDeleted(0);
        try {
            adminMapper.insertAdmin(admin);
            return 200;
        } catch (Exception e) {
            return 500;
        }

    }

    @Override
    public MarketAdmin updateAdmin(MarketAdmin admin) {
        String multiMd5 = null;
        try {
            multiMd5 = MD5Utils.getMultiMd5(admin.getPassword(), admin.getPassword());
        } catch (Exception e) {
            e.printStackTrace();
        }
        admin.setUpdateTime(new Date());
        MarketAdmin marketAdmin = marketAdminMapper.selectByPrimaryKey(admin.getId());
        if (admin.getPassword().equals(marketAdmin.getPassword()) || marketAdmin.getPassword().equals(multiMd5)) {
            admin.setPassword(null);
        } else {
            admin.setPassword(multiMd5);
        }
        int affectRows = marketAdminMapper.updateByPrimaryKeySelective(admin);
        admin.setPassword(null);
        return admin;

    }

    @Override
    public Integer deleteAdmin(MarketAdmin admin) {

        admin.setUpdateTime(new Date());
        admin.setDeleted(true);
        Integer affectedRows = marketAdminMapper.updateByPrimaryKeySelective(admin);
        return affectedRows;
    }
}