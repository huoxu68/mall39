package com.cskaoyan.service.impl;

import com.cskaoyan.bean.*;
import com.cskaoyan.bean.param.BaseParam;
import com.cskaoyan.bean.topic.vo.WxTopicVo;
import com.cskaoyan.mapper.MarketGoodsMapper;
import com.cskaoyan.mapper.MarketTopicMapper;
import com.cskaoyan.service.WxTopicService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @author zxy
 * @version 1.0.0
 * @ClassName WxTopicServiceImpl.java
 * @Description TODO
 * @createTime 2022年06月07日 16:36:00
 */
@Service
public class WxTopicServiceImpl implements WxTopicService {
    @Autowired
    MarketTopicMapper marketTopicMapper;

    @Autowired
    MarketGoodsMapper marketGoodsMapper;

    @Override
    public BaseRespVo wxList(BaseParam baseParam) {
        /**
         * @title
         * @description
         * @author zxy
         * @param: baseParam
         * @updateTime 2022/6/7 16:24
         * @return: com.cskaoyan.bean.BaseRespVo
         * @throws
         */

        PageHelper.startPage(baseParam.getPage(),baseParam.getLimit());
        MarketTopicExample marketTopicExample = new MarketTopicExample();
        List<MarketTopic> marketTopics = marketTopicMapper.selectByExample(marketTopicExample);

        PageInfo pageInfo = new PageInfo(marketTopics);
        long total = pageInfo.getTotal();
        int pages = pageInfo.getPages();
        BaseData<MarketTopic> marketTopicBaseData = new BaseData<>((int) total, pages, baseParam.getLimit(), baseParam.getPage(), marketTopics);



        return BaseRespVo.ok(marketTopicBaseData);
    }

    @Override
    public BaseRespVo detail(Integer id) {
        MarketTopic marketTopic = marketTopicMapper.selectByPrimaryKey(id);

        List<MarketGoods> marketGoods = new ArrayList<>();
        Integer[] goods = marketTopic.getGoods();
        for (Integer good : goods) {




            MarketGoods marketGoods1 = marketGoodsMapper.selectByPrimaryKey(good);
            marketGoods.add(marketGoods1);


        }

        WxTopicVo wxTopicVo = new WxTopicVo();
        wxTopicVo.setGoods(marketGoods);
        wxTopicVo.setTopic(marketTopic);


        return BaseRespVo.ok(wxTopicVo);
    }

    @Override
    public BaseRespVo related(Integer id) {
        PageHelper.startPage(1,4);
        MarketTopicExample marketTopicExample = new MarketTopicExample();
        List<MarketTopic> marketTopics = marketTopicMapper.selectByExample(marketTopicExample);
        PageInfo marketTopicPageInfo = new PageInfo<>(marketTopics);
        int total = (int)marketTopicPageInfo.getTotal();
        int pages = marketTopicPageInfo.getPages();

        BaseData<MarketTopic> marketTopicBaseData = new BaseData<>(total, pages, 1, 4, marketTopics);




        return BaseRespVo.ok(marketTopicBaseData);
    }
}
