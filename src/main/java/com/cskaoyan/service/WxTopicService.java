package com.cskaoyan.service;

import com.cskaoyan.bean.BaseRespVo;
import com.cskaoyan.bean.param.BaseParam;

/**
 * @author zxy
 * @version 1.0.0
 * @ClassName WxTopicService.java
 * @Description TODO
 * @createTime 2022年06月07日 16:35:00
 */
public interface WxTopicService {
    BaseRespVo wxList(BaseParam baseParam);

    BaseRespVo detail(Integer id);

    BaseRespVo related(Integer id);
}
