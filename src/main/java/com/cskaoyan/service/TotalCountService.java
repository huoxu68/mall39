package com.cskaoyan.service;

import com.cskaoyan.bean.TotalCount;

/**
 * @ClassName: TotalCountService
 * @Description: 处理查询总数的请求
 * @Since: 2022/06/03 14:56
 * @Author: ZhangHuixiang
 */

public interface TotalCountService {

    TotalCount getTotalCount();

}
