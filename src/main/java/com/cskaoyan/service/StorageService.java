package com.cskaoyan.service;

import com.cskaoyan.bean.BaseData;
import com.cskaoyan.bean.BaseRespVo;
import com.cskaoyan.bean.MarketStorage;
import com.cskaoyan.bean.param.BaseParam;
import com.cskaoyan.bean.storage.bo.UpdateBo;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author zxy
 * @version 1.0.0
 * @ClassName StorageService.java
 * @Description TODO
 * @createTime 2022年06月05日 16:39:00
 */
public interface StorageService {
    BaseData<MarketStorage> queryByKeyAndName(BaseParam baseParam, String key, String name);

    MarketStorage insertStorage(MultipartFile file);

    MarketStorage updateName(UpdateBo updateBo);

    void delete(UpdateBo updateBo);
}
