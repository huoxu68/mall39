package com.cskaoyan.service;

import com.cskaoyan.bean.BaseRespVo;
import com.cskaoyan.bean.MarketKeyword;
import com.cskaoyan.bean.param.BaseParam;

public interface KeywordService {


    BaseRespVo queryByKeywordAndUrl(BaseParam param, String keyword, String url);

    MarketKeyword create(MarketKeyword marketKeyword);

    MarketKeyword update(MarketKeyword marketKeyword);

    void delete(MarketKeyword marketKeyword);

}
