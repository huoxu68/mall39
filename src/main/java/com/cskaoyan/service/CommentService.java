package com.cskaoyan.service;

import com.cskaoyan.bean.BaseData;
import com.cskaoyan.bean.MarketComment;
import com.cskaoyan.bean.bo.CommentIdBo;
import com.cskaoyan.bean.bo.WxCommentBo;
import com.cskaoyan.bean.param.BaseParam;
import com.cskaoyan.bean.vo.CommentCountVo;
import com.cskaoyan.bean.vo.WxCommentListVo;

public interface CommentService {

    // 评论的删除
    void deleteComment(MarketComment marketComment);

    // 评论的查询
    BaseData<MarketComment> commentList(BaseParam baseParam , CommentIdBo commentIdBo);

    // Wx 小程序端  打印商品的评论
    BaseData<WxCommentListVo> wxCommentList(WxCommentBo wxCommentBo);

    // wx 程序端  晒图评论以及 全部评论的总数
    CommentCountVo CommentCount(WxCommentBo wxCommentBo);

    // wx 专题发表评论
    MarketComment commentPost(MarketComment marketComment);

    // // 订单过七天未评论后将自动评论
    // void orderComment();

}
