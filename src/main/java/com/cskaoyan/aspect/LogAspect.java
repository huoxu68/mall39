package com.cskaoyan.aspect;

import com.cskaoyan.bean.Admin;
import com.cskaoyan.bean.BaseRespVo;
import com.cskaoyan.bean.MarketAdmin;
import com.cskaoyan.bean.MarketLog;
import com.cskaoyan.mapper.MarketLogMapper;
import com.cskaoyan.mapper.PermissionDetailMapper;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.Map;

/**
 * @ClassName: LogAspect
 * @Description:
 * @Since: 2022/06/07 14:28
 * @Author: ZhangHuixiang
 */

@Component
@Aspect
public class LogAspect {

    @Autowired
    PermissionDetailMapper permissionMapper;

    @Autowired
    MarketLogMapper logMapper;

    @Around("execution(* com.cskaoyan.controller.admin..*(..))")
    public Object aroud(ProceedingJoinPoint joinPoint) throws Throwable {

        Subject subject = SecurityUtils.getSubject();
        String admin = null;
        if (subject.getPrincipals() != null) {
            MarketAdmin primaryPrincipal = (MarketAdmin) subject.getPrincipals().getPrimaryPrincipal();
            admin = primaryPrincipal.getUsername();
        }

        Object proceed = joinPoint.proceed();

        // 接收到请求，记录请求内容
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = attributes.getRequest();

        MarketLog marketLog = new MarketLog();
        marketLog.setIp(request.getRemoteHost());
        if (joinPoint.getSignature().getName().contains("login")) {
            marketLog.setAction("登录");
        }
        if (joinPoint.getSignature().getName().contains("logout")) {
            marketLog.setAction("退出");
        }
        if (admin == null) {
            if (subject.getPrincipals() != null) {
                MarketAdmin primaryPrincipal = (MarketAdmin) subject.getPrincipals().getPrimaryPrincipal();
                marketLog.setAdmin(primaryPrincipal.getUsername());
            }
        } else {
            marketLog.setAdmin(admin);
        }

        String action = permissionMapper.selectLabelByAPI(request.getMethod() + " " + request.getRequestURI());
        if (!ObjectUtils.isEmpty(action)) {
            marketLog.setAction(action);
        }

        if (marketLog.getAction() == null) {
            return proceed;
        }

        if (ObjectUtils.isEmpty(marketLog.getAdmin())) {
            if (!marketLog.getAction().equals("登录")) {
                return proceed;
            } else {
                marketLog.setAdmin(((String) ((Map) joinPoint.getArgs()[0]).get("username")));
            }
        }

        marketLog.setAddTime(new Date());
        marketLog.setUpdateTime(new Date());
        marketLog.setResult(((BaseRespVo) proceed).getErrmsg());
        marketLog.setType(1);
        if (((BaseRespVo) proceed).getErrno() == 0) {
            marketLog.setStatus(true);
        } else {
            marketLog.setStatus(false);
        }
        logMapper.insertSelective(marketLog);

        return proceed;
    }

}

