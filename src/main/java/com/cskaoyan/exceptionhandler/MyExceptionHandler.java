package com.cskaoyan.exceptionhandler;

import com.cskaoyan.bean.BaseRespVo;
import com.cskaoyan.bean.MarketLog;
import com.cskaoyan.mapper.MarketLogMapper;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authz.AuthorizationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Date;
import java.util.Map;

/**
 * @ClassName: IncorrectCredentialsExceptionHandler
 * @Description:
 * @Since: 2022/06/06 15:20
 * @Author: ZhangHuixiang
 */

@ControllerAdvice
public class MyExceptionHandler {

    @Autowired
    MarketLogMapper logMapper;

    @ExceptionHandler(UnknownAccountException.class)
    @ResponseBody
    public BaseRespVo unknownAccountExceptionHandler(UnknownAccountException e, HttpServletRequest request) {
        insertLog(request.getRemoteHost(), "用户名错误");
        return BaseRespVo.invalidParameter("用户名错误");
    }

    @ExceptionHandler(IncorrectCredentialsException.class)
    @ResponseBody
    public BaseRespVo incorrectCredentialsExceptionHandler(IncorrectCredentialsException e, HttpServletRequest request) {
        insertLog(request.getRemoteHost(), "密码错误");
        return BaseRespVo.invalidParameter("密码错误");
    }

    @ExceptionHandler(AuthorizationException.class)
    @ResponseBody
    public BaseRespVo authorizationException(AuthorizationException e) {
        return BaseRespVo.invalidData("没有认证");
    }

    private void insertLog(String host, String comment) {
        MarketLog marketLog = new MarketLog();
        marketLog.setIp(host);
        marketLog.setAction("登录");
        marketLog.setType(0);
        marketLog.setStatus(false);
        marketLog.setResult("登录失败");
        marketLog.setDeleted(false);
        marketLog.setAddTime(new Date());
        marketLog.setUpdateTime(new Date());
        marketLog.setComment(comment);
        logMapper.insertSelective(marketLog);
    }

    @ExceptionHandler(Exception.class)
    @ResponseBody
    public BaseRespVo exception(Exception e) {
        e.printStackTrace();
        return BaseRespVo.internalException();
    }
}
