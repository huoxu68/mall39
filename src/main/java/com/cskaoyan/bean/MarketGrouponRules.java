package com.cskaoyan.bean;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
public class MarketGrouponRules {
    private Integer id;

    private Integer goodsId;

    private String goodsName;

    private String picUrl;

    private BigDecimal discount;

    private Integer discountMember;

    private Date expireTime;

    private Short status;

    private Date addTime;

    private Date updateTime;

    private Boolean deleted;
}