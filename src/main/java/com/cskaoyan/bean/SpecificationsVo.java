package com.cskaoyan.bean;

/**
 * 商品的规格参数
 *
 * @author 李贵灏
 * @since 2022/06/04 21:40
 */
public class SpecificationsVo {
    private Integer id;
    private Integer goodsId;
    private String specification;
    private String value;
    private String picUrl;
    private String addTime;
    private String updateTime;
    private boolean deleted;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Integer goodsId) {
        this.goodsId = goodsId;
    }

    public String getSpecification() {
        return specification;
    }

    public void setSpecification(String specification) {
        this.specification = specification;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getPicUrl() {
        return picUrl;
    }

    public void setPicUrl(String picUrl) {
        this.picUrl = picUrl;
    }

    public String getAddTime() {
        return addTime;
    }

    public void setAddTime(String addTime) {
        this.addTime = addTime;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Integer deleted) {
        if(deleted==1){
            this.deleted  = true;
        }else if(deleted==0){
            this.deleted  = false;
        }
    }
}
