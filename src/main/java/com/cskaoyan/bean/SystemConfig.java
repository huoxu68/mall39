package com.cskaoyan.bean;

import lombok.Data;

/**
 * @author zxy
 * @version 1.0.0
 * @ClassName SystemConfig.java
 * @Description TODO
 * @createTime 2022年06月05日 21:35:00
 */
@Data
public class SystemConfig {
    private String market_mall_longitude;
    private String market_mall_latitude;
    private String market_mall_address;
    private String market_mall_qq;
    private String market_mall_phone;
    private String market_mall_name;
    private String market_express_freight_min;
    private String market_express_freight_value;
    private String market_order_unconfirm;
    private String market_order_unpaid;
    private String market_order_comment;
    private String market_wx_index_new;
    private String market_wx_index_topic;
    private String market_wx_share;
    private String market_wx_index_brand;
    private String market_wx_catlog_goods;
    private String market_wx_catlog_list;
    private String market_wx_index_hot;
}
