package com.cskaoyan.bean;

/**
 * 规格
 *
 * @author 李贵灏
 * @since 2022/06/04 21:51
 */
public class AttributesVo {
    private Integer id;
    private Integer goodsId;
    private String attribute;
    private String value;
    private String addTime;
    private String updateTime;
    private boolean deleted;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Integer goodsId) {
        this.goodsId = goodsId;
    }

    public String getAttribute() {
        return attribute;
    }

    public void setAttribute(String attribute) {
        this.attribute = attribute;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getAddTime() {
        return addTime;
    }

    public void setAddTime(String addTime) {
        this.addTime = addTime;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(Integer deleted) {
        if(deleted==1){
            this.deleted  = true;
        }else if(deleted==0){
            this.deleted  = false;
        }
    }
}
