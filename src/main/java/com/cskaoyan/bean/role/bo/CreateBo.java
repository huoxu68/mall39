package com.cskaoyan.bean.role.bo;

import lombok.Data;

/**
 * @author zxy
 * @version 1.0.0
 * @ClassName createBo.java
 * @Description TODO
 * @createTime 2022年06月04日 16:59:00
 */
@Data
public class CreateBo {

    /**
     * name : lgh
     * desc : niubi
     */
    private String name;
    private String desc;

    public void setName(String name) {
        this.name = name;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getName() {
        return name;
    }

    public String getDesc() {
        return desc;
    }
}
