package com.cskaoyan.bean.role.bo;

import java.util.List;

/**
 * @author zxy
 * @version 1.0.0
 * @ClassName PermissionBo.java
 * @Description TODO
 * @createTime 2022年06月06日 20:29:00
 */

public class PermissionBo {

    /**
     * roleId : 110
     * permissions : ["admin:feedback:list","admin:user:list","admin:notice:list","admin:comment:list"]
     */
    private int roleId;
    private List<String> permissions;

    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }

    public void setPermissions(List<String> permissions) {
        this.permissions = permissions;
    }

    public int getRoleId() {
        return roleId;
    }

    public List<String> getPermissions() {
        return permissions;
    }
}
