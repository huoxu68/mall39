package com.cskaoyan.bean.role.bo;

import lombok.Data;

/**
 * @author zxy
 * @version 1.0.0
 * @ClassName updateBo.java
 * @Description TODO
 * @createTime 2022年06月04日 20:57:00
 */
@Data
public class UpdateBo {

    /**
     * deleted : false
     * addTime : 2022-06-04 16:49:58
     * name : lgh
     * updateTime : 2022-06-04 16:49:58
     * id : 93
     * enabled : true
     * desc : niubi123
     */
    private boolean deleted;
    private String addTime;
    private String name;
    private String updateTime;
    private int id;
    private boolean enabled;
    private String desc;
}
