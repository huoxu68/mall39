package com.cskaoyan.bean.role.vo;

import lombok.Data;

/**
 * @author zxy
 * @version 1.0.0
 * @ClassName CreateVo.java
 * @Description TODO
 * @createTime 2022年06月04日 17:00:00
 */
@Data
public class CreateVo {

    /**
     * addTime : 2022-06-04 16:49:57
     * name : lgh
     * updateTime : 2022-06-04 16:49:57
     * id : 93
     * desc : niubi
     */
    private String addTime;
    private String name;
    private String updateTime;
    private int id;
    private String desc;


}
