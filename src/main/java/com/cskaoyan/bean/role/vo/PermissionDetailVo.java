package com.cskaoyan.bean.role.vo;

import com.cskaoyan.bean.role.po.PermissionDetail;
import lombok.Data;

import java.util.List;

/**
 * @author zxy
 * @version 1.0.0
 * @ClassName PermissionDetailVo.java
 * @Description TODO
 * @createTime 2022年06月06日 21:29:00
 */
@Data
public class PermissionDetailVo {
   List<String> assignedPermissions;
   List<PermissionDetail> systemPermissions;

}
