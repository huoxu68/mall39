package com.cskaoyan.bean.role.po;

import lombok.Data;

import java.util.List;

/**
 * @author zxy
 * @version 1.0.0
 * @ClassName PermissionDetail.java
 * @Description TODO
 * @createTime 2022年06月06日 21:13:00
 */
@Data
public class PermissionDetail {
    String id;
    Integer pid;
    String label;
    String api;
    String type;
    Integer num;
    List<PermissionDetail> children;

}
