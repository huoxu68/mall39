package com.cskaoyan.bean;

import lombok.Data;

/**
 * @ClassName: OrderCount
 * @Description:
 * @Since: 2022/06/06 19:52
 * @Author: ZhangHuixiang
 */

@Data
public class OrderCount {

    private Integer unrecv;
    private Integer uncomment;
    private Integer unpaid;
    private Integer unship;

}
