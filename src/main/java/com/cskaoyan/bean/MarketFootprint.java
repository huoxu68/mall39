package com.cskaoyan.bean;

import lombok.Data;

import java.util.Date;

@Data
public class MarketFootprint {
    private Integer id;

    private Integer userId;

    private Integer goodsId;

    private Date addTime;

    private Date updateTime;

    private Boolean deleted;
}