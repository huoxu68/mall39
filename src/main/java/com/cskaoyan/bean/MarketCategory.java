package com.cskaoyan.bean;

import lombok.Data;

import java.util.Date;

@Data
public class MarketCategory {
    private Integer id;

    private String name;

    private String keywords;

    private String desc;

    private Integer pid;

    private String iconUrl;

    private String picUrl;

    private String level;

    private Byte sortOrder;

    private Date addTime;

    private Date updateTime;

    private Boolean deleted;
}