package com.cskaoyan.bean;

import lombok.Data;

/**
 * @ClassName: HandleOption
 * @Description: 小程序订单的权限
 * @Since: 2022/06/07 14:13
 * @Author: FanLujia
 */

@Data
public class HandleOption {

    private boolean cancel = false; //取消订单
    private boolean confirm = false; //确认收货
    private boolean rebuy = false; //再次购买
    private boolean pay = false; //付款
    private boolean comment = false; //评论
    private boolean delete = false; //删除
    private boolean aftersale = false; //申请售后
    private boolean refund = false; //已经退款

}