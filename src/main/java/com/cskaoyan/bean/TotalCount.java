package com.cskaoyan.bean;

import lombok.Data;

/**
 * @ClassName: TotalCount
 * @Description:
 * @Since: 2022/06/03 14:51
 * @Author: ZhangHuixiang
 */

@Data
public class TotalCount {
    /**
     * goodsTotal : 288
     * userTotal : 1
     * productTotal : 298
     * orderTotal : 262
     */
    private Integer goodsTotal;
    private Integer userTotal;
    private Integer productTotal;
    private Integer orderTotal;
}
