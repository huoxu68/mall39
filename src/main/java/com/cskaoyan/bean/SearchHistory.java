package com.cskaoyan.bean;

import lombok.Data;

import java.util.Date;

/**
 * @ClassName: SearchHistory
 * @Description:
 * @Since: 2022/06/04 13:33
 * @Author: ZhangHuixiang
 */

@Data
public class SearchHistory {
    private Integer id;
    private Integer userId;
    private String keyword;
    private String from;
    private Date addTime;
    private Date updateTime;
    private Boolean deleted;
}
