package com.cskaoyan.bean;

import lombok.Data;

import java.util.List;

/**
 * @ClassName: GoodsDetailData
 * @Description:
 * @Since: 2022/06/06 21:18
 * @Author: ZhangHuixiang
 */
@Data
public class GoodsDetailData {

    private List<SpecificationsInGoodsDetail> specificationList;
    private List<MarketGrouponRules> groupon;
    private List<MarketIssue> issue;
    private Boolean userHasCollect;
    private CommentsInGoodsDetail comment;
    private Boolean share;
    private List<MarketGoodsAttribute> attribute;
    private MarketBrand brand;
    private List<MarketGoodsProduct> productList;
    private MarketGoods info;

}
