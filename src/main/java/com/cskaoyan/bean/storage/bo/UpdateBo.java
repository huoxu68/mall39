package com.cskaoyan.bean.storage.bo;

import lombok.Data;

/**
 * @author zxy
 * @version 1.0.0
 * @ClassName updateBo.java
 * @Description TODO
 * @createTime 2022年06月05日 20:26:00
 */
@Data
public class UpdateBo {


    /**
     * deleted : null
     * size : 13355
     * addTime : 2022-06-05 19:20:03
     * name : 优盘.jpeg
     * updateTime : 2022-06-05 19:20:03
     * id : 1040
     * type : image/jpeg
     * key : 2cdb8439a72e8ac1a516e231085dd98c.jpeg
     * url : http://localhost:8083/pic/2cdb8439a72e8ac1a516e231085dd98c.jpeg
     */
    private String deleted;
    private int size;
    private String addTime;
    private String name;
    private String updateTime;
    private int id;
    private String type;
    private String key;
    private String url;

    public void setDeleted(String deleted) {
        this.deleted = deleted;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public void setAddTime(String addTime) {
        this.addTime = addTime;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDeleted() {
        return deleted;
    }

    public int getSize() {
        return size;
    }

    public String getAddTime() {
        return addTime;
    }

    public String getName() {
        return name;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public int getId() {
        return id;
    }

    public String getType() {
        return type;
    }

    public String getKey() {
        return key;
    }

    public String getUrl() {
        return url;
    }
}
