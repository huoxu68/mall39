package com.cskaoyan.bean.wx_cart_index;

import lombok.Data;

/**
 * @author jcp
 * @Description
 * @date 2022年06月08日 19:57
 */
@Data
public class DeleteBO {
    private Integer[] productIds;
}
