package com.cskaoyan.bean.wx_cart_index;

import lombok.Data;

/**
 * @author jcp
 * @Description
 * @date 2022年06月06日 21:28
 */
@Data
public class CartTotal {

    private Integer goodsCount;
    private Integer checkedGoodsCount;
    private Double goodsAmount;
    private Double checkedGoodsAmount;


}
