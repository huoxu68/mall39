package com.cskaoyan.bean.wx_cart_index;

import com.cskaoyan.bean.MarketAddress;
import com.cskaoyan.bean.MarketCart;
import lombok.Data;

import java.util.List;

/**
 * @author jcp
 * @Description 结账的返回类
 * @date 2022年06月07日 20:05
 */
@Data
public class CheckoutVO {

    private Integer grouponPrice;
    private Integer grouponRulesId;
    private Double actualPrice;
    private Double orderTotalPrice;
    private Integer cartId;
    private Integer couponPrice;
    private Integer userCouponId;
    private Integer availableCouponLength;
    private Integer couponId;
    private Integer freightPrice;
    private Double goodsTotalPrice;
    private Integer addressId;
    private MarketAddress checkedAddress;
    private List<MarketCart>  checkedGoodsList;

}
