package com.cskaoyan.bean.wx_cart_index;

import lombok.Data;

/**
 * @author jcp
 * @Description
 * @date 2022年06月07日 20:09
 */
@Data
public class CheckedAddressOfCheckoutVO {

    private String addTime;
    private String city;
    private String county;
    private String updateTime;
    private Integer userId;
    private String areaCode;
    private boolean isDefault;
    private String addressDetail;
    private boolean deleted;
    private String province;
    private String name;
    private String tel;
    private Integer id;

}
