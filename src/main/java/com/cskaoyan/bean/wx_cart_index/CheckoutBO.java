package com.cskaoyan.bean.wx_cart_index;

import lombok.Data;

/**
 * @author jcp
 * @Description
 * @date 2022年06月07日 20:14
 */
@Data
public class CheckoutBO {
    private String cartId;
    private String addressId;
    private String couponId;
    private String userCouponId;
    private String grouponRulesId;
}
