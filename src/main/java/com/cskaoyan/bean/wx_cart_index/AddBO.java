package com.cskaoyan.bean.wx_cart_index;

import lombok.Data;

/**
 * @author jcp
 * @Description
 * @date 2022年06月06日 22:04
 */

@Data
public class AddBO {
   private Integer goodsId;
   private Integer number;
   private Integer productId;
   private Integer userId;
}
