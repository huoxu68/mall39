package com.cskaoyan.bean.wx_cart_index;

import com.cskaoyan.bean.MarketCart;
import lombok.Data;

import java.util.List;

/**
 * @author jcp
 * @Description 购物车类  index
 * @date 2022年06月06日 21:26
 */

@Data
public class Cart {
    private List<MarketCart> cartList;
    private CartTotal cartTotal;
}
