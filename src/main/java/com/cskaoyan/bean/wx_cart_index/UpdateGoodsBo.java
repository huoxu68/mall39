package com.cskaoyan.bean.wx_cart_index;

import lombok.Data;

/**
 * @author jcp
 * @Description
 * @date 2022年06月07日 16:58
 */
@Data
public class UpdateGoodsBo {
    private Integer number;
    private Integer productId;
    private Integer goodsId;
    private Integer id;

}
