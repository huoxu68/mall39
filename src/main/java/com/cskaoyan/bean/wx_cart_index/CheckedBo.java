package com.cskaoyan.bean.wx_cart_index;

import lombok.Data;

import java.util.List;

/**
 * @author jcp
 * @Description
 * @date 2022年06月07日 16:26
 */
@Data
public class CheckedBo {
    private List<Integer> productIds;
    private Integer isChecked;
}
