package com.cskaoyan.bean;

import lombok.Data;

import java.util.List;

@Data
public class InfoData {
    private List<String> roles;
    private String name;
    private List<String> perms;
    private String avatar;
}
