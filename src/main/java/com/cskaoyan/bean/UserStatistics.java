package com.cskaoyan.bean;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

/**
 * @ClassName: UserStatistics
 * @Description:
 * @Since: 2022/06/04 14:53
 * @Author: ZhangHuixiang
 */
@Data
public class UserStatistics {
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date day;
    private Integer users;
}
