package com.cskaoyan.bean;

import lombok.Data;

/**
 * @ClassName: UserIndexData
 * @Description:
 * @Since: 2022/06/06 19:52
 * @Author: ZhangHuixiang
 */

@Data
public class UserIndexData {

    private OrderCount order;

}
