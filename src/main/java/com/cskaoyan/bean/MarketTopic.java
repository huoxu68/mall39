package com.cskaoyan.bean;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Data
public class MarketTopic {
    private Integer id;

    private String title;

    private String subtitle;

    private Double price;

    private String readCount;

    private String picUrl;

    private Integer sortOrder;

    private Integer[] goods;

    private Date addTime;

    private Date updateTime;

    private Boolean deleted;

    private String content;
}