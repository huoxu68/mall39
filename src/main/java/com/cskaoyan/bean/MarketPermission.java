package com.cskaoyan.bean;

import lombok.Data;

import java.util.Date;

@Data
public class MarketPermission {
    private Integer id;

    private Integer roleId;

    private String permission;

    private Date addTime;

    private Date updateTime;

    private Boolean deleted;
}