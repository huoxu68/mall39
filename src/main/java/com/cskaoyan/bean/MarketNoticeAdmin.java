package com.cskaoyan.bean;

import lombok.Data;

import java.util.Date;

@Data
public class MarketNoticeAdmin {
    private Integer id;

    private Integer noticeId;

    private String noticeTitle;

    private Integer adminId;

    private Date readTime;

    private Date addTime;

    private Date updateTime;

    private Boolean deleted;
}