package com.cskaoyan.bean;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @ClassName: OrderStatistics
 * @Description:
 * @Since: 2022/06/04 15:16
 * @Author: ZhangHuixiang
 */
@Data
public class OrderStatistics {
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date day;
    private Integer orders;
    private Integer customers;
    private BigDecimal amount;
    private BigDecimal pcr;
}
