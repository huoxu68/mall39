package com.cskaoyan.bean;

import lombok.Data;

import java.util.Date;

/**
 * @ClassName: Address
 * @Description:
 * @Since: 2022/06/03 21:20
 * @Author: ZhangHuixiang
 */

@Data
public class Address {
    private Integer id;
    private String name;
    private Integer userId;
    private String province;
    private String city;
    private String county;
    private String addressDetail;
    private String areaCode;
    private String tel;
    private Boolean isDefault;
    private Date addTime;
    private Date updateTime;
    private Boolean deleted;
}
