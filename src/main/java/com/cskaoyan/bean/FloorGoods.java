package com.cskaoyan.bean;

import lombok.Data;

import java.util.List;

/**
 * @ClassName: FloorGoods
 * @Description:
 * @Since: 2022/06/05 19:39
 * @Author: ZhangHuixiang
 */
@Data
public class FloorGoods {
    private Integer id;
    private String name;
    private List<MarketGoods> goodsList;
}
