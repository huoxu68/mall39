package com.cskaoyan.bean;

import lombok.Data;

import java.util.List;

/**
 * @ClassName: GoodsComments
 * @Description:
 * @Since: 2022/06/05 21:21
 * @Author: ZhangHuixiang
 */

@Data
public class GoodsComments {
    Integer count;
    List<MarketComment> data;
}
