package com.cskaoyan.bean;

import lombok.Data;

import java.util.Date;

@Data
public class MarketUser {
    private Integer id;

    private String username;

    private String password;

    private Byte gender;

    private Date birthday;

    private Date lastLoginTime;

    private String lastLoginIp;

    private Byte userLevel;

    private String nickname;

    private String mobile;

    private String avatar;

    private String weixinOpenid;

    private String sessionKey;

    private Byte status;

    private Date addTime;

    private Date updateTime;

    private Boolean deleted;
}