package com.cskaoyan.bean.category.vo;

import com.cskaoyan.bean.MarketCategory;
import lombok.Data;

import java.util.List;

/**
 * @author zxy
 * @version 1.0.0
 * @ClassName CurrentVo.java
 * @Description TODO
 * @createTime 2022年06月07日 09:28:00
 */
@Data
public class CurrentVo {
    MarketCategory currentCategory;
    List<MarketCategory> currentSubCategory;
}
