package com.cskaoyan.bean.category.vo;

import com.cskaoyan.bean.MarketCategory;
import lombok.Data;

import java.util.List;

/**
 * @author zxy
 * @version 1.0.0
 * @ClassName CategoryIndex.java
 * @Description TODO
 * @createTime 2022年06月07日 14:26:00
 */
@Data
public class CategoryIndex {
    List<MarketCategory> categoryList;
    MarketCategory currentCategory;
    List<MarketCategory>  currentSubCategory;
}
