package com.cskaoyan.bean.category;

import com.cskaoyan.bean.vo.CategoryList;
import lombok.Data;

import java.util.List;


/**
 * @author jcp
 * @Description list中的父类
 * @date 2022年06月05日 15:15
 */

@Data
public class CategoryListL1 {
    private Integer id;
    private String  name;
    private String  keywords;
    private String  desc;
    private String  iconUrl;
    private String  picUrl;
    private String  level;
    private List<CategoryListL2> children;

}
