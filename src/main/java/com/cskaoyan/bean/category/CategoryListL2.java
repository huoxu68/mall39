package com.cskaoyan.bean.category;

import lombok.Data;

/**
 * @author jcp
 * @Description CategoryListL1的子类
 * @date 2022年06月05日 15:19
 */
@Data
public class CategoryListL2 {
    private Integer id;
    private String  name;
    private String  keywords;
    private String  desc;
    private String  iconUrl;
    private String  picUrl;
    private String  level;
}
