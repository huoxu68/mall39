package com.cskaoyan.bean.category;

import lombok.Data;

/**
 * @author jcp
 * @Description admin/category/l1的类
 * @date 2022年06月05日 16:19
 */
@Data
public class CategoryL1 {
    private String label;
    private Integer value;
}
