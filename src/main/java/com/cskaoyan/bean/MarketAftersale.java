package com.cskaoyan.bean;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
public class MarketAftersale {

    //退款类型说明
    //private String typeDesc;

    private Integer id;

    private String aftersaleSn;

    private Integer orderId;

    private Integer userId;

    private Short type;

    private String reason;

    private BigDecimal amount;

    private String[] pictures;

    private String comment;

    private Short status;

    private Date handleTime;

    private Date addTime;

    private Date updateTime;

    private Boolean deleted;

}