package com.cskaoyan.bean;

import lombok.Data;

/**
 * @ClassName: RegisterBo
 * @Description:
 * @Since: 2022/06/07 17:47
 * @Author: ZhangHuixiang
 */

@Data
public class RegisterBo {
    private String username;
    private String password;
    private String mobile;
    private String code;
    private String wxCode;
}
