package com.cskaoyan.bean;

import lombok.Data;

import java.util.List;

/**
 * @ClassName: WxGoodsCategory
 * @Description:
 * @Since: 2022/06/05 20:09
 * @Author: ZhangHuixiang
 */

@Data
public class WxGoodsCategory {
    private MarketCategory currentCategory;
    private List<MarketCategory> brotherCategory;
    private MarketCategory parentCategory;
}
