package com.cskaoyan.bean.search.vo;

import com.cskaoyan.bean.MarketKeyword;
import com.cskaoyan.bean.MarketSearchHistory;
import lombok.Data;

import java.util.LinkedList;
import java.util.List;

/**
 * @author zxy
 * @version 1.0.0
 * @ClassName SearchIndexVo.java
 * @Description TODO
 * @createTime 2022年06月07日 11:10:00
 */
@Data
public class SearchIndexVo {
    MarketKeyword defaultKeyword;
   List<HistoryKeyword> historyKeywordList;
    List<MarketKeyword> hotKeywordList;
}
