package com.cskaoyan.bean.search.vo;

import lombok.Data;

/**
 * @author zxy
 * @version 1.0.0
 * @ClassName HistoryKeyword.java
 * @Description TODO
 * @createTime 2022年06月07日 11:43:00
 */
@Data
public class HistoryKeyword {
    String keyword;
}
