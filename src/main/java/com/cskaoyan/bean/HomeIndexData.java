package com.cskaoyan.bean;

import lombok.Data;

import java.util.List;

/**
 * @ClassName: HomeIndexData
 * @Description:
 * @Since: 2022/06/05 15:45
 * @Author: ZhangHuixiang
 */
@Data
public class HomeIndexData {
    private List<MarketGoods> newGoodsList;
    private List<MarketCoupon> couponList;
    private List<MarketCategory> channel;
    private List<MarketAd> banner;
    private List<MarketBrand> brandList;
    private List<MarketGoods> hotGoodsList;
    private List<MarketTopic> topicList;
    private List<FloorGoods> floorGoodsList;
}
