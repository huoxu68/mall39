package com.cskaoyan.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @ClassName: UserData
 * @Description:
 * @Since: 2022/06/03 13:39
 * @Author: ZhangHuixiang
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class BaseData<T> {
    private Integer total;
    private Integer pages;
    private Integer limit;
    private Integer page;
    private List<T> list;
}
