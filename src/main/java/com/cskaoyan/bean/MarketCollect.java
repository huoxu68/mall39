package com.cskaoyan.bean;

import lombok.Data;

import java.util.Date;

@Data
public class MarketCollect {
    private Integer id;

    private Integer userId;

    private Integer valueId;

    private Byte type;

    private Date addTime;

    private Date updateTime;

    private Boolean deleted;
}