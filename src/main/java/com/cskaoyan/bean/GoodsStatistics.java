package com.cskaoyan.bean;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @ClassName: GoodsStatistics
 * @Description:
 * @Since: 2022/06/04 15:45
 * @Author: ZhangHuixiang
 */

@Data
public class GoodsStatistics {
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date day;
    private Integer orders;
    private Integer products;
    private BigDecimal amount;
}
