package com.cskaoyan.bean.topic.vo;

import com.cskaoyan.bean.MarketGoods;
import com.cskaoyan.bean.MarketTopic;
import lombok.Data;

import java.util.List;

/**
 * @author zxy
 * @version 1.0.0
 * @ClassName WxTopicVo.java
 * @Description TODO
 * @createTime 2022年06月07日 17:15:00
 */
@Data
public class WxTopicVo {
    List<MarketGoods> goods;
    MarketTopic topic;
}
