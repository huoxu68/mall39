package com.cskaoyan.bean;

import lombok.Data;

/**
 * @ClassName: PasswordUpdateBo
 * @Description:
 * @Since: 2022/06/07 12:31
 * @Author: ZhangHuixiang
 */

@Data
public class PasswordUpdateBo {
    private String newPassword2;
    private String oldPassword;
    private String newPassword;
}
