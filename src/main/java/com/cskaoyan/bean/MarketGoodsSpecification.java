package com.cskaoyan.bean;

import lombok.Data;

import java.util.Date;

@Data
public class MarketGoodsSpecification {
    private Integer id;

    private Integer goodsId;

    private String specification;

    private String value;

    private String picUrl;

    private Date addTime;

    private Date updateTime;

    private Boolean deleted;
}
