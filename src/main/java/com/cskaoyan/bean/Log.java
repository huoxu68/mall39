package com.cskaoyan.bean;

import lombok.Data;

import java.util.Date;

/**
 * @ClassName: Log
 * @Description: 操作日志
 * @Since: 2022/06/04 22:53
 * @Author: FanLujia
 */

@Data
public class Log {

    private Integer id;
    private String admin;
    private String ip;
    private Integer type;//操作分类
    private String action;//操作动作
    private Integer status;//操作状态
    private String result;//操作结果，或者成功消息，或者失败消息
    private String comment;//补充信息
    private Date add_time;//创建时间
    private Date update_time;//更新时间
    private Integer deleted;//逻辑删除

}