package com.cskaoyan.bean.vo;

import com.cskaoyan.bean.MarketKeyword;
import lombok.Data;

import java.util.List;

/**
 * 父子类目以及品牌商
 *
 * @author 李贵灏
 * @since 2022/06/04 20:23
 */
@Data
public class GoodsCategoryVo {
    private List<CategoryList> categoryList;
    private List<Children> brandList;
    private List<MarketKeyword> allKeywords;
}
