package com.cskaoyan.bean.vo;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author zhongyimin
 * @since 2022/06/07 11:21
 */
@Data
public class CommonCoupon {
    Integer id;
    BigDecimal min;
    Short days;
    BigDecimal discount;
    String desc;
    String name;
    String tag;

}
