package com.cskaoyan.bean.vo;

import com.cskaoyan.bean.MarketGoods;
import com.cskaoyan.bean.MarketTopic;
import com.cskaoyan.bean.bo.TopicBo;
import lombok.Data;

import java.util.List;

/**
 * @ClassName: TopicReadDataVo
 * @Description: 专题编辑页面回显返回前端参数类
 * @Since: 2022/06/06 17:37
 * @Author: FanLujia
 */

@Data
public class TopicReadDataVo {

    private List<MarketGoods> goodsList;

    private TopicBo topic;


}