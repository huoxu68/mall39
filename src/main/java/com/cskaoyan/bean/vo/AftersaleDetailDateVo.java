package com.cskaoyan.bean.vo;

import com.cskaoyan.bean.MarketAftersale;
import com.cskaoyan.bean.MarketOrder;
import com.cskaoyan.bean.MarketOrderGoods;
import com.cskaoyan.bean.po.OrderGoods;
import lombok.Data;

import java.util.List;

/**
 * @ClassName: AftersaleDetailDateVo
 * @Description: 售后信息显示
 * @Since: 2022/06/09 15:26
 * @Author: FanLujia
 */

@Data
public class AftersaleDetailDateVo {

    private List<MarketOrderGoods> orderGoods;

    private MarketAftersale aftersale;

    private MarketOrder order;



}