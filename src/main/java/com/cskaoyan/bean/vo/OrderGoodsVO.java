package com.cskaoyan.bean.vo;

import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * @author zhongyimin
 * @since 2022/06/04 18:24
 */
@Data
public class OrderGoodsVO {
    private Integer id;
    private Integer orderId;
    private Integer goodsId;
    private String goodsName;
    private String goodsSn;
    private Integer productId;
    private Integer number;
    private Double price;
    //需要返回数组，得做处理
    private String[] specifications;
    private String picUrl;
    //订单商品评论，如果是-1，则超期不能评价；如果是0，则可以评价；
    // 如果其他值，则是comment表里面的评论ID
    private Integer comment;
    private Date addTime;
    private Date updateTime;
    private Boolean deleted;

}
