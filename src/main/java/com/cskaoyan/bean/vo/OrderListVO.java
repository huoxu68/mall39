package com.cskaoyan.bean.vo;

import lombok.Data;

import java.util.Date;

/**
 * 订单类-- 用于list请求的显示
 * @author zhongyimin
 * @since 2022/06/04 18:31
 */
@Data
public class OrderListVO {
    private Integer id;
    private Integer userId;
    // 订单编号
    private String orderSn;
    private Integer orderStatus;
    //售后状态，0是可申请，1是用户已申请，2是管理员审核通过，3是管理员退款成功，4是管理员审核拒绝，5是用户已取消
    private Integer aftersaleStatus;
    // 收货人名称
    private String consignee;

    private String mobile;
    private String address;
    // 用户订单留言
    private String message;

    private Double goodsPrice;
    // 配送费用
    private Double freightPrice;
    // 优惠劵减免
    private Double couponPrice;
    // 用户积分减免
    private Double integralPrice;
    // 团购优惠减免
    private Double grouponPrice;
    // 订单费用
    private Double orderPrice;
    //实际付款
    private Double actualPrice;
    private String shipChannel;
    private String shipSn;
    private Date shipTime;
  /*  // 微信付款编号
    private String payId;
    private Date payTime;
    //实际退款金额
    private Double refundAmount;
    // 退款方式
    private String refundType;
    // 退款备注
    private String refundContent;

    private Date refundTime;
    //用户确认收货时间
    private Date confirmTime;

    private Date endTime;*/

    //待评价订单商品数量
    private Integer comments;
    private Date addTime;
    private Date updateTime;
    private Boolean deleted;

}
