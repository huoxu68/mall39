package com.cskaoyan.bean.vo;

import lombok.Data;

/**
 * @ClassName: RoleInAdmin
 * @Description: admin显示页面需要的role数据
 * @Since: 2022/06/05 17:02
 * @Author: FanLujia
 */

@Data
public class RoleInAdmin {

    private Integer value;//role的id
    private String label;//role的name

}