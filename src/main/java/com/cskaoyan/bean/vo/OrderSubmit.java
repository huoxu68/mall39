package com.cskaoyan.bean.vo;

import lombok.Data;

/**
 * @author zhongyimin
 * @since 2022/06/09 11:22
 */
@Data
public class OrderSubmit {
    Integer grouponLinkId;
    Integer orderId;

}
