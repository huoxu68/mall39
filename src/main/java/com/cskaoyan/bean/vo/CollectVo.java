package com.cskaoyan.bean.vo;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @ClassName: CollectVo
 * @Description:
 * @Since: 2022/06/06 21:09
 * @Author: changxing
 */

@Data
public class CollectVo {

    /**
     * brief :
     * picUrl :
     * valueId : 1181194
     * name : 1223
     * id : 122
     * type : 0
     * retailPrice : 1000.0
     */
    private String brief;
    private String picUrl;
    private Integer valueId;
    private String name;
    private Integer id;
    private Byte type;
    private BigDecimal retailPrice;

}
