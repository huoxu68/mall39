package com.cskaoyan.bean.vo;

import lombok.Data;

/**
 * 足迹的显示
 *
 * @author 李贵灏
 * @since 2022/06/07 14:25
 */
@Data
public class WxFootprintListVo {
    private Integer goodsId;
    private Integer id;
    private String addTime;
    private String name;
    private String picUrl;
    private Double retailPrice;
    private String brief;
}
