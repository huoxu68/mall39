package com.cskaoyan.bean.vo;

import lombok.Data;

import java.util.Date;

/**
 * @author zhongyimin
 * @since 2022/06/06 21:35
 */
@Data
public class MyCoupon {
    Integer id;
    Integer cid;
    Integer discount;
    Integer min;
    String desc;
    String name;
    String tag;
    Date startTime;
    Date endTime;
    Boolean available;

}
