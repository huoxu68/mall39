package com.cskaoyan.bean.vo;

import com.cskaoyan.bean.HandleOption;
import lombok.Data;

import java.util.List;

/**
 * @ClassName: OrderListOfWx
 * @Description: 小程序订单显示
 * @Since: 2022/06/07 14:18
 * @Author: FanLujia
 */

@Data
public class OrderListOfWx {

    private Integer id;

    //订单状态名称:"已付款",
    private String orderStatusText;

    private String orderStatus;

    //售后状态，0是可申请，1是用户已申请，2是管理员审核通过，3是管理员退款成功，4是管理员审核拒绝，5是用户已取消
    private Short aftersaleStatus;

    //是否是团购
    private Boolean isGroupin;

    private String orderSn;

    private Double actualPrice;

    private List<OrderGoodsVO> goodsList;

    private HandleOption handleOption;


}

