package com.cskaoyan.bean.vo;

import com.cskaoyan.bean.MarketComment;
import lombok.Data;

/**
 * WxCommentlistVo
 * wx 上 商品评论的显示
 * @author 李贵灏
 * @since 2022/06/06 19:58
 */
@Data
public class WxCommentListVo {
    private UserCommentVo userInfo;
    private String addTime;
    private String[] picList;
    private String adminContent;
    private String content;

}
