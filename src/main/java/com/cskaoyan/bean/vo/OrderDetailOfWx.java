package com.cskaoyan.bean.vo;

import com.cskaoyan.bean.po.OrderGoods;
import lombok.Data;

import java.util.List;

/**
 * @ClassName: OrderDetailOfWx
 * @Description: 小程序的订单详情
 * @Since: 2022/06/07 20:21
 * @Author: FanLujia
 */

@Data
public class OrderDetailOfWx {

    private String[] expressInfo;

    private OrderInfoOfDetileOfWx orderInfo;

    private List<OrderGoods> orderGoods;


}