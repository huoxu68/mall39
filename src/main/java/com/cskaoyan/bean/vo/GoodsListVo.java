package com.cskaoyan.bean.vo;

import lombok.Data;
import org.springframework.boot.json.JacksonJsonParser;

import java.util.List;

/**
 * 商品表的信息
 * @ClassName:Goods
 * @author 李贵灏
 * @since 2022/06/04 11:01
 */
@Data
public class GoodsListVo {

    private Integer id;
    private String goodsSn;
    private String name;
    private Integer categoryId;
    private Integer brandId;
    // 数据库存储的是字符串 而返回的数据是lsit，需要进行转换
    private String[] gallery;
    private String keywords;
    private String brief;
    private Boolean isOnSale;
    private Integer sortOrder;
    private String picUrl;
    private String shareUrl;
    private Boolean isNew;
    private Boolean isHot;
    private String unit;
    private Double counterPrice;
    private Double retailPrice;
    private String detail;
    private String addTime;
    private String updateTime;
    private Boolean deleted;
}
