package com.cskaoyan.bean.vo;

import lombok.Data;

import java.util.List;

/**
 * @author 李贵灏
 *   父类目
 * @since 2022/06/04 20:15
 */
@Data
public class CategoryList {
    private Integer value;
    private String label;
    private List<Children> children;
}
