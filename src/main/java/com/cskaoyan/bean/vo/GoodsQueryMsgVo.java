package com.cskaoyan.bean.vo;

import com.cskaoyan.bean.*;
import lombok.Data;

import java.util.List;

/**
 * 商品信息的回溯
 *
 * @author 李贵灏
 * @since 2022/06/04 21:45
 */
@Data
public class GoodsQueryMsgVo {
    private  List<Integer> categoryIds;
    private MarketGoods goods;
    private List<MarketGoodsAttribute> attributes;
    private List<MarketGoodsSpecification> specifications;
    private List<MarketGoodsProduct> products;
 }
