package com.cskaoyan.bean.vo;

import com.cskaoyan.bean.po.Order;
import com.cskaoyan.bean.po.OrderGoods;
import lombok.Data;

import java.util.List;

/**
 * 显示订单详情信息使用的类
 * @author zhongyimin
 * @since 2022/06/04 18:39
 */
@Data
public class OrderDetailInfoVO {
    private List<OrderGoods> orderGoods;
    private UserInOrder user;
    private OrderVO order;
}
