package com.cskaoyan.bean.vo;

import lombok.Data;

/**
 * CommentCountVo
 *
 * @author 李贵灏
 * @since 2022/06/06 21:16
 */
@Data
public class CommentCountVo {
    private Integer hasPicCount;
    private Integer allCount;
}
