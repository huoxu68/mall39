package com.cskaoyan.bean.vo;


import org.springframework.boot.json.JacksonJsonParser;

import java.util.List;

/**
 * prducts
 * @author 李贵灏
 * @since 2022/06/04 21:29
 */
public class ProductsVo {
    private Integer id;
    private Integer goodsId;
    private List<Object> specifications;
    private Double price;
    private Integer number;
    private String url;
    private String addTime;
    private String updateTime;
    private boolean deleted;
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Integer goodsId) {
        this.goodsId = goodsId;
    }

    public List<Object> getSpecifications() {
        return specifications;
    }

    /**
     * 数据库中存储的是json字符串
     * @param specifications
     */
    public void setSpecifications(String specifications) {
        JacksonJsonParser jacksonJsonParser = new JacksonJsonParser();
        this.specifications = jacksonJsonParser.parseList(specifications);
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getAddTime() {
        return addTime;
    }

    public void setAddTime(String addTime) {
        this.addTime = addTime;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public boolean isDeleted() {
        return deleted;
    }

    /**
     * 转化为boolean型
     * @param deleted
     */
    public void setDeleted(Integer deleted) {
        if(deleted==1){
            this.deleted  = true;
        }else if(deleted==0){
            this.deleted  = false;
        }
    }


}
