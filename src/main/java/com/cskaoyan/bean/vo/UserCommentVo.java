package com.cskaoyan.bean.vo;

import lombok.Data;

/**
 * UserCommentVo
 *
 * @author 李贵灏
 * @since 2022/06/06 20:01
 */
@Data
public class UserCommentVo {
    private String nickName;
    private String avatarUrl;
}
