package com.cskaoyan.bean.vo;

import lombok.Data;

/**
 * @author zhongyimin
 * @since 2022/06/04 18:30
 */
@Data
public class UserInOrder {
    private String nickname;
    private String avatar;

}
