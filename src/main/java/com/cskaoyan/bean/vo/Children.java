package com.cskaoyan.bean.vo;

import lombok.Data;

/**
 * @author 李贵灏
 * @since 2022/06/04 20:19
 * 子类目
 */
@Data
public class Children {
    private Integer value;
    private String label;
}
