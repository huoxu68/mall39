package com.cskaoyan.bean.vo;

import com.cskaoyan.bean.HandleOption;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Data
public class OrderInfoOfDetileOfWx {

    //新增订单状态显示名称
    private String orderStatusText;
    //新增权限显示类
    private HandleOption handleOption;
    // //是否是团购
    private Boolean isGroupin;
    //新增goods列表
    private List<OrderGoodsVO> goodsList;

    //新增快递相关
    private String expName;//快递名字
    private String expCode;//快递代码
    private String expNo;//快递单号


    private Integer id;

    private Integer userId;

    private String orderSn;

    //订单状态
    private Short orderStatus;

    //售后状态，0是可申请，1是用户已申请，2是管理员审核通过，3是管理员退款成功，4是管理员审核拒绝，5是用户已取消
    private Short aftersaleStatus;

    //收货人
    private String consignee;

    private String mobile;

    private String address;

    private String message;

    private BigDecimal goodsPrice;

    //配送费用
    private BigDecimal freightPrice;

    //优惠券减免
    private BigDecimal couponPrice;

    //用户积分减免
    private BigDecimal integralPrice;

    private BigDecimal grouponPrice;

    private BigDecimal orderPrice;

    //实付费用
    private BigDecimal actualPrice;

    private String payId;

    private Date payTime;

    //发货编号
    private String shipSn;

    //发货快递公司
    private String shipChannel;

    //发货时间
    private Date shipTime;

    //退款相关
    private BigDecimal refundAmount;

    private String refundType;

    private String refundContent;

    private Date refundTime;

    private Date confirmTime;

    private Short comments;

    private Date endTime;

    private Date addTime;

    private Date updateTime;

    private Boolean deleted;



}