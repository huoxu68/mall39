package com.cskaoyan.bean;

import lombok.Data;

import java.util.Date;

/**
 * @ClassName: Admin
 * @Description: 管理员新增数据接收
 * @Since: 2022/06/04 17:29
 * @Author: FanLujia
 */

@Data
public class Admin {
    private Integer id;
    private String username;//管理员名称
    private String password;
    private String lastLoginIp;//最近一次登录IP地址
    private String lastLoginTime;
    private String avatar; //头像图片
    private Date addTime; //创建时间
    private Date updateTime; //更新时间
    private Integer deleted; //逻辑删除
    private Integer[] roleIds; //角色列表

}