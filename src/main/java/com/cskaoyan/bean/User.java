package com.cskaoyan.bean;

import lombok.Data;

import java.util.Date;

/**
 * @ClassName: UserInfo
 * @Description:
 * @Since: 2022/06/03 13:06
 * @Author: ZhangHuixiang
 */
@Data
public class User {
    private Integer id;
    private String username;
    private String password;
    private Integer gender;
    private Date lastLoginTime;
    private String lastLoginIp;
    private Integer userLevel;
    private String nickname;
    private String mobile;
    private String avatar;
    private String weixinOpenid;
    private String sessionKey;
    private Integer status;
    private Date addTime;
    private Date updateTime;
    private Boolean deleted;
}
