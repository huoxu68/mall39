package com.cskaoyan.bean.region;

import lombok.Data;

/**
 * @author jcp
 * @Description  市类中的区县
 * @date 2022年06月04日 15:02
 */
@Data
public class AreaOfCity {
    private Integer id;
    private String name;
    private Integer type;
    private Integer code;
}
