package com.cskaoyan.bean.region;

import lombok.Data;

import java.util.List;

/**
 * @author jcp
 * @Description  行政区域 data类中中的list类  省类
 * @date 2022年06月04日 14:43
 */
@Data
public class ProvinceListOfDataOfRegionVO {
    private Integer id;
    private String name;
    private Integer type;
    private Integer code;
    private List<CityOfProvinveOfListOfDataOfRegionVO> children;
}
