package com.cskaoyan.bean.region;

import lombok.Data;

import java.util.List;

/**
 * @author jcp
 * @Description list类中的children类  市类
 * @date 2022年06月04日 14:46
 */
@Data
public class CityOfProvinveOfListOfDataOfRegionVO {
    private Integer code;
    private Integer id;
    private String name;
    private Integer type;
    private List<AreaOfCity> children;
}
