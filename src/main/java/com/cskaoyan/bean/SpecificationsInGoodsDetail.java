package com.cskaoyan.bean;

import lombok.Data;

import java.util.List;

/**
 * @ClassName: SpecificationsInGoodsDetail
 * @Description:
 * @Since: 2022/06/06 21:19
 * @Author: ZhangHuixiang
 */
@Data
public class SpecificationsInGoodsDetail {
    private String name;
    private List<MarketGoodsSpecification> valueList;
}
