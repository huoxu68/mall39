package com.cskaoyan.bean;

import lombok.Data;

/**
 * @ClassName: BaseRespVo
 * @Description:
 * @Since: 2022/06/03 12:58
 * @Author: ZhangHuixiang
 */
@Data
public class BaseRespVo<T> {

    private Integer errno;
    private T data;
    private String errmsg;

    public static <T> BaseRespVo ok(T data) {
        BaseRespVo baseRespVo = new BaseRespVo();
        baseRespVo.setErrno(0);
        baseRespVo.setErrmsg("成功");
        baseRespVo.setData(data);
        return baseRespVo;
    }
    public static <T> BaseRespVo invalidData(String msg) {
        BaseRespVo baseRespVo = new BaseRespVo();
        baseRespVo.setErrno(504);
        baseRespVo.setErrmsg(msg);
        return baseRespVo;
    }
    public static <T> BaseRespVo invalidData() {
        BaseRespVo baseRespVo = new BaseRespVo();
        baseRespVo.setErrno(504);
        baseRespVo.setErrmsg("更新数据已失效");
        return baseRespVo;
    }
    public static <T> BaseRespVo invalidParameter(String msg) {
        BaseRespVo baseRespVo = new BaseRespVo();
        baseRespVo.setErrno(400);
        baseRespVo.setErrmsg(msg);
        return baseRespVo;
    }

    public static <T> BaseRespVo internalException() {
        BaseRespVo baseRespVo = new BaseRespVo();
        baseRespVo.setErrno(503);
        return baseRespVo;
    }

    public static <T> BaseRespVo unAuthorizing() {
        BaseRespVo baseRespVo = new BaseRespVo();
        baseRespVo.setErrno(501);
        return baseRespVo;
    }

    public static BaseRespVo fine(){
        BaseRespVo baseRespVo = new BaseRespVo();
        baseRespVo.setErrno(0);
        baseRespVo.setErrmsg("成功");
        return baseRespVo;
    }

    public static BaseRespVo ok() {
        /**
         * @title
         * @description 不带data的返回
         * @author zxy
         * @updateTime 2022/6/4 20:28
         * @return: com.cskaoyan.bean.BaseRespVo
         * @throws
         */
        BaseRespVo baseRespVo = new BaseRespVo();
        baseRespVo.setErrno(0);
        baseRespVo.setErrmsg("成功");
        return baseRespVo;
    }
}
