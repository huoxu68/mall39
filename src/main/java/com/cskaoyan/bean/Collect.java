package com.cskaoyan.bean;

import lombok.Data;

/**
 * @ClassName: Collect
 * @Description:
 * @Since: 2022/06/03 22:09
 * @Author: ZhangHuixiang
 */

@Data
public class Collect {
    private Integer id;
    private Integer userId;
    private Integer valueId;
    private Integer type;
    private String addTime;
    private String updateTime;
    private Boolean deleted;
}
