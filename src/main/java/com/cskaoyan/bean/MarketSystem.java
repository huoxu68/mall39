package com.cskaoyan.bean;

import lombok.Data;

import java.util.Date;

@Data
public class MarketSystem {
    private Integer id;

    private String keyName;

    private String keyValue;

    private Date addTime;

    private Date updateTime;

    private Boolean deleted;
}