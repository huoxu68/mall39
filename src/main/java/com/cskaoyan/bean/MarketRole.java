package com.cskaoyan.bean;

import lombok.Data;

import java.util.Date;

@Data
public class MarketRole {
    private Integer id;

    private String name;

    private String desc;

    private Boolean enabled;

    private Date addTime;

    private Date updateTime;

    private Boolean deleted;
}