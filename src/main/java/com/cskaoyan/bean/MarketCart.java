package com.cskaoyan.bean;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
public class MarketCart {
    private Integer id;
    private Integer userId;
    private Integer goodsId;
    private String goodsSn;
    private String goodsName;
    private Integer productId;
    private Double price;
    private Integer number;
    private String[] specifications;
    private Boolean checked;
    private String picUrl;
    private Date addTime;
    private Date updateTime;
    private Boolean deleted;
}