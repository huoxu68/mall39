package com.cskaoyan.bean;

import lombok.Data;

import java.util.List;

/**
 * @ClassName: ConmmentsInGoodsDetail
 * @Description:
 * @Since: 2022/06/06 21:21
 * @Author: ZhangHuixiang
 */

@Data
public class CommentsInGoodsDetail {
    private Integer count;
    private List<MarketComment> data;
}
