package com.cskaoyan.bean;

import lombok.Data;

import java.util.Date;

@Data
public class MarketSearchHistory {
    private Integer id;

    private Integer userId;

    private String keyword;

    private String from;

    private Date addTime;

    private Date updateTime;

    private Boolean deleted;
}