package com.cskaoyan.bean;

import lombok.Data;

import java.util.List;

/**
 * @ClassName: GoodsSpecifications
 * @Description:
 * @Since: 2022/06/05 21:16
 * @Author: ZhangHuixiang
 */
@Data
public class GoodsSpecifications {
    private String name;
    private List<MarketGoodsSpecification> valueList;
}
