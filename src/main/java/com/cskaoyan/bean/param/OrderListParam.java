package com.cskaoyan.bean.param;

import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * @author zhongyimin
 * @since 2022/06/04 20:15
 */
@Data
public class OrderListParam {
    private List<Integer> orderStatusArray;
    private Integer orderId;
    private Date[] timeArray;
    private Date start;
    private Date end;
    private Integer userId;
    private String orderSn;

}
