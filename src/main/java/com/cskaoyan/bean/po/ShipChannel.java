package com.cskaoyan.bean.po;

import lombok.Data;

/**
 * @author zhongyimin
 * @since 2022/06/05 21:31
 */
@Data
public class ShipChannel {
    // Integer id;
    String code;
    String name;

}
