package com.cskaoyan.bean.po;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

/**
 *
 * 与数据库market_issue表格直接交互的类
 * @author zhongyimin
 * @since 2022/06/04 15:56
 */
@Data
public class Issue {
    private Integer id;
    private String question;
    private String answer;
    private Date addTime;
    private Date updateTime;
    private Boolean deleted;


}
