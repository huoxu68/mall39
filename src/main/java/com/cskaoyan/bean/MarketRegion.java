package com.cskaoyan.bean;

import lombok.Data;

@Data
public class MarketRegion {
    private Integer id;

    private Integer pid;

    private String name;

    private Byte type;

    private Integer code;
}