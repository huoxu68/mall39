package com.cskaoyan.bean;

import lombok.Data;

import java.util.Date;

@Data
public class MarketLog {
    private Integer id;

    private String admin;

    private String ip;

    private Integer type;

    private String action;

    private Boolean status;

    private String result;

    private String comment;

    private Date addTime;

    private Date updateTime;

    private Boolean deleted;
}