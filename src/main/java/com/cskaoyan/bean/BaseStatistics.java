package com.cskaoyan.bean;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName: BaseStatus
 * @Description:
 * @Since: 2022/06/04 14:44
 * @Author: ZhangHuixiang
 */

public class BaseStatistics<T> {
    private List<String> columns;
    private List<T> rows;

    public BaseStatistics() {
    }

    public BaseStatistics(List<T> data) {
        this.setRows(data);
    }

    public List<String> getColumns() {
        return columns;
    }

    public void setColumns(List<String> columns) {
        this.columns = columns;
    }

    public List<T> getRows() {
        return rows;
    }

    public void setRows(List<T> rows) {
        if (rows.size() > 0) {
            T row = rows.get(0);
            Field[] declaredFields = row.getClass().getDeclaredFields();
            ArrayList<String> columns = new ArrayList<>();
            for (Field declaredField : declaredFields) {
                columns.add(declaredField.getName());
            }
            this.setColumns(columns);
            this.rows = rows;
        }
    }
}
