package com.cskaoyan.bean.bo;

import lombok.Data;

/**
 * WxCommentBo
 * Wx 显示评论的请求参数
 * @author 李贵灏
 * @since 2022/06/06 20:19
 */
@Data
public class WxCommentBo {
    private Integer valueId;
    private Integer type;
    private Integer limit;
    private Integer page;
    private Integer showType;
}
