package com.cskaoyan.bean.bo;

import lombok.Data;

/**
 * @author 李贵灏
 * @since 2022/06/05 23:21
 */
@Data
public class CommentIdBo {
    private Integer userId;
    private Integer valueId;
}
