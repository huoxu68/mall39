package com.cskaoyan.bean.bo;

import com.cskaoyan.bean.*;
import com.cskaoyan.bean.vo.GoodsListVo;
import com.cskaoyan.bean.vo.ProductsVo;
import lombok.Data;

import java.util.List;

/**
 * 商品的创建，需要接收数据
 *
 * @author 李贵灏
 * @since 2022/06/05 09:18
 */
@Data
public class GoodsCreateBo {
    private MarketGoods goods;
    private List<MarketGoodsSpecification> specifications;
    private List<MarketGoodsProduct> products;
    private List<MarketGoodsAttribute> attributes;
}
