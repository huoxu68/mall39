package com.cskaoyan.bean.bo;

import lombok.Data;

import java.util.List;

/**
 * @author 李贵灏
 * @since 2022/06/05 15:58
 */
@Data
public class GoodsBo { private Integer id;
    private String goodsSn;
    private String name;
    private Integer categoryId;
    private Integer brandId;
    // 数据库存储的是字符串 而返回的数据是lsit，需要进行转换
    private String[] gallery;
    private String keywords;
    private String brief;
    private String  isOnSale;
    private Integer sortOrder;
    private String picUrl;
    private String shareUrl;
    private String  isNew;
    private String  isHot;
    private String unit;
    private Double counterPrice;
    private Double retailPrice;
    private String addTime;
    private String updateTime;
    private String  deleted;
}
