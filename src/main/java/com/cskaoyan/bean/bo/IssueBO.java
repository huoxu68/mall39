package com.cskaoyan.bean.bo;

import lombok.Data;

import java.util.Date;

/**
 * @author zhongyimin
 * @since 2022/06/04 18:00
 */
@Data
public class IssueBO {
    private Integer id;
    private String question;
    private String answer;
    private Date addTime;
    private Date updateTime;
    private Boolean deleted;


}
