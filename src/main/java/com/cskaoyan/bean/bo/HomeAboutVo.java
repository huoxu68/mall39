package com.cskaoyan.bean.bo;

/**
 * @author zxy
 * @version 1.0.0
 * @ClassName HomeAboutBo.java
 * @Description TODO
 * @createTime 2022年06月10日 09:12:00
 */

public class HomeAboutVo {

    /**
     * qq : 65419655
     * address : 啦啦啦啦
     * phone : 165156553
     * latitude : 12.222
     * name : 哈哈kk
     * longitude : 11.11
     */
    private String qq;
    private String address;
    private String phone;
    private String latitude;
    private String name;
    private String longitude;

    public void setQq(String qq) {
        this.qq = qq;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getQq() {
        return qq;
    }

    public String getAddress() {
        return address;
    }

    public String getPhone() {
        return phone;
    }

    public String getLatitude() {
        return latitude;
    }

    public String getName() {
        return name;
    }

    public String getLongitude() {
        return longitude;
    }
}
