package com.cskaoyan.bean.bo;

import lombok.Data;

/**
 * wx 订单中的评价
 *
 * @author 李贵灏
 * @since 2022/06/07 20:22
 */
@Data
public class OrderCommentBo {
    private Integer orderGoodsId;
    private String[] picUrls;
    private Boolean hasPicture;
    private Short star;
    private String content;
}
