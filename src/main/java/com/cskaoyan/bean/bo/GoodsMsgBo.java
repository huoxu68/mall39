package com.cskaoyan.bean.bo;

import lombok.Data;

/**
 * 查询传入的数据
 *goodsId   GoodsSn   name：
 * @author 李贵灏
 * @since 2022/06/04 15:24
 */
@Data
public class GoodsMsgBo {
    private Integer goodsId;
    private Integer goodsSn;
    private String name;
}
