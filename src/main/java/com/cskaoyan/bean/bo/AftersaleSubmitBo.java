package com.cskaoyan.bean.bo;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * @ClassName: AftersaleSubmitBo
 * @Description: 售后申请请求数据bean
 * @Since: 2022/06/09 12:20
 * @Author: FanLujia
 */

@Data
public class AftersaleSubmitBo {
    /**
     * reason : 退款原因
     * amount : 5307
     * typeDesc : 未收货退款
     * orderId : 560
     * type : 0
     * pictures : ["http://182.92.235.201:8083/wx/storage/fetch/au3kgm4c6wg88ynxw0ng.jpg"]
     */
    private String reason;
    private Double amount;
    private String typeDesc; //退款类型描述
    private String orderId;
    private Integer type;  //退款类型
    private List<String> pictures;

}