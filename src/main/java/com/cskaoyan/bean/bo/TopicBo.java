package com.cskaoyan.bean.bo;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 接收前端传进来的topic数据，goodsId为数组类型
 * @param
 * @return
 * @author FanLujia
 * @since 2022/06/06 18:49
 */
@Data
public class TopicBo {
    private Integer id;

    private String title;

    private String subtitle;

    private BigDecimal price;

    private String readCount;

    private String picUrl;

    private Integer sortOrder;

    //数组类型，便于参数的处理
    private Integer[] goods;

    private Date addTime;

    private Date updateTime;

    private Boolean deleted;

    private String content;
}