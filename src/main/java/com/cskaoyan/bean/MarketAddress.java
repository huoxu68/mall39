package com.cskaoyan.bean;

import lombok.Data;

import java.util.Date;

@Data
public class MarketAddress {
    private Integer id;

    private String name;

    private Integer userId;

    private String province;

    private String city;

    private String county;

    private String addressDetail;

    private String areaCode;

    private String postalCode;

    private String tel;

    private Boolean isDefault;

    private Date addTime;

    private Date updateTime;

    private Boolean deleted;
}