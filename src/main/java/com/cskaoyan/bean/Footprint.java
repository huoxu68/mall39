package com.cskaoyan.bean;

import lombok.Data;

import java.util.Date;

/**
 * @ClassName: Footprint
 * @Description:
 * @Since: 2022/06/04 13:15
 * @Author: ZhangHuixiang
 */

@Data
public class Footprint {
    private Integer id;
    private Integer userId;
    private Integer goodsId;
    private Date addTime;
    private Date updateTime;
    private Boolean deleted;
}
