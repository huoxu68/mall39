package com.cskaoyan.bean;

import lombok.Data;

import java.util.Date;

/**
 * @ClassName: Feedback
 * @Description:
 * @Since: 2022/06/04 14:03
 * @Author: ZhangHuixiang
 */
@Data
public class Feedback {
    private Integer id;
    private Integer userId;
    private String username;
    private String mobile;
    private String feedType;
    private String content;
    private Integer status;
    private Boolean hasPicture;
    private String[] picUrls;
    private Date addTime;
    private Date updateTime;
    private Boolean deleted;
}
