package com.cskaoyan.bean;

import lombok.Data;

import java.util.Date;

@Data
public class MarketGroupon {
    private Integer id;

    private Integer orderId;

    private Integer grouponId;

    private Integer rulesId;

    private Integer userId;

    private String shareUrl;

    private Integer creatorUserId;

    private Date creatorUserTime;

    private Short status;

    private Date addTime;

    private Date updateTime;

    private Boolean deleted;
}