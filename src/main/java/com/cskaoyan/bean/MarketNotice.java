package com.cskaoyan.bean;

import lombok.Data;

import java.util.Date;

@Data
public class MarketNotice {
    private Integer id;

    private String title;

    private String content;

    private Integer adminId;

    private Date addTime;

    private Date updateTime;

    private Boolean deleted;
}