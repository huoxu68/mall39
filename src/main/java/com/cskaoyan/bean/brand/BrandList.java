package com.cskaoyan.bean.brand;

import lombok.Data;

/**
 * @author jcp
 * @Description 商品品牌列表类
 * @date 2022年06月04日 19:50
 */
@Data
public class BrandList {

    private Double floorPrice;
    private String picUrl;
    private boolean deleted;
    private String addTime;
    private Integer sortOrder;
    private String name;
    private String updateTime;
    private Integer id;
    private String desc;


}
