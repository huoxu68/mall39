package com.cskaoyan.bean;

import lombok.Data;

import java.util.List;

/**
 * @ClassName: GoodsDetail
 * @Description:
 * @Since: 2022/06/05 21:09
 * @Author: ZhangHuixiang
 */

@Data
public class GoodsDetail {
    private List<GoodsSpecifications> specificationList;
    private List<MarketGroupon> groupon;
    private List<MarketIssue> issue;
    private Integer userHasCollect;
    private String shareImage;
    private GoodsComments comment;
    private Boolean share;
    private List<MarketGoodsAttribute> attribute;
    private MarketBrand brand;
    private List<MarketGoodsProduct> productList;
    private MarketGoods info;
}
