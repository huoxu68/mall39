package com.cskaoyan.controller.admin;

import com.cskaoyan.bean.BaseData;
import com.cskaoyan.bean.BaseRespVo;
import com.cskaoyan.bean.MarketLog;
import com.cskaoyan.bean.param.BaseParam;
import com.cskaoyan.service.LogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ClassName: LogController
 * @Description: 日志请求的处理
 * @Since: 2022/06/06 10:47
 * @Author: FanLujia
 */

@RestController
@RequestMapping("admin/log")
public class LogController {

    @Autowired
    LogService logService;

    @RequestMapping("list")
    public BaseRespVo list(BaseParam baseParam,String name){

        BaseData<MarketLog> logBaseData = logService.queryLogs(baseParam, name);
        return BaseRespVo.ok(logBaseData);
    }


}