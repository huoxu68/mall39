package com.cskaoyan.controller.admin;

import com.cskaoyan.bean.BaseData;
import com.cskaoyan.bean.BaseRespVo;
import com.cskaoyan.bean.MarketCoupon;
import com.cskaoyan.bean.MarketCouponUser;
import com.cskaoyan.bean.param.BaseParam;
import com.cskaoyan.service.CouponService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @ClassName: CouponController
 * @Description:优惠卷管理
 * @Since: 2022/06/04 22:19
 * @Author: changxing
 */


@RestController
@RequestMapping("admin/coupon")
public class CouponController {


    @Autowired
    CouponService couponService;


    @RequiresPermissions("admin:coupon:list")
    @RequestMapping("list")
    public BaseRespVo<BaseData<MarketCoupon>> list(BaseParam baseParam, String name, Short type, Short status) {
        BaseData<MarketCoupon> couponList = couponService.list(baseParam, name, type, status);
        return BaseRespVo.ok(couponList);
    }



    @RequiresPermissions("admin:coupon:create")
    @RequestMapping("create")
    public BaseRespVo<MarketCoupon> create(@RequestBody MarketCoupon coupon) {
        MarketCoupon coupon1 = couponService.create(coupon);
        return BaseRespVo.ok(coupon1);
    }


    @RequiresPermissions("admin:coupon:update")
    @RequestMapping("update")
    public BaseRespVo<MarketCoupon> update(@RequestBody MarketCoupon coupon) {
        MarketCoupon coupon1 = couponService.update(coupon);
        return BaseRespVo.ok(coupon1);
    }



    @RequiresPermissions("admin:coupon:delete")
    @RequestMapping("delete")
    public BaseRespVo delete(@RequestBody MarketCoupon coupon) {
        couponService.delete(coupon);
        return BaseRespVo.ok(null);
    }



    @RequiresPermissions("admin:coupon:listuser")
    @RequestMapping("listuser")
    public BaseRespVo listuser(BaseParam baseParam, Integer couponId, Integer userId, Short status) {
        BaseData<MarketCouponUser> couponList = couponService.listuser(baseParam, couponId, userId, status);
        return BaseRespVo.ok(couponList);
    }



    @RequiresPermissions("admin:coupon:read")
    @RequestMapping("read")
    public BaseRespVo<MarketCoupon> read(Integer id) {
        MarketCoupon couponList = couponService.read(id);
        return BaseRespVo.ok(couponList);
    }

}



