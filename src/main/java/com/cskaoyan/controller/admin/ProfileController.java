package com.cskaoyan.controller.admin;

import com.cskaoyan.bean.BaseRespVo;
import com.cskaoyan.bean.PasswordUpdateBo;
import com.cskaoyan.service.ProfileService;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ClassName: ProfileController
 * @Description:
 * @Since: 2022/06/07 09:36
 * @Author: ZhangHuixiang
 */

@RestController
@RequestMapping("admin/profile")
public class ProfileController {

    @Autowired
    ProfileService profileService;

    @RequestMapping("nnotice")
    public BaseRespVo nnotice() {
        Subject subject = SecurityUtils.getSubject();
        if (subject.isAuthenticated()) {
            return BaseRespVo.ok();
        } else {
            return BaseRespVo.invalidData();
        }
    }

    @RequestMapping("password")
    public BaseRespVo password(@RequestBody PasswordUpdateBo updateBo) {
        if (ObjectUtils.isEmpty(updateBo.getOldPassword())) {
            return BaseRespVo.invalidParameter("原密码不能为空");
        }
        if (ObjectUtils.isEmpty(updateBo.getNewPassword()) || ObjectUtils.isEmpty(updateBo.getNewPassword2())) {
            return BaseRespVo.invalidParameter("新密码不能为空");
        }
        if (!ObjectUtils.nullSafeEquals(updateBo.getNewPassword(), updateBo.getNewPassword2())) {
            return BaseRespVo.invalidParameter("两次新密码不相同");
        }

        Boolean result = profileService.updatePassword(updateBo.getOldPassword(), updateBo.getNewPassword());

        if (result) {
            return BaseRespVo.ok();
        } else {
            return BaseRespVo.invalidParameter("修改密码错误");
        }
    }
}
