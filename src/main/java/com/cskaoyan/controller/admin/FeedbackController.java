package com.cskaoyan.controller.admin;

import com.cskaoyan.bean.BaseData;
import com.cskaoyan.bean.BaseRespVo;
import com.cskaoyan.bean.Feedback;
import com.cskaoyan.bean.param.BaseParam;
import com.cskaoyan.service.FeedbackService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ClassName: FeedbackController
 * @Description: 处理所有前缀为/admin/feedback的请求
 * @Since: 2022/06/04 14:08
 * @Author: ZhangHuixiang
 */

@RestController
@RequestMapping("admin/feedback")
public class FeedbackController {

    @Autowired
    FeedbackService feedbackService;

    @RequiresPermissions("admin:feedback:list")
    @RequestMapping("list")
    public BaseRespVo<BaseData<Feedback>> list(BaseParam baseParam, String username, Integer id) {
        BaseData<Feedback> feedbackData = feedbackService.query(baseParam, username, id);
        return BaseRespVo.ok(feedbackData);
    }
}
