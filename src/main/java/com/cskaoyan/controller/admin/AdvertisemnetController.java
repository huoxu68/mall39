package com.cskaoyan.controller.admin;

import com.cskaoyan.bean.BaseData;
import com.cskaoyan.bean.BaseRespVo;
import com.cskaoyan.bean.MarketAd;
import com.cskaoyan.bean.param.BaseParam;
import com.cskaoyan.service.AdvertisementService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

/**
 * @ClassName: AdvertisemnetController
 * @Description:处理广告管理模块得代码逻辑
 * @Since: 2022/06/04 20:34
 * @Author: changxing
 */


@RestController
@RequestMapping("admin/ad")
public class AdvertisemnetController {

    @Autowired
    AdvertisementService advertisementService;


    @RequiresPermissions("admin:ad:list")
    @RequestMapping("list")
    public BaseRespVo<BaseData<MarketAd>> list(BaseParam baseParam, String name, String content) {
        BaseData<MarketAd> adlList = advertisementService.adlList(baseParam, name, content);
        return BaseRespVo.ok(adlList);
    }


    @RequiresPermissions("admin:ad:create")
    @RequestMapping("create")
    public BaseRespVo<MarketAd> create(@RequestBody MarketAd advertisement) {
        advertisement.setAddTime(new Date());
        advertisement.setUpdateTime(new Date());
        advertisement.setDeleted(false);
        MarketAd ad = advertisementService.createAd(advertisement);
        return BaseRespVo.ok(ad);
    }


    @RequiresPermissions("admin:ad:update")
    @RequestMapping("update")
    public BaseRespVo<MarketAd> update(@RequestBody MarketAd advertisement) {
        MarketAd ad = advertisementService.update(advertisement);
        return BaseRespVo.ok(ad);
    }


    @RequiresPermissions("admin:ad:delete")
    @RequestMapping("delete")
    public BaseRespVo delete(@RequestBody MarketAd advertisement) {
        advertisementService.delete(advertisement);
        return BaseRespVo.ok(null);
    }
}
