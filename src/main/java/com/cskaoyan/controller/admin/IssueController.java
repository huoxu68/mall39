package com.cskaoyan.controller.admin;

import com.cskaoyan.bean.BaseData;
import com.cskaoyan.bean.BaseRespVo;
import com.cskaoyan.bean.bo.IssueBO;
import com.cskaoyan.bean.param.BaseParam;
import com.cskaoyan.bean.po.Issue;
import com.cskaoyan.service.IssueService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 通用问题处理
 * @author zhongyimin
 * @since 2022/06/04 15:46
 */
@RestController
@RequestMapping("admin/issue")
public class IssueController {
    @Autowired
    IssueService issueService;

    @RequestMapping("list")
    public BaseRespVo list(BaseParam param,String question){
        BaseRespVo<Object> list = issueService.list(param,question);
        return list;
    }

    @RequestMapping("create")
    public BaseRespVo<Issue> create(@RequestBody Issue issue){
        BaseRespVo<Issue> respVo = issueService.create(issue.getAnswer(), issue.getQuestion());

        return respVo;
    }

    @RequestMapping("update")
    public BaseRespVo<Issue> update(@RequestBody IssueBO issue){
        BaseRespVo<Issue> update = issueService.update(issue);
        // System.out.println(update);
        return update;
    }

    @RequestMapping("delete")
    public BaseRespVo delete(@RequestBody IssueBO issue){
        issueService.delete(issue);
        return BaseRespVo.ok(null);
    }


}
