package com.cskaoyan.controller.admin;

import com.cskaoyan.config.shiro.MallToken;
import com.cskaoyan.bean.*;
import com.cskaoyan.service.RoleService;
import com.cskaoyan.util.MD5Utils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.io.Serializable;
import java.util.Map;

/**
 * @ClassName: AuthController
 * @Description:
 * @Since: 2022/06/03 13:13
 * @Author: ZhangHuixiang
 */

@RestController
@RequestMapping("admin/auth")
public class AuthController {

    @Autowired
    RoleService roleService;

    @PostMapping("login")
    public BaseRespVo<LoginAdminData> login(@RequestBody Map map, HttpServletRequest request) {
        String username = (String)map.get("username");
        String password = (String)map.get("password");
        String remoteHost = request.getRemoteHost();
        try {
            password = MD5Utils.getMultiMd5(password, password);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Subject subject = SecurityUtils.getSubject();
        subject.login(new MallToken(username, password, remoteHost, "admin"));

        MarketAdmin primaryPrincipal = (MarketAdmin) subject.getPrincipals().getPrimaryPrincipal();

        if (primaryPrincipal == null) {
            return BaseRespVo.invalidParameter("用户名错误");
        }

        Serializable sessionId = subject.getSession().getId();


        LoginAdminData loginAdminData = new LoginAdminData();
        AdminInfoBean adminInfo = new AdminInfoBean();
        adminInfo.setAvatar(primaryPrincipal.getAvatar());
        adminInfo.setNickName(primaryPrincipal.getUsername());
        loginAdminData.setAdminInfo(adminInfo);
        loginAdminData.setToken((String) sessionId);
        return BaseRespVo.ok(loginAdminData);
    }

    @RequiresAuthentication
    @RequestMapping("info")
    public BaseRespVo<InfoData> info(String token) {
        Subject subject = SecurityUtils.getSubject();
        MarketAdmin primaryPrincipal = (MarketAdmin) subject.getPrincipals().getPrimaryPrincipal();
        InfoData infoData = roleService.queryRolesByIds(primaryPrincipal);
        return BaseRespVo.ok(infoData);
    }

    @RequiresAuthentication
    @RequestMapping("logout")
    public BaseRespVo logout() {
        Subject subject = SecurityUtils.getSubject();
        subject.logout();

        return BaseRespVo.ok();
    }
}
