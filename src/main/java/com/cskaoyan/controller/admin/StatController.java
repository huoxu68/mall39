package com.cskaoyan.controller.admin;

import com.cskaoyan.bean.*;
import com.cskaoyan.service.StatService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ClassName: StatController
 * @Description: 处理所有前缀为/admin/stat的请求
 * @Since: 2022/06/04 14:54
 * @Author: ZhangHuixiang
 */

@RestController
@RequestMapping("admin/stat")
public class StatController {

    @Autowired
    StatService statService;

    @RequiresPermissions("admin:stat:user")
    @RequestMapping("user")
    public BaseRespVo<BaseStatistics<UserStatistics>> user() {
        BaseStatistics<UserStatistics> userStatistics = statService.queryUserStat();
        return BaseRespVo.ok(userStatistics);
    }

    @RequiresPermissions("admin:stat:order")
    @RequestMapping("order")
    public BaseRespVo<BaseStatistics<OrderStatistics>> order() {
        BaseStatistics<OrderStatistics> orderStatistics = statService.queryOrderStat();
        return BaseRespVo.ok(orderStatistics);
    }

    @RequiresPermissions("admin:stat:goods")
    @RequestMapping("goods")
    public BaseRespVo<BaseStatistics<GoodsStatistics>> goods() {
        BaseStatistics<GoodsStatistics> goodsStatistics = statService.queryGoodsStat();
        return BaseRespVo.ok(goodsStatistics);
    }
}
