package com.cskaoyan.controller.admin;

import com.cskaoyan.bean.Address;
import com.cskaoyan.bean.BaseData;
import com.cskaoyan.bean.BaseRespVo;
import com.cskaoyan.bean.param.BaseParam;
import com.cskaoyan.service.AddressService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ClassName: AddressController
 * @Description: 处理所有请求URI前缀为/admin/address的请求
 * @Since: 2022/06/03 21:24
 * @Author: ZhangHuixiang
 */

@RestController
@RequestMapping("admin/address")
public class AddressController {

    @Autowired
    AddressService addressService;

    @RequiresPermissions("admin:address:list")
    @RequestMapping("list")
    public BaseRespVo<BaseData<Address>> list(BaseParam baseParam, Integer userId, String name) {
        BaseData<Address> addressData = addressService.query(baseParam, userId, name);
        return BaseRespVo.ok(addressData);
    }
}
