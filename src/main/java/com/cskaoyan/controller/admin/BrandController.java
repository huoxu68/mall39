package com.cskaoyan.controller.admin;

import com.cskaoyan.bean.BaseData;
import com.cskaoyan.bean.BaseRespVo;
import com.cskaoyan.bean.MarketBrand;
import com.cskaoyan.bean.brand.BrandList;
import com.cskaoyan.service.BrandService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author jcp
 * @Description 品牌 处理前缀是/admin/brand 的请求
 * @date 2022年06月04日 19:35
 */

@RestController
@RequestMapping("admin/brand")
public class BrandController {

    @Autowired
    BrandService brandService;

    //显示商品
    @RequiresPermissions("admin:brand:list")
    @RequestMapping("list")
    public BaseRespVo<BaseData<BrandList>> list(Integer id,String name){
      BaseData<BrandList> brandList = brandService.queryAll(id,name);
        return  BaseRespVo.ok(brandList);
    }


    /**
      * @Description: 商场管理 品牌制造商 逻辑删除 删除后delete 变成 1
      * @Author: jcp
      * @Date: 2022/6/4 21:01
      * @Param brandList:
      *@return com.cskaoyan.bean.BaseRespVo
      **/
    @RequiresPermissions("admin:brand:delete")
    @RequestMapping("delete")
    public BaseRespVo delete(@RequestBody BrandList brandList){
        Integer id = brandList.getId();
        brandService.deleteById(id);
        BaseRespVo<Object> baseRespVo = new BaseRespVo<>();
        baseRespVo.setErrno(0);
        baseRespVo.setErrmsg("成功");
        return baseRespVo;
    }
    //编辑商品
    @RequiresPermissions("admin:brand:update")
    @RequestMapping("update")
    public BaseRespVo<BrandList> update(@RequestBody BrandList brandList){
        BrandList brandList1 = brandService.updateBrand(brandList);
        return  BaseRespVo.ok(brandList1) ;
    }

    //添加商品
    @RequiresPermissions("admin:brand:create")
    @RequestMapping("create")
    public BaseRespVo<MarketBrand> createNewBrand(@RequestBody MarketBrand marketBrand){
       if(StringUtils.isEmpty(marketBrand.getDesc())||StringUtils.isEmpty(marketBrand.getPicUrl())||StringUtils.isEmpty(marketBrand.getFloorPrice())){
           BaseRespVo<MarketBrand> baseRespVo = new BaseRespVo<>();
           baseRespVo.setErrno(401);
           baseRespVo.setErrmsg("参数不对");
           return baseRespVo;
       }
        MarketBrand marketBrand1 = brandService.insertNewBrand(marketBrand);
        return BaseRespVo.ok(marketBrand1);

    }
}
