package com.cskaoyan.controller.admin;

import com.cskaoyan.bean.BaseRespVo;
import com.cskaoyan.bean.SystemConfig;
import com.cskaoyan.service.ConfigService;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

/**
 * @author zxy
 * @version 1.0.0
 * @ClassName ConfigController.java
 * @Description TODO
 * @createTime 2022年06月05日 20:59:00
 */
@RestController
@RequestMapping("admin/config")

public class ConfigController {
    @Autowired
    ConfigService configService;

    @RequiresPermissions(value = {"admin:config:mall", "admin:config:express", "admin:config:order", "admin:config:wx"}, logical = Logical.OR)
    @GetMapping({"mall", "express", "order", "wx"})
    public BaseRespVo config() {
        SystemConfig systemConfig = configService.selectAllConfig();
        return BaseRespVo.ok(systemConfig);
    }

    @RequiresPermissions(value = {"admin:config:mall", "admin:config:express", "admin:config:order", "admin:config:wx"}, logical = Logical.OR)
    @PostMapping({"mall", "express", "order", "wx"})
    public BaseRespVo config(@RequestBody SystemConfig config) {
        Boolean result = configService.updateConfig(config);
        if (result) {
            return BaseRespVo.ok();
        } else {
            return BaseRespVo.invalidData("更新失败");
        }
    }
}
