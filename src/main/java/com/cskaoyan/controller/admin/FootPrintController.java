package com.cskaoyan.controller.admin;

import com.cskaoyan.bean.BaseData;
import com.cskaoyan.bean.BaseRespVo;
import com.cskaoyan.bean.Footprint;
import com.cskaoyan.bean.param.BaseParam;
import com.cskaoyan.service.FootprintService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ClassName: FootPrintController
 * @Description: 处理所有前缀为/admin/footprint的请求
 * @Since: 2022/06/04 13:13
 * @Author: ZhangHuixiang
 */

@RestController
@RequestMapping("admin/footprint")
public class FootPrintController {

    @Autowired
    FootprintService footprintService;

    @RequiresPermissions("admin:footprint:list")
    @RequestMapping("list")
    public BaseRespVo<BaseData<Footprint>> list(BaseParam baseParam, Integer userId, Integer goodsId) {
        BaseData<Footprint> footprintData = footprintService.query(baseParam, userId, goodsId);
        return BaseRespVo.ok(footprintData);
    }
}
