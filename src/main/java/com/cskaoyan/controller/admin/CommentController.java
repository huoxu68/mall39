package com.cskaoyan.controller.admin;

import com.cskaoyan.bean.BaseData;
import com.cskaoyan.bean.BaseRespVo;
import com.cskaoyan.bean.MarketComment;
import com.cskaoyan.bean.bo.CommentIdBo;
import com.cskaoyan.bean.param.BaseParam;
import com.cskaoyan.service.CommentService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 商品的评论
 *
 * @author 李贵灏
 * @since 2022/06/05 21:35
 */
@RequestMapping("admin/comment")
@RestController
public class CommentController {

    @Autowired
    CommentService commentService;

    /**
     * 评论的查询
     *
     * @param baseParam
     * @return
     */
    @RequiresPermissions("admin:comment:list")
    @RequestMapping("list")
    public BaseRespVo<BaseData<MarketComment>> list(BaseParam baseParam, CommentIdBo commentIdBo) {
        BaseData<MarketComment> baseData = commentService.commentList(baseParam, commentIdBo);
        return BaseRespVo.ok(baseData);
    }

    /**
     * 评论的删除
     *
     * @param marketComment
     * @return
     */
    @RequiresPermissions("admin:comment:delete")
    @RequestMapping("delete")
    public BaseRespVo delete(@RequestBody MarketComment marketComment) {
        commentService.deleteComment(marketComment);
        return BaseRespVo.ok(null);
    }
}
