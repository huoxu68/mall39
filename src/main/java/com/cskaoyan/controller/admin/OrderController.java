package com.cskaoyan.controller.admin;

import com.cskaoyan.bean.*;
import com.cskaoyan.bean.param.BaseParam;
import com.cskaoyan.bean.param.OrderListParam;
import com.cskaoyan.bean.po.ShipChannel;
import com.cskaoyan.bean.vo.OrderDetailInfoVO;
import com.cskaoyan.bean.vo.OrderListVO;
import com.cskaoyan.service.OrderService;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author zhongyimin
 * @since 2022/06/04 18:15
 */
@RestController()
@RequestMapping("admin/order")
public class OrderController {

    @Autowired
    OrderService orderService;

    @RequiresPermissions("admin:order:list")
    @RequestMapping("list")
    public BaseRespVo<BaseData<OrderListVO>> list(BaseParam param, OrderListParam orderListParam) {
        BaseRespVo<BaseData<OrderListVO>> orderList = orderService.listByParams(param, orderListParam);

        return orderList;
    }

    @RequiresPermissions("admin:order:channel")
    @RequestMapping("channel")
    public BaseRespVo<ShipChannel> channel() {
        List<ShipChannel> channels = orderService.getChanel();
        return BaseRespVo.ok(channels);
    }

    @RequiresPermissions("admin:order:detail")
    @RequestMapping("detail")
    public BaseRespVo<OrderDetailInfoVO> detailInfo(Integer id) {

        BaseRespVo<OrderDetailInfoVO> detailInfoVOBaseRespVo = orderService.detailInfo(id);
        return detailInfoVOBaseRespVo;
    }

    /**
     * TODO 此处的afterStatus都是0，暂时不管，有时间再写
     *
     * @param orderId
     * @param refundMoney
     * @return
     */
    @RequiresPermissions("admin:order:refund")
    @RequestMapping("refund")
    public BaseRespVo refund(Integer orderId, Double refundMoney) {
        orderService.refundById(orderId, refundMoney);
        return BaseRespVo.ok(null);
    }

    /**
     * TODO 暂时找不到该请求在哪
     */
    @RequiresPermissions("admin:order:ship")
    @RequestMapping("ship")
    public BaseRespVo ship(@RequestBody Map map) {
        Integer orderId = (Integer) map.get("orderId");
        MarketOrder marketOrder = new MarketOrder();
        marketOrder.setShipChannel((String) map.get("shipChannel"));
        marketOrder.setShipSn((String) map.get("shipSn"));
        orderService.ship(marketOrder, orderId);
        return BaseRespVo.fine();
    }

    /**
     * 评论的回复
     *
     * @param map
     * @return
     */
    @RequiresPermissions("admin:order:reply")
    @RequestMapping("reply")
    public BaseRespVo reply(@RequestBody Map map) {
        Integer commentId = (Integer) map.get("commentId");
        String content = (String) map.get("content");
        MarketComment marketComment = new MarketComment();
        marketComment.setId(commentId);
        marketComment.setAdminContent(content);
        Date date = new Date();
        marketComment.setUpdateTime(date);
        Boolean flag = orderService.replyComment(marketComment);
        if (flag) {
            BaseRespVo baseRespVo = new BaseRespVo();
            baseRespVo.setErrmsg("订单商品已回复！");
            baseRespVo.setErrno(622);
            return baseRespVo;
        }
        return BaseRespVo.fine();
    }

}
