package com.cskaoyan.controller.admin;

import com.cskaoyan.bean.BaseData;
import com.cskaoyan.bean.BaseRespVo;
import com.cskaoyan.bean.region.ProvinceListOfDataOfRegionVO;
import com.cskaoyan.service.Regionservice;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author jcp
 * @Description 行政区域
 * @date 2022年06月04日 14:16
 */

@RestController
@RequestMapping("admin/region")
public class RegionController {

    @Autowired
    Regionservice regionservice;


    @RequiresPermissions("admin:region:list")
    @RequestMapping("list")
    public BaseRespVo<BaseData<ProvinceListOfDataOfRegionVO>> list() {
        BaseData<ProvinceListOfDataOfRegionVO> regionData = regionservice.selectAllRegion();
        return BaseRespVo.ok(regionData);
    }
}
