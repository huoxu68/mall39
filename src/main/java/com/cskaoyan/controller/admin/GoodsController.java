package com.cskaoyan.controller.admin;

import com.cskaoyan.bean.*;
import com.cskaoyan.bean.bo.GoodsCreateBo;
import com.cskaoyan.bean.bo.GoodsMsgBo;
import com.cskaoyan.bean.param.BaseParam;
import com.cskaoyan.bean.vo.GoodsCategoryVo;
import com.cskaoyan.bean.vo.GoodsListVo;
import com.cskaoyan.bean.vo.GoodsQueryMsgVo;
import com.cskaoyan.service.GoodsService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 商品的管理：GoodsController
 *
 * @author 李贵灏
 * @since 2022/06/04 10:29
 */
@RestController
@RequestMapping("admin/goods")
public class GoodsController {

    @Autowired
    GoodsService goodsService;

    /**
     * 显示所有的商品  admin/goods/list
     *
     * @param baseParam
     * @return
     */
    @RequiresPermissions("admin:goods:list")
    @RequestMapping("list")
    public BaseRespVo<BaseData<GoodsListVo>> list(BaseParam baseParam, GoodsMsgBo goodsMsgBo) {
        if (baseParam.getLimit() == 0) {
            return BaseRespVo.ok();
        }
        BaseData<GoodsListVo> baseData = goodsService.goodsList(baseParam, goodsMsgBo);
        return BaseRespVo.ok(baseData);
    }

    /**
     * 响应可选择的父子类  以及品牌商   /admin/goods/catAndBrand
     * 向响应体中返回父子类目 以及 品牌制造商
     *
     * @return
     */
    @RequiresPermissions("admin:goods:catAndBrand")
    @RequestMapping("catAndBrand")
    public BaseRespVo<GoodsCategoryVo> catAndBrand() {
        GoodsCategoryVo goodsCategoryVo = goodsService.goodsCategory();
        return BaseRespVo.ok(goodsCategoryVo);
    }

    /**
     * 商品信息修改时的信息回显
     *
     * @param id
     * @return
     */
    @RequiresPermissions("admin:goods:detail")
    @RequestMapping("detail")
    public BaseRespVo<GoodsQueryMsgVo> detail(Integer id) {
        GoodsQueryMsgVo goodsQueryMsgVo = goodsService.goodsMsgById(id);
        return BaseRespVo.ok(goodsQueryMsgVo);
    }

    /**
     * 商品的删除
     *
     * @param
     * @return
     */
    @RequiresPermissions("admin:goods:delete")
    @RequestMapping("delete")
    public BaseRespVo delete(@RequestBody MarketGoods goodsBo) {

        goodsService.deleteGoodsMsg(goodsBo.getId());
        return BaseRespVo.ok(null);
    }

    /**
     * 商品的上架
     *
     * @param goodsCreateBo
     * @return
     */
    @RequiresPermissions("admin:goods:create")
    @RequestMapping("create")
    public BaseRespVo create(@RequestBody GoodsCreateBo goodsCreateBo) {
        goodsService.createNewGoods(goodsCreateBo);
        return BaseRespVo.ok(null);
    }

    /**
     * 商品的修改操作
     *
     * @param goodsCreateBo
     * @return
     */
    @RequiresPermissions("admin:goods:update")
    @RequestMapping("update")
    public BaseRespVo update(@RequestBody GoodsCreateBo goodsCreateBo) {
        goodsService.updateGoodsMsg(goodsCreateBo);
        return BaseRespVo.ok(null);
    }
}
