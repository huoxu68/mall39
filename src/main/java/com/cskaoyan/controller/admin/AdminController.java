package com.cskaoyan.controller.admin;

import com.cskaoyan.bean.*;
import com.cskaoyan.bean.param.BaseParam;
import com.cskaoyan.service.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ClassName: AdminController
 * @Description: 管理员相关操作
 * @Since: 2022/06/04 17:40
 * @Author: FanLujia
 */

@RestController
@RequestMapping("admin/admin")
public class AdminController {

    @Autowired
    AdminService adminService;

    /**
     * 处理管理员查询的请求
     *
     * @param baseParam, Integer userId, String name)
     * @return
     * @author FanLujia
     * @since 2022/06/04 21:38
     */
    @RequestMapping("list")
    public BaseRespVo list(BaseParam baseParam, String username) {
        BaseData<Admin> adminBaseData = adminService.queryAdmins(baseParam, username);
        return BaseRespVo.ok(adminBaseData);
    }

    /**
     * 处理新增管理员的请求
     *
     * @param admin
     * @return
     * @author FanLujia
     * @since 2022/06/04 21:39
     */
    @RequestMapping("create")
    public BaseRespVo create(@RequestBody Admin admin) {
        if (admin.getUsername().length() < 6){
            return BaseRespVo.invalidParameter("管理员名称不符合规定！");
        }else if (admin.getPassword().length() < 6){
            return BaseRespVo.invalidParameter("密码长度不能小于6");
        }
        Integer code = adminService.addAdmin(admin);
        if (code == 200) {
            return BaseRespVo.ok(admin);
        } else if (code == 400) {
            return BaseRespVo.invalidParameter("管理员名称已存在！");
        } else if (code == 500) {
            return BaseRespVo.invalidParameter("网络异常，请重试！");
        } else {
            return BaseRespVo.invalidData();
        }
    }

    /**
     * 处理更改管理员信息的请求
     *
     * @param admin
     * @return
     * @author FanLujia
     * @since 2022/06/04 22:04
     */
    @RequestMapping("update")
    public BaseRespVo update(@RequestBody MarketAdmin admin) {
        MarketAdmin updateAdmin = adminService.updateAdmin(admin);
        return BaseRespVo.ok(updateAdmin);
    }


    @RequestMapping("delete")
    public BaseRespVo delete(@RequestBody MarketAdmin admin) {
        Integer code = adminService.deleteAdmin(admin);
        return BaseRespVo.ok();
    }


}