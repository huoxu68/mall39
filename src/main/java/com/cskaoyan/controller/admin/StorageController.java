package com.cskaoyan.controller.admin;

import com.cskaoyan.bean.BaseData;
import com.cskaoyan.bean.BaseRespVo;
import com.cskaoyan.bean.MarketStorage;
import com.cskaoyan.bean.param.BaseParam;
import com.cskaoyan.bean.storage.bo.UpdateBo;
import com.cskaoyan.service.StorageService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.Date;

/**
 * @author zxy
 * @version 1.0.0
 * @ClassName StorageController.java
 * @Description TODO
 * @createTime 2022年06月05日 16:32:00
 */
@RestController
@RequestMapping("admin/storage")
public class StorageController {

    @Autowired
    StorageService storageService;

    @RequiresPermissions("admin:storage:list")
    @RequestMapping("list")
    public BaseRespVo<BaseData<MarketStorage>> queryByKeyAndName(BaseParam baseParam, String key, String name){

        BaseData<MarketStorage> storageData = storageService.queryByKeyAndName(baseParam, key, name);
        return BaseRespVo.ok(storageData);
    }
    @RequiresPermissions("admin:storage:create")
    @RequestMapping("create")
    public BaseRespVo<MarketStorage> create(MultipartFile file) {

        MarketStorage storage = storageService.insertStorage(file);

        return BaseRespVo.ok(storage);

    }
    @RequiresPermissions("admin:storage:update")
    @RequestMapping("update")
    public BaseRespVo<MarketStorage> updateName(@RequestBody UpdateBo updateBo){

       MarketStorage marketStorage = storageService.updateName(updateBo);

       return BaseRespVo.ok(marketStorage);
    }
    @RequiresPermissions("admin:storage:delete")
    @RequestMapping("delete")
    public BaseRespVo delete(@RequestBody UpdateBo updateBo){
        storageService.delete(updateBo);
        return BaseRespVo.ok();
    }
}
