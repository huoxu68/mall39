package com.cskaoyan.controller.admin;

import com.cskaoyan.bean.BaseData;
import com.cskaoyan.bean.BaseRespVo;
import com.cskaoyan.bean.MarketCategory;
import com.cskaoyan.bean.MarketCategoryExample;
import com.cskaoyan.bean.category.CategoryL1;
import com.cskaoyan.bean.category.CategoryListL1;
import com.cskaoyan.service.CategoryService;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author jcp
 * @Description 商品类目类
 * @date 2022年06月05日 14:43
 */

@RestController
@RequestMapping("admin/category")
public class CategoryContoller {

    @Autowired
    CategoryService categoryService;

    @RequiresPermissions("admin:category:list")
    @RequestMapping("list")
    public BaseRespVo<BaseData<CategoryListL1>> list() {
        BaseData<CategoryListL1> data = categoryService.queryAll();
        return BaseRespVo.ok(data);
    }

    @RequiresAuthentication
    @RequestMapping("l1")
    public BaseRespVo<BaseData<CategoryL1>> l1() {
        BaseData<CategoryL1> l1BaseData = categoryService.queryL1();
        return BaseRespVo.ok(l1BaseData);
    }

    @RequiresPermissions("admin:category:delete")
    @RequestMapping("delete")
    public BaseRespVo delete(@RequestBody CategoryListL1 categoryListL1) {
        //逻辑删除将deleted 值变成1
        categoryService.updateDeletedById(categoryListL1.getId());
        BaseRespVo<Object> baseRespVo = new BaseRespVo<>();
        baseRespVo.setErrno(0);
        baseRespVo.setErrmsg("成功");
        return baseRespVo;
    }

    @RequiresPermissions("admin:category:create")
    @RequestMapping("create")
    public BaseRespVo<MarketCategory> createNewCategory(@RequestBody MarketCategory category) {
        if (StringUtils.isEmpty(category.getKeywords()) || StringUtils.isEmpty(category.getLevel()) || StringUtils.isEmpty(category.getPid()) ||
                StringUtils.isEmpty(category.getDesc()) || StringUtils.isEmpty(category.getIconUrl()) || StringUtils.isEmpty(category.getPicUrl())) {
            BaseRespVo<MarketCategory> baseRespVo = new BaseRespVo<>();
            baseRespVo.setErrno(401);
            baseRespVo.setErrmsg("参数不对");
            return baseRespVo;
        }

        MarketCategory marketCategory = categoryService.addCategory(category);
        return BaseRespVo.ok(marketCategory);

    }


    @RequestMapping("update")
    @RequiresPermissions("admin:category:update")
    public BaseRespVo updateCategory(@RequestBody MarketCategory category) {
        Integer rows = categoryService.updateCategory(category);
        //更新失败
        if(rows == 0 ){
            return BaseRespVo.invalidData("更新失败");
        }
        BaseRespVo<Object> baseRespVo = new BaseRespVo<>();
        baseRespVo.setErrmsg("成功");
        baseRespVo.setErrno(0);
        return baseRespVo;


    }


}
