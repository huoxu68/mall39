package com.cskaoyan.controller.admin;

import com.cskaoyan.bean.BaseRespVo;
import com.cskaoyan.bean.TotalCount;
import com.cskaoyan.service.TotalCountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ClassName: DashBoardController
 * @Description: 处理dashboard请求
 * @Since: 2022/06/03 14:52
 * @Author: ZhangHuixiang
 */

@RestController
@RequestMapping("admin")
public class DashBoardController {

    @Autowired
    TotalCountService totalCountService;

    @RequestMapping("dashboard")
    public BaseRespVo<TotalCount> dashboard() {
        TotalCount totalCount = totalCountService.getTotalCount();

        return BaseRespVo.ok(totalCount);
    }
}
