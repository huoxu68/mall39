package com.cskaoyan.controller.admin;

import com.cskaoyan.bean.BaseRespVo;
import com.cskaoyan.bean.MarketKeyword;
import com.cskaoyan.bean.param.BaseParam;
import com.cskaoyan.service.KeywordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author zhongyimin
 * @since 2022/06/05 19:43
 */
@RestController
@RequestMapping("admin/keyword")
public class KeywordController {

    @Autowired
    KeywordService keywordService;

    @RequestMapping("list")
    public BaseRespVo list(BaseParam param,String keyword,String url){
        BaseRespVo baseRespVo = keywordService.queryByKeywordAndUrl(param,keyword,url);
        return baseRespVo;
    }

    @RequestMapping("create")
    public BaseRespVo create(@RequestBody MarketKeyword marketKeyword){
        MarketKeyword newMarketkeyword = keywordService.create(marketKeyword);
        return BaseRespVo.ok(newMarketkeyword);
    }

    @RequestMapping("update")
    public BaseRespVo update(@RequestBody MarketKeyword marketKeyword){
        MarketKeyword newMarketKeyword = keywordService.update(marketKeyword);
        return BaseRespVo.ok(newMarketKeyword);
    }

    @RequestMapping("delete")
    public BaseRespVo delete(@RequestBody MarketKeyword marketKeyword){
        keywordService.delete(marketKeyword);
        return BaseRespVo.ok(null);
    }

}
