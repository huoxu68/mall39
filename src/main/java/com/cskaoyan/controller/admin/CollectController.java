package com.cskaoyan.controller.admin;

import com.cskaoyan.bean.BaseData;
import com.cskaoyan.bean.BaseRespVo;
import com.cskaoyan.bean.Collect;
import com.cskaoyan.bean.param.BaseParam;
import com.cskaoyan.service.CollectService;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ClassName: CollectController
 * @Description: 处理所有前缀为/admin/collect的请求
 * @Since: 2022/06/03 22:13
 * @Author: ZhangHuixiang
 */

@RestController
@RequestMapping("admin/collect")
public class CollectController {

    @Autowired
    CollectService collectService;

    @RequiresPermissions("admin:collect:list")
    @RequestMapping("list")
    public BaseRespVo<BaseData<Collect>> list(BaseParam baseParam, Integer userId, Integer valueId) {
        BaseData<Collect> collectData = collectService.query(baseParam, userId, valueId);
        return BaseRespVo.ok(collectData);
    }
}
