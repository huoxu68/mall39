package com.cskaoyan.controller.admin;

import com.cskaoyan.bean.BaseData;
import com.cskaoyan.bean.BaseRespVo;
import com.cskaoyan.bean.User;
import com.cskaoyan.bean.param.BaseParam;
import com.cskaoyan.service.UserService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ClassName: UserController
 * @Description: 处理admin/user下的所有请求
 * @Since: 2022/06/03 13:26
 * @Author: ZhangHuixiang
 */

@RestController
@RequestMapping("admin/user")
public class UserController {

    @Autowired
    UserService userService;

    @RequiresPermissions("admin:user:list")
    @RequestMapping("list")
    public BaseRespVo<BaseData<User>> list(BaseParam baseParam, String username, String mobile) {
        BaseData<User> userData = userService.query(baseParam, username, mobile);
        return BaseRespVo.ok(userData);
    }

    @RequiresPermissions("admin:user:detail")
    @RequestMapping("detail")
    public BaseRespVo<User> detail(Integer id) {
        User user = userService.queryByUserId(id);
        return BaseRespVo.ok(user);
    }

    @RequiresPermissions("admin:user:update")
    @PostMapping("update")
    public BaseRespVo<Integer> update(@RequestBody User user) {
        Integer affectedRows = userService.update(user);
        return BaseRespVo.ok(affectedRows);
    }
}
