package com.cskaoyan.controller.admin;

import com.cskaoyan.bean.BaseData;
import com.cskaoyan.bean.BaseRespVo;
import com.cskaoyan.bean.SearchHistory;
import com.cskaoyan.bean.param.BaseParam;
import com.cskaoyan.service.HistoryService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ClassName: HistoryController
 * @Description: 处理所有前缀为/admin/history的请求
 * @Since: 2022/06/04 13:35
 * @Author: ZhangHuixiang
 */

@RestController
@RequestMapping("admin/history")
public class HistoryController {

    @Autowired
    HistoryService historyService;

    @RequiresPermissions("admin:history:list")
    @RequestMapping("list")
    public BaseRespVo<BaseData<SearchHistory>> list(BaseParam baseParam, Integer userId, String keyword) {
        BaseData<SearchHistory> searchHistoryData = historyService.query(baseParam, userId, keyword);
        return BaseRespVo.ok(searchHistoryData);
    }
}
