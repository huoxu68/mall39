package com.cskaoyan.controller.admin;

import com.cskaoyan.bean.BaseData;
import com.cskaoyan.bean.BaseRespVo;
import com.cskaoyan.bean.MarketTopic;
import com.cskaoyan.bean.param.BaseParam;
import com.cskaoyan.bean.vo.TopicReadDataVo;
import com.cskaoyan.service.TopicService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ClassName: TopicController
 * @Description:专题管理
 * @Since: 2022/06/05 17:37
 * @Author: changxing
 */


@RestController
@RequestMapping("admin/topic")
public class TopicController {


    @Autowired
    TopicService topicService;


    @RequiresPermissions("admin:topic:list")
    @RequestMapping("list")
    public BaseRespVo<BaseData<MarketTopic>> list(BaseParam baseParam, String title, String subtitle) {
        BaseData<MarketTopic> adlList = topicService.list(baseParam, title, subtitle);
        return BaseRespVo.ok(adlList);
    }

    /**
     * topic专题的新增/回显/更改/逻辑删除
     *
     * @author FanLujia
     * @since 2022/06/06 15:43
     */
    @RequestMapping("create")
    public BaseRespVo create(@RequestBody MarketTopic marketTopic) {

        MarketTopic insertTopic = topicService.insertTopic(marketTopic);
        return BaseRespVo.ok(insertTopic);
    }

    @RequestMapping("read")
    public BaseRespVo read(Integer id) {
        TopicReadDataVo topicReadDataVo = topicService.readTopic(id);
        return BaseRespVo.ok(topicReadDataVo);
    }

    @RequestMapping("update")
    public BaseRespVo update(@RequestBody MarketTopic marketTopic) {
        MarketTopic updateTopic = topicService.updateTopic(marketTopic);
        return BaseRespVo.ok(updateTopic);

    }

    @RequestMapping("delete")
    public BaseRespVo delete(@RequestBody MarketTopic marketTopic) {
        Integer code = topicService.deleteTopic(marketTopic);
        return BaseRespVo.ok();

    }


}
