package com.cskaoyan.controller.admin;

import com.cskaoyan.bean.BaseData;
import com.cskaoyan.bean.BaseRespVo;
import com.cskaoyan.bean.MarketRole;
import com.cskaoyan.bean.role.bo.CreateBo;
import com.cskaoyan.bean.role.bo.PermissionBo;
import com.cskaoyan.bean.role.bo.UpdateBo;
import com.cskaoyan.bean.role.vo.CreateVo;
import com.cskaoyan.bean.role.vo.PermissionDetailVo;
import com.cskaoyan.bean.param.BaseParam;
import com.cskaoyan.bean.vo.RoleInAdmin;
import com.cskaoyan.service.RoleService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author zxy
 * @version 1.0.0
 * @ClassName RoleController.java
 * @Description TODO
 * @createTime 2022年06月04日 16:27:00
 */
@RestController
@RequestMapping("admin/role")
public class RoleController {
    @Autowired
    RoleService roleService;


    @RequiresPermissions("admin:role:create")
    @RequestMapping("create")
    public BaseRespVo<MarketRole> createRole(@RequestBody CreateBo createBo){

       BaseRespVo<MarketRole> baseRespVo= roleService.createRole(createBo);
        return baseRespVo;
    }

    @RequiresPermissions("admin:role:delete")
    @RequestMapping("delete")
    public BaseRespVo deleteRole(@RequestBody CreateVo createVo){

        roleService.deleteRole(createVo);
            return BaseRespVo.ok();
    }
    @RequiresPermissions("admin:role:update")
    @RequestMapping("update")
    public BaseRespVo updateRole(@RequestBody UpdateBo updateBo){

        roleService.updateRole(updateBo);
        return BaseRespVo.ok();
    }

    @RequiresPermissions("admin:role:permissions")
    @PostMapping("permissions")
    public BaseRespVo<PermissionDetailVo> Permissions(@RequestBody PermissionBo permissionBo){
        BaseRespVo baseRespVo = roleService.insertpermissions(permissionBo);
        return baseRespVo;


    }

    @RequiresPermissions("admin:role:permissions")
    @GetMapping("permissions")
    public BaseRespVo<PermissionDetailVo> Permissions2(Integer roleId){
        BaseRespVo baseRespVo = roleService.getpermissions(roleId);
        return baseRespVo;


    }

    /**
     * admin回显页面的role数据
     * @author FanLujia
     * @since 2022/06/05 22:37
     */
    @RequestMapping("options")
    public BaseRespVo queryRole(){
        BaseData<RoleInAdmin> roleBaseData = roleService.queryRole();
        return BaseRespVo.ok(roleBaseData);

    }

    /**
     * 查询role数据
     * @author FanLujia
     * @since 2022/06/05 23:29
     */
    @RequestMapping("list")
    public BaseRespVo listRole(BaseParam baseParam,String name){

        BaseData<MarketRole> roleBaseData = roleService.selectRole(baseParam, name);
        return BaseRespVo.ok(roleBaseData);

    }



}
