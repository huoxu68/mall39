package com.cskaoyan.controller.wx;

import com.cskaoyan.bean.BaseRespVo;
import com.cskaoyan.bean.search.vo.SearchIndexVo;
import com.cskaoyan.service.SearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author zxy
 * @version 1.0.0
 * @ClassName SearchController.java
 * @Description TODO
 * @createTime 2022年06月07日 10:40:00
 */
@RestController
@RequestMapping("wx/search")
public class SearchController {

    @Autowired
    SearchService searchService;

    @RequestMapping("index")
    public BaseRespVo<SearchIndexVo> index(){
        BaseRespVo baseRespVo = searchService.getIndex();
        return baseRespVo;
    }

    @RequestMapping("clearhistory")
    public BaseRespVo clearhistory(){
        searchService.clearhistory();
        return BaseRespVo.ok();
    }
    @RequestMapping("helper")
    public BaseRespVo<List<String>> helper(String keyword){
      BaseRespVo baseRespVo =  searchService.helper(keyword);
      return baseRespVo;

    }


}
