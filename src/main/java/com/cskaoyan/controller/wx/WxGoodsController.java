package com.cskaoyan.controller.wx;

import com.cskaoyan.bean.*;
import com.cskaoyan.bean.param.BaseParam;
import com.cskaoyan.service.GoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ClassName: GoodsController
 * @Description: 处理所有前缀为/wx/goods的请求
 * @Since: 2022/06/05 16:40
 * @Author: ZhangHuixiang
 */

@RestController
@RequestMapping("wx/goods")
public class WxGoodsController {

    @Autowired
    GoodsService goodsService;

    @RequestMapping("count")
    public BaseRespVo<Long> count() {
        Long count = goodsService.selectGoodsCount();
        return BaseRespVo.ok(count);
    }

    @RequestMapping("category")
    public BaseRespVo<WxGoodsCategory> category(Integer id) {
        WxGoodsCategory category = goodsService.selectGoodsCategoryById(id);
        return BaseRespVo.ok(category);
    }

    @RequestMapping("list")
    public BaseRespVo<BaseData<MarketGoods>> list(BaseParam baseParam, String keyword, Integer categoryId) {
        BaseData<MarketGoods> goodsData = goodsService.selectGoodsByCategoryIdAndKeyword(baseParam, keyword, categoryId);
        return BaseRespVo.ok(goodsData);
    }

    @RequestMapping("detail")
    public BaseRespVo<GoodsDetailData> detail(Integer id) {
        GoodsDetailData goodsData = goodsService.selectGoodsDetailByGoodsId(id);
        return BaseRespVo.ok(goodsData);
    }

    @RequestMapping("related")
    public BaseRespVo<BaseData<MarketGoods>> related(Integer id) {
        BaseData<MarketGoods> goodsData = goodsService.selectRelatedGoodsByGoodsId(id);
        return BaseRespVo.ok(goodsData);
    }
}
