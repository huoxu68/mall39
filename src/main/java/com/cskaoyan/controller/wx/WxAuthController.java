package com.cskaoyan.controller.wx;

import com.cskaoyan.config.shiro.MallToken;
import com.cskaoyan.bean.*;
import com.cskaoyan.service.UserService;
import com.cskaoyan.util.MD5Utils;
import com.cskaoyan.util.SMSUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.io.Serializable;
import java.util.Map;

/**
 * @ClassName: WxAuthController
 * @Description: 处理所有前缀为/wx/auth的请求
 * @Since: 2022/06/06 19:05
 * @Author: ZhangHuixiang
 */

@RestController
@RequestMapping("wx/auth")
public class WxAuthController {

    @Autowired
    UserService userService;

    @PostMapping("login")
    public BaseRespVo<LoginUserData> login(@RequestBody Map map, HttpServletRequest request) {
        String username = (String) map.get("username");
        String password = (String) map.get("password");
        String remoteHost = request.getRemoteHost();
        try {
            password = MD5Utils.getMultiMd5(password, password);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Subject subject = SecurityUtils.getSubject();
        subject.login(new MallToken(username, password, remoteHost, "user"));

        MarketUser primaryPrincipal = (MarketUser) subject.getPrincipals().getPrimaryPrincipal();

        if (primaryPrincipal == null) {
            return BaseRespVo.invalidParameter("用户名错误");
        }

        Serializable sessionId = subject.getSession().getId();


        LoginUserData loginUserData = new LoginUserData();
        UserInfoBean userInfo = new UserInfoBean();
        userInfo.setAvatarUrl(primaryPrincipal.getAvatar());
        userInfo.setNickName(primaryPrincipal.getNickname());
        loginUserData.setUserInfo(userInfo);
        loginUserData.setToken((String) sessionId);
        return BaseRespVo.ok(loginUserData);
    }

    @RequiresAuthentication
    @RequestMapping("logout")
    public BaseRespVo logout() {
        Subject subject = SecurityUtils.getSubject();
        subject.logout();
        return BaseRespVo.ok();
    }

    private String code = null;

    private long time = 0;

    @RequestMapping("regCaptcha")
    public BaseRespVo regCaptcha(@RequestBody RegisterBo registerBo) {
        if (System.currentTimeMillis() - time < 60000) {
            return BaseRespVo.invalidData("距离上次发送验证码还不到一分钟");
        }
        time = System.currentTimeMillis();
        code = SMSUtils.sendMessage(registerBo.getMobile());
        if (code != null) {
            return BaseRespVo.ok();
        }
        return BaseRespVo.invalidData("发送短信错误");
    }

    @RequestMapping("register")
    public BaseRespVo register(@RequestBody RegisterBo registerBo, HttpServletRequest req) {
        if (!ObjectUtils.nullSafeEquals(code, registerBo.getCode())) {
            BaseRespVo baseRespVo = new BaseRespVo();
            baseRespVo.setErrno(703);
            baseRespVo.setErrmsg("验证码错误");
            return baseRespVo;
        }

        String password = registerBo.getPassword();
        try {
            registerBo.setPassword(MD5Utils.getMultiMd5(password, password));
        } catch (Exception e) {
            e.printStackTrace();
        }

        Boolean result = userService.insertUser(registerBo);

        if (!result) {
            return BaseRespVo.invalidData("注册失败");
        }

        Subject subject = SecurityUtils.getSubject();
        subject.login(new MallToken(registerBo.getUsername(), registerBo.getPassword(), req.getRemoteHost(), "user"));

        if (subject.isAuthenticated()) {
            LoginUserData data = new LoginUserData();
            UserInfoBean userInfo = new UserInfoBean();
            userInfo.setNickName(registerBo.getUsername());
            userInfo.setAvatarUrl("https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif");
            data.setUserInfo(userInfo);
            data.setToken((String) subject.getSession().getId());
            return BaseRespVo.ok(data);
        } else {
            return BaseRespVo.invalidData("登录失败");
        }
    }

    @RequestMapping("reset")
    public BaseRespVo reset(@RequestBody RegisterBo registerBo) {
        if (!ObjectUtils.nullSafeEquals(code, registerBo.getCode())) {
            BaseRespVo baseRespVo = new BaseRespVo();
            baseRespVo.setErrno(703);
            baseRespVo.setErrmsg("验证码错误");
            return baseRespVo;
        }

        String password = registerBo.getPassword();
        try {
            registerBo.setPassword(MD5Utils.getMultiMd5(password, password));
        } catch (Exception e) {
            e.printStackTrace();
        }

        Boolean result = userService.updatePasswordByMobile(registerBo);

        if (result) {
            return BaseRespVo.ok();
        } else {
            return BaseRespVo.invalidData("修改密码失败");
        }
    }
}
