package com.cskaoyan.controller.wx.cart;

import com.cskaoyan.bean.BaseRespVo;
import com.cskaoyan.bean.MarketCart;
import com.cskaoyan.bean.MarketUser;
import com.cskaoyan.bean.wx_cart_index.*;
import com.cskaoyan.service.CartService;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.lang.reflect.Field;
import java.util.List;
import java.util.Map;

/**
 * @author jcp
 * @Description 添加商品至购物车
 * @date 2022年06月06日 21:35
 */

@RequiresAuthentication
@RestController
@RequestMapping("/wx/cart")
public class CartController {


    @Autowired
    CartService cartService;

    @RequestMapping("add")
    public BaseRespVo addGoods(@RequestBody MarketCart cart){
        //获取userId
        Subject subject = SecurityUtils.getSubject();

        MarketUser user = (MarketUser) subject.getPrincipals().getPrimaryPrincipal();

        Integer userId = user.getId();
        cart.setUserId(userId);
        Integer count = cartService.addGoods(cart);
        return  BaseRespVo.ok(count);
    }

    @RequestMapping("goodscount")
    public  BaseRespVo goodsCounts(){
        Integer count = cartService.queryCounts();
        return BaseRespVo.ok(count);
    }

    @RequestMapping("index")
    public BaseRespVo<Cart> cartIndex(){
        Cart cart = cartService.queryCart();
        if (cart != null) {
            return BaseRespVo.ok(cart);
        } else {
            return BaseRespVo.unAuthorizing();
        }
    }

    @RequestMapping("checked")
    public BaseRespVo<Cart> updateChecked(@RequestBody CheckedBo checkedBo){
        Cart cart =  cartService.updateChecked(checkedBo);
        return  BaseRespVo.ok(cart);
    }

    @RequestMapping("update")
    public BaseRespVo updateNumber(@RequestBody UpdateGoodsBo updateGoodsBo){
        int affectRows = cartService.updateGoodsNum(updateGoodsBo);
        if (affectRows==0){
            return BaseRespVo.invalidData("更新数据失败");
        }
        return BaseRespVo.ok();
    }

    @RequestMapping("checkout")
    public BaseRespVo<CheckoutVO> checkout(CheckoutBO checkoutBO){
       CheckoutVO checkoutVO = cartService.queryCheckout(checkoutBO);
       return BaseRespVo.ok(checkoutVO);
    }

    @RequestMapping("fastadd")
    public BaseRespVo fastadd(@RequestBody MarketCart cart){
        Integer cartId = cartService.fastAdd(cart);
        return BaseRespVo.ok(cartId);
    }

    @RequestMapping("delete")
    public BaseRespVo<Cart> delete(@RequestBody DeleteBO deleteBO){
        Integer[] productIds = deleteBO.getProductIds();
        Cart cart = cartService.dalete(productIds);
        return BaseRespVo.ok(cart);
    }



}
