package com.cskaoyan.controller.wx;

import com.cskaoyan.bean.BaseRespVo;
import com.cskaoyan.bean.MarketStorage;
import com.cskaoyan.service.StorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * @ClassName: WxStorageController
 * @Description: 文件上传处理
 * @Since: 2022/06/09 23:39
 * @Author: FanLujia
 */

@RestController
@RequestMapping("wx/storage")
public class WxStorageController {


}