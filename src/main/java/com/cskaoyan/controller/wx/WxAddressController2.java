package com.cskaoyan.controller.wx;

import com.cskaoyan.bean.BaseData;
import com.cskaoyan.bean.BaseRespVo;
import com.cskaoyan.bean.MarketAddress;
import com.cskaoyan.service.AddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * WxAdressController
 *
 * @author 李贵灏
 * @since 2022/06/06 21:44
 */
// @RequestMapping("wx/address")
// @RestController
// public class WxAddressController2 {
//
//     @Autowired
//     AddressService addressService;
//
//     /**
//      * 打印地址栏
//      * @return
//      */
//     @RequestMapping("list")
//     public BaseRespVo<BaseData<MarketAddress>> list(){
//         BaseData<MarketAddress> baseData = addressService.addressList();
//         return BaseRespVo.ok(baseData);
//     }
//
//     /**
//      * 需修改地址信息
//      * @param marketAddress
//      * @return
//      */
//     @RequestMapping("save")
//     public BaseRespVo<Integer> save(@RequestBody MarketAddress marketAddress){
//        Integer id = addressService.updateAddressMsg(marketAddress);
//         return BaseRespVo.ok(id);
//     }
//
//     /**
//      * 逻辑删除此地址
//      * @param
//      * @return
//      */
//     @RequestMapping("delete")
//     public BaseRespVo delete(@RequestBody Map map ){
//         Integer id = (Integer) map.get("id");
//         addressService.deleteAdress(id);
//         return BaseRespVo.fine();
//     }
//
//     /**
//      * 修改地址信息的时候，地址信息的回显
//      * @param id
//      * @return
//      */
//     @RequestMapping("detail")
//     public BaseRespVo<MarketAddress> detail(Integer id){
//         MarketAddress marketAddress = addressService.addressMsgById(id);
//         return BaseRespVo.ok(marketAddress);
//     }
//
//
// }
