package com.cskaoyan.controller.wx;

import com.cskaoyan.bean.*;
import com.cskaoyan.mapper.MarketAddressMapper;
import com.cskaoyan.service.AddressService;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.AuthorizationException;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import java.util.Map;


/**
 * @ClassName: AddressController
 * @Description: 地址管理业务
 * @Since: 2022/06/06 21:44
 * @Author: changxing
 */
@RestController
@RequestMapping("wx/address")
public class WxAddressController {


    @Autowired
    AddressService addressService;
    @Autowired
    MarketAddressMapper marketAddressMapper;


    @RequiresAuthentication
    @RequestMapping("list")
    public BaseRespVo<BaseData<MarketAddress>> list() {

        Subject subject = SecurityUtils.getSubject();
        if (subject.isAuthenticated()) {
            MarketUser marketUser = (MarketUser) subject.getPrincipals().getPrimaryPrincipal();
            BaseData<MarketAddress> marketAddressList = addressService.list(marketUser.getId());
            return BaseRespVo.ok(marketAddressList);
        }
        throw new AuthorizationException();
    }

    @RequiresAuthentication
    @RequestMapping("detail")
    public BaseRespVo<MarketAddress> detail(Integer id) {
        MarketAddress marketAddress = addressService.detail(id);
        return BaseRespVo.ok(marketAddress);
    }

    @RequiresAuthentication
    @RequestMapping("save")
    public BaseRespVo<Integer> save(@RequestBody MarketAddress marketAddress) {
        Subject subject = SecurityUtils.getSubject();
        if (subject.isAuthenticated()) {
            MarketUser marketUser = (MarketUser) subject.getPrincipals().getPrimaryPrincipal();
            addressService.save(marketAddress, marketUser.getId());
            return BaseRespVo.ok(marketAddress.getId());
        }
        throw new AuthorizationException();
    }

    @RequiresAuthentication
    @RequestMapping("delete")
    public BaseRespVo delete(@RequestBody Map map) {
        Integer id = (Integer) map.get("id");
        addressService.delete(id);
        return BaseRespVo.ok();
    }


}
