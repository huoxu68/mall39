package com.cskaoyan.controller.wx;

import com.cskaoyan.bean.BaseData;
import com.cskaoyan.bean.BaseRespVo;
import com.cskaoyan.bean.MarketTopic;
import com.cskaoyan.bean.param.BaseParam;
import com.cskaoyan.service.TopicService;
import com.cskaoyan.service.WxTopicService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author zxy
 * @version 1.0.0
 * @ClassName TopicController.java
 * @Description TODO
 * @createTime 2022年06月07日 16:10:00
 */
@RestController
@RequestMapping("wx/topic")
public class WxTopicController {

    @Autowired
    WxTopicService wxTopicService;

    @RequestMapping("list")
    public BaseRespVo<BaseData<MarketTopic>> list(BaseParam baseParam){
        BaseRespVo baseRespVo = wxTopicService.wxList(baseParam);
        return baseRespVo;
    }
    @RequestMapping("detail")
    public BaseRespVo detail(Integer id){
        BaseRespVo baseRespVo = wxTopicService.detail(id);
        return baseRespVo;
    }
    @RequestMapping("related")
    public BaseRespVo related(Integer id){
        BaseRespVo baseRespVo = wxTopicService.related(id);
        return  baseRespVo;
    }

}
