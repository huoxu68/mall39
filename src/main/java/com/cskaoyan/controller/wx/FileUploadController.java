package com.cskaoyan.controller.wx;

import com.cskaoyan.bean.BaseRespVo;
import com.cskaoyan.bean.MarketStorage;
import com.cskaoyan.mapper.MarketStorageMapper;
import com.cskaoyan.service.StorageService;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * @author zhongyimin
 * @since 2022/06/07 19:38
 */
@RestController
@RequestMapping("/wx/storage")
public class FileUploadController {
    @Autowired
    StorageService storageService;
    @RequestMapping("upload")
    public BaseRespVo<MarketStorage> upload(MultipartFile file) {
        MarketStorage marketStorage = storageService.insertStorage(file);
        return BaseRespVo.ok(marketStorage);


    }



}
