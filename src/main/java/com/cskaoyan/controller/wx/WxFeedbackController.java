package com.cskaoyan.controller.wx;

import com.cskaoyan.bean.BaseRespVo;
import com.cskaoyan.bean.MarketFeedback;
import com.cskaoyan.mapper.MarketFeedbackMapper;
import com.cskaoyan.service.WxFeedbackService;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 李贵灏
 * @since 2022/06/06 23:27
 */
@RequiresAuthentication
@RequestMapping("wx/feedback")
@RestController
public class WxFeedbackController {

    @Autowired
    WxFeedbackService wxFeedbackService;

    @RequestMapping("submit")
    public BaseRespVo submit(@RequestBody MarketFeedback marketFeedback){
        wxFeedbackService.submitMsg(marketFeedback);
        return BaseRespVo.fine();
    }
}
