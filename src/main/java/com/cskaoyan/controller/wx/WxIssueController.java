package com.cskaoyan.controller.wx;

import com.cskaoyan.bean.BaseData;
import com.cskaoyan.bean.BaseRespVo;
import com.cskaoyan.bean.MarketIssue;
import com.cskaoyan.mapper.MarketIssueMapper;
import com.cskaoyan.service.IssueService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 帮助中心
 *
 * @author 李贵灏
 * @since 2022/06/09 14:31
 */
@RequestMapping("wx/issue")
@RestController
public class WxIssueController {

    @Autowired
    IssueService issueService;


    /**
     * 帮助中心
     * @return
     */
    @RequestMapping("list")
    public BaseRespVo<BaseData<MarketIssue>> list(Integer page , Integer limit){
        BaseData<MarketIssue> baseData = issueService.issueList(page, limit);
        return BaseRespVo.ok(baseData);
    }
}
