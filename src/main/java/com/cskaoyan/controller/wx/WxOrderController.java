package com.cskaoyan.controller.wx;

import com.cskaoyan.bean.BaseData;
import com.cskaoyan.bean.BaseRespVo;
import com.cskaoyan.bean.MarketUser;
import com.cskaoyan.bean.bo.OrderCommentBo;
import com.cskaoyan.bean.MarketOrderGoods;
import com.cskaoyan.bean.vo.OrderDetailOfWx;
import com.cskaoyan.bean.vo.OrderInfoOfDetileOfWx;
import com.cskaoyan.service.OrderService;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * @ClassName: OrderController
 * @Description: 订单相关请求处理
 * @Since: 2022/06/07 10:32
 * @Author: FanLujia
 */

@RestController
@RequestMapping("wx/order")
public class WxOrderController {
    @Autowired
    OrderService orderService;

    @RequestMapping("list")
    public BaseRespVo listOrder(Short showType, Integer page, Integer limit) {
        //showType=0&page=1&limit=10
        BaseData<OrderInfoOfDetileOfWx> listOrderData = orderService.listAllByShowType(showType, page, limit);
        return BaseRespVo.ok(listOrderData);

    }

    @RequestMapping("detail")
    public BaseRespVo detailOrder(Integer orderId) {

        OrderDetailOfWx orderDetailOfWx = orderService.orderAllDetileByOrderId(orderId);
        return BaseRespVo.ok(orderDetailOfWx);

    }

    //申请退款
    @RequestMapping("refund")
    public BaseRespVo refundOrder(@RequestBody Map map) {
        Integer orderId = (Integer) map.get("orderId");
        Integer code = orderService.refundByIdOfWx(orderId);
        if (code == 200) {
            return BaseRespVo.ok();
        } else if (code == 400) {
            return BaseRespVo.invalidData("网络繁忙，稍后重试");
        }
        return BaseRespVo.invalidData("数据异常");
    }

    //申请退款
    @RequestMapping("delete")
    public BaseRespVo deleteOrder(@RequestBody Map map) {
        Integer orderId = (Integer) map.get("orderId");
        Integer affectRows = orderService.deleteByIdOfWx(orderId);
        if (affectRows == 1) {
            return BaseRespVo.ok();
        } else {
            return BaseRespVo.invalidData("删除失败");
        }
    }

    /**
     * wx 订单中的评价
     * @param orderCommentBo
     * @return
     */
    @RequiresAuthentication
    @RequestMapping("comment")
    public BaseRespVo comment(@RequestBody OrderCommentBo orderCommentBo) {
        orderService.orderComment(orderCommentBo);
        return BaseRespVo.fine();
    }

    //确认收货
    @RequestMapping("confirm")
    public BaseRespVo confirmOrder(@RequestBody Map map) {

        Integer orderId = (Integer) map.get("orderId");
        Integer affectRows = orderService.confirmByIdOfWx(orderId);
        if (affectRows == 1) {
            return BaseRespVo.ok();
        } else {
            return BaseRespVo.invalidData("未能确认收货");
        }
    }

    //评论里的订单商品信息显示
    @RequestMapping("goods")
    public BaseRespVo goodsOrder(Integer orderId, Integer goodsId) {
        MarketOrderGoods orderGoods = orderService.goodsOrderInComment(orderId, goodsId);
        return BaseRespVo.ok(orderGoods);
    }

    @RequestMapping("submit")
    public BaseRespVo submit(@RequestBody Map map){
        Subject subject = SecurityUtils.getSubject();
        MarketUser user = (MarketUser) subject.getPrincipals().getPrimaryPrincipal();
        BaseRespVo baseRespVo = orderService.addOrder(map,user);
        return baseRespVo;

    }
    @RequestMapping("prepay")
    public BaseRespVo prepay(@RequestBody Map map){
        Integer orderId = (Integer) map.get("orderId");
        orderService.payById(orderId);
        return BaseRespVo.fine();
    }


}
