package com.cskaoyan.controller.wx;

import com.cskaoyan.bean.BaseData;
import com.cskaoyan.bean.BaseRespVo;
import com.cskaoyan.bean.MarketCoupon;
import com.cskaoyan.bean.MarketUser;
import com.cskaoyan.bean.vo.CommonCoupon;
import com.cskaoyan.bean.vo.MyCoupon;
import com.cskaoyan.service.CouponService;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * @author zhongyimin
 * @since 2022/06/06 20:34
 */
@RestController
@RequestMapping("/wx/coupon")
public class WxCouponController {
    @Autowired
    CouponService couponService;

    @RequiresAuthentication
    @RequestMapping("list")
    public BaseRespVo list(Integer limit, Integer page) {
        Subject subject = SecurityUtils.getSubject();
        if (subject.getPrincipals() == null){
            return BaseRespVo.invalidData("请先登录");
        }
        MarketUser user = (MarketUser) subject.getPrincipals().getPrimaryPrincipal();
        BaseRespVo<BaseData<CommonCoupon>> dataBaseRespVo = couponService.wxListByType(limit, page, user);
        return dataBaseRespVo;
    }

    @RequiresAuthentication
    @RequestMapping("mylist")
    public BaseRespVo mylist(Integer status, Integer page, Integer limit) {
        Subject subject = SecurityUtils.getSubject();
        if (subject.getPrincipals() == null){
            return BaseRespVo.invalidData("请先登录");
        }
        MarketUser primaryPrincipal = (MarketUser) subject.getPrincipals().getPrimaryPrincipal();
        BaseRespVo<BaseData> couponList = couponService.couponListByUser(primaryPrincipal, status, page, limit);
        return couponList;
    }
    @RequiresAuthentication
    @RequestMapping("selectlist")
    public BaseRespVo selectlist(Integer cartId, Integer grouponRuleId) {
        BaseRespVo<BaseData<MyCoupon>> selectCouponList = couponService.selectCouponList(cartId);
        return selectCouponList;
    }

    @RequiresAuthentication
    @RequestMapping("receive")
    public BaseRespVo receive(@RequestBody Map map) {
        Integer couponId = (Integer) map.get("couponId");
        Subject subject = SecurityUtils.getSubject();
        if (subject.getPrincipals() == null){
            return BaseRespVo.invalidData("请先登录");
        }
        MarketUser user = (MarketUser) subject.getPrincipals().getPrimaryPrincipal();
        BaseRespVo respVo = couponService.receiveCoupon(couponId, user);
        return respVo;
    }
    @RequiresAuthentication
    @RequestMapping("exchange")
    public BaseRespVo exchange(@RequestBody Map map) {
        Object code = map.get("code");
        Subject subject = SecurityUtils.getSubject();


        if (subject.getPrincipals() == null){
            return BaseRespVo.invalidData("请先登录");
        }
        MarketUser user = (MarketUser) subject.getPrincipals().getPrimaryPrincipal();
        Boolean flag = couponService.exchange(code, user);
        if (flag) {

            return BaseRespVo.fine();
        } else {
            BaseRespVo<Object> respVo = new BaseRespVo<>();
            respVo.setErrmsg("优惠劵不正确");
            respVo.setErrno(742);
            return respVo;
        }

    }


}
