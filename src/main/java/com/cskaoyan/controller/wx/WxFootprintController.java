package com.cskaoyan.controller.wx;

import com.cskaoyan.bean.BaseData;
import com.cskaoyan.bean.BaseRespVo;
import com.cskaoyan.bean.vo.WxFootprintListVo;
import com.cskaoyan.service.FootprintService;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * wx 端 足迹的一系列操作
 *
 * @author 李贵灏
 * @since 2022/06/07 08:21
 */
@RequiresAuthentication
@RequestMapping("wx/footprint")
@RestController
public class WxFootprintController {


    @Autowired
    FootprintService footprintService;

    /**
     * wx 小程序的打印足迹信息
     * @return
     */
    @RequestMapping("list")
    public BaseRespVo<BaseData<WxFootprintListVo>> list(Integer page,Integer limit ) {
        BaseData<WxFootprintListVo> base = footprintService.footprintList(page,limit);
        return BaseRespVo.ok(base);
    }

    @RequestMapping("delete")
    public BaseRespVo delete(Integer id){
        footprintService.updateFootprint(id);
        return BaseRespVo.fine();
    }
}
