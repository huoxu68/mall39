package com.cskaoyan.controller.wx;

import com.cskaoyan.bean.BaseData;
import com.cskaoyan.bean.BaseRespVo;
import com.cskaoyan.bean.MarketUser;
import com.cskaoyan.bean.param.BaseParam;
import com.cskaoyan.bean.vo.CollectVo;
import com.cskaoyan.service.CollectService;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.AuthorizationException;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * @ClassName: CollectController
 * @Description: 收藏的业务管理
 * @Since: 2022/06/06 19:57
 * @Author: changxing
 */


@RestController
@RequestMapping("wx/collect")
public class WxCollectController {

    @Autowired
    CollectService collectService;


    @RequiresAuthentication
    @RequestMapping("list")
    public BaseRespVo<BaseData<CollectVo>> list(BaseParam baseParam, Byte type) {

        Subject subject = SecurityUtils.getSubject();
        if (subject.isAuthenticated()) {
            MarketUser marketUser = (MarketUser) subject.getPrincipals().getPrimaryPrincipal();
            BaseData<CollectVo> collectList = collectService.list(baseParam, type, marketUser.getId());
            return BaseRespVo.ok(collectList);
        }
        throw new AuthorizationException();
    }


    @RequiresAuthentication
    @RequestMapping("addordelete")
    public BaseRespVo addordelete(@RequestBody Map map) {
        int type = (int) map.get("type");
        byte type1 = (byte) type;
        Integer valueId = (Integer) map.get("valueId");

        Subject subject = SecurityUtils.getSubject();
        if (subject.isAuthenticated()) {
            MarketUser marketUser = (MarketUser) subject.getPrincipals().getPrimaryPrincipal();
            collectService.addordelete(type1, valueId, marketUser.getId());
        }
        return BaseRespVo.ok();
    }
}
