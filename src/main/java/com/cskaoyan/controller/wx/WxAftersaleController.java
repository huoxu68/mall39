package com.cskaoyan.controller.wx;

import com.cskaoyan.bean.BaseRespVo;
import com.cskaoyan.bean.MarketAftersale;
import com.cskaoyan.bean.vo.AftersaleDetailDateVo;
import com.cskaoyan.service.AftersaleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ClassName: WxAftersaleController
 * @Description: 售后请求处理
 * @Since: 2022/06/09 12:26
 * @Author: FanLujia
 */

@RestController
@RequestMapping("wx/aftersale")//submit
public class WxAftersaleController {

    @Autowired
    AftersaleService aftersaleService;

    @RequestMapping("submit")
    public BaseRespVo submitAftersale(@RequestBody MarketAftersale marketAftersale) {
        //showType=0&page=1&limit=10
        aftersaleService.addSubmitAftersale(marketAftersale);
        return BaseRespVo.ok();
    }

    @RequestMapping("detail")
    public BaseRespVo detailAftersale(Integer orderId) {
        //showType=0&page=1&limit=10
        AftersaleDetailDateVo aftersaleDateVo = aftersaleService.detailAftersale(orderId);
        return BaseRespVo.ok(aftersaleDateVo);
    }




}