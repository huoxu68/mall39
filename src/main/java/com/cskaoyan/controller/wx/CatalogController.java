package com.cskaoyan.controller.wx;

import com.cskaoyan.bean.BaseRespVo;
import com.cskaoyan.bean.category.vo.CategoryIndex;
import com.cskaoyan.bean.category.vo.CurrentVo;
import com.cskaoyan.service.CatalogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author zxy
 * @version 1.0.0
 * @ClassName CatalogController.java
 * @Description TODO
 * @createTime 2022年06月07日 09:11:00
 */
@RestController
@RequestMapping("wx/catalog")
public class CatalogController {
    @Autowired
    CatalogService catalogService;


    @RequestMapping("current")
    public BaseRespVo<CurrentVo> current(Integer id){

        BaseRespVo baseRespVo = catalogService.getCurrent(id);
        return baseRespVo;
    }


    @RequestMapping("index")
    public BaseRespVo<CategoryIndex> index(){
        BaseRespVo baseRespVo = catalogService.index();
        return baseRespVo;
    }



}
