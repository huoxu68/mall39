package com.cskaoyan.controller.wx;

import com.cskaoyan.bean.BaseRespVo;
import com.cskaoyan.bean.HomeIndexData;
import com.cskaoyan.bean.SystemConfig;
import com.cskaoyan.bean.vo.HomeAboutVo;
import com.cskaoyan.service.ConfigService;
import com.cskaoyan.service.HomeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ClassName: HomeController
 * @Description: 处理所有前缀为/wx/home的请求
 * @Since: 2022/06/05 15:54
 * @Author: ZhangHuixiang
 */

@RestController
@RequestMapping("/wx/home")
public class HomeController {

    @Autowired
    HomeService homeService;

    @Autowired
    ConfigService configService;

    @RequestMapping("index")
    public BaseRespVo<HomeIndexData> index() {
        HomeIndexData homeIndexData = homeService.getIndexData();
        return BaseRespVo.ok(homeIndexData);
    }

    @RequestMapping("about")
    public BaseRespVo about(){
        /**
         * @title 小程序个人中心中的关于我们
         * @description 
         * @author zxy 
         * @updateTime 2022/6/9 14:51 
         * @return: com.cskaoyan.bean.BaseRespVo
         * @throws 
         */
        SystemConfig systemConfig = configService.selectAllConfig();
        HomeAboutVo homeAboutVo = new HomeAboutVo();
        homeAboutVo.setAddress(systemConfig.getMarket_mall_address());
        homeAboutVo.setLatitude(systemConfig.getMarket_mall_latitude());
        homeAboutVo.setLongitude(systemConfig.getMarket_mall_longitude());
        homeAboutVo.setName(systemConfig.getMarket_mall_name());
        homeAboutVo.setPhone(systemConfig.getMarket_mall_phone());
        homeAboutVo.setQq(systemConfig.getMarket_mall_qq());
        return BaseRespVo.ok(homeAboutVo);
    }

}
