package com.cskaoyan.controller.wx;

import com.cskaoyan.bean.BaseData;
import com.cskaoyan.bean.BaseRespVo;
import com.cskaoyan.bean.MarketComment;
import com.cskaoyan.bean.bo.WxCommentBo;
import com.cskaoyan.bean.vo.CommentCountVo;
import com.cskaoyan.bean.vo.WxCommentListVo;
import com.cskaoyan.service.CommentService;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 商品评价的相关操作
 * @author 李贵灏
 * @ClassName:WxCommentController
 * @since 2022/06/06 19:38
 */

@RequestMapping("wx/comment")
@RestController
public class WxCommentController {

    @Autowired
    CommentService commentService;

    /**
     * wx 小程序的 打印评论信息
     * @param wxCommentBo
     * @return
     */
    @RequestMapping("list")
    public BaseRespVo<BaseData<WxCommentListVo>>  list(WxCommentBo wxCommentBo){
        BaseData<WxCommentListVo> baseData = commentService.wxCommentList(wxCommentBo);
        return BaseRespVo.ok(baseData);
    }

    /**
     * wx 小程序  晒图评论 和全部评论总数
     * @param wxCommentBo
     * @return
     */
    @RequestMapping("count")
    public BaseRespVo<CommentCountVo> count(WxCommentBo wxCommentBo){
        CommentCountVo commentCount = commentService.CommentCount(wxCommentBo);
        return BaseRespVo.ok(commentCount);
    }

    /**
     * wx 的 专题发表评论
     * @param marketComment
     * @return
     */
    @RequiresAuthentication
    @RequestMapping("post")
    public BaseRespVo<MarketComment> post(@RequestBody MarketComment marketComment){
        MarketComment comment = commentService.commentPost(marketComment);
        return BaseRespVo.ok(comment);
    }
}
