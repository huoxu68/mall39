package com.cskaoyan.controller.wx;

import com.cskaoyan.bean.BaseData;
import com.cskaoyan.bean.BaseRespVo;
import com.cskaoyan.bean.MarketBrand;
import com.cskaoyan.bean.param.BaseParam;
import com.cskaoyan.service.BrandService;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ClassName: BrandController
 * @Description: 品牌业务管理
 * @Since: 2022/06/06 20:20
 * @Author: changxing
 */
@RestController
@RequestMapping("wx/brand")
public class WxBrandController {

    @Autowired
    BrandService brandService;


    @RequestMapping("list")
    public BaseRespVo<BaseData<MarketBrand>> list(BaseParam baseParam) {
        BaseData<MarketBrand> brandlList = brandService.list(baseParam);
        return BaseRespVo.ok(brandlList);
    }


    @RequestMapping("detail")
    public BaseRespVo<MarketBrand> detail(Integer id) {
        MarketBrand marketBrand = brandService.detail(id);
        return BaseRespVo.ok(marketBrand);
    }
}
