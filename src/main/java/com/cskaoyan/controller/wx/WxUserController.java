package com.cskaoyan.controller.wx;

import com.cskaoyan.bean.BaseRespVo;
import com.cskaoyan.bean.UserIndexData;
import com.cskaoyan.service.CommentService;
import com.cskaoyan.service.UserService;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ClassName: WxUserController
 * @Description:
 * @Since: 2022/06/06 19:43
 * @Author: ZhangHuixiang
 */

@RestController
@RequestMapping("wx/user")
public class WxUserController {

    @Autowired
    UserService userService;

    @Autowired
    CommentService commentService;

    @RequiresAuthentication
    @RequestMapping("index")
    public BaseRespVo<UserIndexData> index() {
        // commentService.orderComment();
        UserIndexData userIndexData = userService.queryOrderCountByUserId();
        if (userIndexData == null) {
            return BaseRespVo.unAuthorizing();
        }
        return BaseRespVo.ok(userIndexData);
    }
}
