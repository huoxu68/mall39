package com.cskaoyan.controller;

import com.cskaoyan.bean.BaseRespVo;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ClassName: RedirectController
 * @Description:
 * @Since: 2022/06/06 15:29
 * @Author: ZhangHuixiang
 */

@RestController
public class RedirectController {

    @RequestMapping("redirect")
    public BaseRespVo redirect() {
        return BaseRespVo.invalidData("认证失败");
    }
}
